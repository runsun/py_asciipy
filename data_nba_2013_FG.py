

import pprint,math, os, imp
import inspect, doctest 
from copy import deepcopy  # deepcopy used in AscGrid.data 

def getThisPath():
	path= os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))) 
	return path #, fn

THISPATH = getThisPath()
ascgrid= imp.load_source('ascgrid.py', THISPATH)
print ascgrid
from ascgrid import AscGrid
#plotscaler= imp.load_source('plotscaler', os.path.join( THISPATH,'plotscaler.py'))

'''
FG_Lin   =[ (0,37.3),(1,48.9),(2,42.2),(3,43.8),(4,48.2),(5,44.0)]
FG_Harden=[ (0,41.2),(1,46.3),(2,42.9),(3,52.9),(4,38.9),(5,39)]
ag=AscGrid(autoscale=True, gshow=False, legend=True, wline ='-', hline='|', node='+')	
ag.addpxdata( FG_Lin, name='Lin' )
ag.addpxdata( FG_Harden, name='Harden' )
print '\n'.join( ag() )
'''

'''
True Shooting 
http://en.wikipedia.org/wiki/True_shooting_percentage

	Team	James Harden, SG	Jeremy Lin, PG
11	46.32%	44.87%	40.80%
12	49.86%	49.11%	50.36%
1	48.64%	45.86%	43.66%
2	52.72%	55.41%	47.70%
3	48.50%	46.72%	50.00%
4	48.24%	43.58%	47.79%
'''
TS_Team  =[ (0,46.3),(1,49.9),(2,48.6),(3,52.7),(4,48.5),(5,48.2)]
TS_Harden=[ (0,44.9),(1,49.1),(2,45.9),(3,55.4),(4,46.7),(5,43.6)]
TS_Lin   =[ (0,40.8),(1,50.4),(2,43.7),(3,47.7),(4,50),(5,47.8)]

ag=AscGrid(autoscale=True, gshow=False, legend=True, wline ='-', hline='|', node='+')	
ag.addpxdata( TS_Team, name='Team' )
print '\n'.join( ag() )

ag=AscGrid(autoscale=True, gshow=False, legend=True, wline ='-', hline='|', node='+')	
ag.addpxdata( TS_Lin, name='Lin' )
print '\n'.join( ag() )

ag=AscGrid(autoscale=True, gshow=False, legend=True, wline ='-', hline='|', node='+')	
ag.addpxdata( TS_Harden, name='Harden' )
print '\n'.join( ag() )
'''
50 +====+====+====+====o====+
   |                        |
49 +                        +
   |                        |
48 +                        o
   |              o         |
47 +                        +
   |                        |
46 +                        +
   |                        |
45 +                        +
   |                        |
44 +                        +
   |         o              |
43 +                        +
   |                        |
42 +                        +
   |                        |
41 o                        +
   |                        |
40 +====o====+====+====+====+
   0    1    2    3    4    5

　　５０＋－－：－－：－－：－－ｏ－－＋
　　　　｜　　　　　　　　　　　　　　｜
　　４８－　　　　　　　　ｏ　　　　　ｏ
　　　　｜　　　　　　　　　　　　　　｜
　　４６－　　　　　　　　　　　　　　－
％　　　｜　　　　　　　　　　　　　　｜
　　４４－　　　　　ｏ　　　　　　　　－
　　　　｜　　　　　　　　　　　　　　｜
　　４２－　　　　　　　　　　　　　　－
　　　　ｏ　　　　　　　　　　　　　　｜
　　４０＋－－ｏ－－：－－：－－：－－＋
　　　１１　１２　　１　　２　　３　　４

[o:Lin, ｘ:Harden]　

　　５６＋－－：－－：－－：－－：－－＋
　　　　｜　　　　　　　　ｘ　　　　　｜
　　５４－　　　　　　　　　　　　　　－
　　　　｜　　　　　　　　　　　　　　｜
　　５２－　　　　　　　　　　　　　　－
　　　　｜　　　　　　　　　　　　　　｜
　　５０－　　ｏ　　　　　　　　ｏ　　－
　　　　｜　　ｘ　　　　　　　　　　　｜
％　４８－　　　　　　　　ｏ　　ｘ　　ｏ
　　　　｜　　　　　　　　　　　　　　｜
　　４６－　　　　　ｘ　　　　　　　　－
　　　　ｘ　　　　　　　　　　　　　　｜
　　４４－　　　　　ｏ　　　　　　　　ｘ
　　　　｜　　　　　　　　　　　　　　｜
　　４２－　　　　　　　　　　　　　　－
　　　　ｏ　　　　　　　　　　　　　　｜
　　４０＋－－：－－：－－：－－：－－＋
　　　１１　１２　　１　　２　　３　　４

	Team	Harden	Lin
11	46.32%	44.87%	40.80%
12	49.86%	49.11%	50.36%
1	48.64%	45.86%	43.66%
2	52.72%	55.41%	47.70%
3	48.50%	46.72%	50.00%
4	48.24%	43.58%	47.79%

　
'''   


'''
print '\n=================' 
#agg = AscGrid(legend=True, autoscale=True)
#agg.addpxdata( d_121011, name='121011' )
#agg.addpxdata( d_121117, name='121117' )
print '\n'.join( ag() )
'''	
	
def test3():
    def legendfunction(dataset):
        return '\n'.join( [ '%(symb)s: %(name)s, %(data)s'%d+ (d['yaxisr'] and ' (Y2)' or '') for d in dataset] )
        
    ops =  { 'hunit': 4
           , 'hcount': 4
           , 'wunit': 12
           , 'wcount': 5 
           , 'xlabel': 'x value'
           , 'ylabel':'sin x'
           , 'ylabelr': 'e -0.5x'
           , 'yscaleranger':True
           , 'footer':'This is a demo to show sine wave'
           , 'style':'func1'
           , 'hline': ':'
           , 'yformat':'%1.1f'
           , 'title':'Two sin waves and x/10'
           , 'header':'At the same graph'
           , 'xscalerange':(0, 10, 0.25)
           , 'yscalerange':(-1,1)  
           , 'legend': legendfunction  
           }
    f = 'math.sin(x)'  #lambda x: math.sin(x)
    f2 = 'math.exp(-0.3*x)'      #lambda x: x/10 #math.exp(-0.5*x)
    
    
    ag=AscGrid(**ops)
    ag.addpxdata(pt=f)
    #print '\n'.join( ag( ) )             
    
    
    ops2=ops.copy()
    ops2.update( { 'yscaleranger': (0,1) #-0.5,1.5)
                 , 'yformatr': '%1.2f'
                 , 'outerborder': True
                 } )
    ag2= AscGrid(**ops2)
    ag2.addpxdata(pt=f, symb='o')
    ag2.addpxdata(pt=f2, yaxisr=True)
    print '\n'.join( ag2( ) )             
    
    '''
    +-----------------------------------------------------------------------------+
    |                            Two sin waves and x/10                           |
    | At the same graph                                                           |
    |    1.0 x-------oo-o+-----------+-----------+--------o-oo-----------+ 1.00   |
    |        | x   o     o           :           :       o   : o         |        |
    |        |  x        : o         :           :           :  o        |        |
    |        |    x      :  o        :           :     o     :    o      |        |
    |    0.5 +..o..x..................................o..................+ 0.75   |
    |        |       x   :    o      :           :           :     o     |      e |
    | s      | o      x  :           :           :  o        :           |        |
    | i      |          xx     o     :           :           :       o   |      - |
    | n  0.0 o.............xx......................o.....................+ 0.50 0 |
    |        |           :    xx o   :           :           :        o  |      . |
    | x      |           :       xx  :           o           :           |      5 |
    |        |           :        o xx           :           :          o|      x |
    |   -0.5 +.........................xx.xx....o........................o 0.25   |
    |        |           :          o:       xx xx           :           |        |
    |        |           :           o        o  : xx xx xx x:           |        |
    |        |           :           : o     o   :           x xx xx xx xx        |
    |   -1.0 +-----------+-----------+--o-oo-----+-----------+-----------+ 0.00   |
    |        0           2           4           6           8          10        |
    |                                   x value                                   |
    |        o: #1, math.sin(x)                                                   |
    |        x: #2, math.exp(-0.3*x) (Y2)                                         |
    | This is a demo to show sine wave                                            |
    +-----------------------------------------------------------------------------+

    '''

def demo_data():
	
	d_121011=[(0, 5), (1, 19), (2, 65), (3, 163), (4, 221), (5, 220), (6, 234), (7, 202), 
	          (8, 156), (9, 120), (10, 62), (11, 44), (12, 16), (13, 7), (14, 5), (15, 1)]

	d_121117=[(0, 4), (1, 17), (2, 67), (3, 155), (4, 217), (5, 220), (6, 231), (7, 198), 
               (8, 161), (9, 126), (10, 65), (11, 49), (12, 15), (13, 8), (14, 6), (15, 1)]

	ops =  { 'title' :'Frequency distribution of subs2 in MM. '
           , 'header':'\nData taken from 2012.10.11 and 2012.11.17\n'
           , 'footer':('\nMost of subs2 repeat for about 3~9 times\n' )
           , 'hunit' : 5
           , 'hcount': 5
           , 'wunit' : 9
           , 'wcount': 5 
           , 'xlabel': 'Count of subs2 apparance'
           , 'ylabel': '# of subs'
           , 'yscaleranger':True
           , 'style' :'func1'
           , 'hline' : ':'
           , 'yformat' :'%1.1f'
           , 'yformatr':'%1.1f'
           , 'xscalerange':(0, 15)
           , 'yscalerange':(0, 250)  
           , 'outerborder': True
           , 'legend': True  
           }
	
	ag=AscGrid(**ops)	
	
	ag.addpxdata( d_121011, name='121011' )
	ag.addpxdata( d_121117, name='121117' )
	#print '\n'.join( ag() )
	
	print '\n=================' 
	agg = AscGrid(legend=True, autoscale=True)
	agg.addpxdata( d_121011, name='121011' )
	agg.addpxdata( d_121117, name='121117' )
	print '\n'.join( agg() )
	
	
	print '\n=================' 
	agg = AscGrid(legend=True, autoscale=True)
	agg.addpxdata( d_121011, name='121011' )
	#agg.addpxdata( d_121117, name='121117' )
	print '\n'.join( agg() )
	
	agg.addpxdata( d_121117, name='121117' )
	print '\n'.join( agg() )
	
	print '\n=================' 
	agg = AscGrid(legend=True, autoscale=True)
	agg.addpxdata( d_121117, name='121117' )
	print '\n'.join( agg() )
	
	agg.addpxdata( d_121011, name='121011' )
	print '\n'.join( agg( wunit=10) )


