t='''
  4 -+-----------------+
     |                 |
  3 -+        *        |
     |                 |
  2 -+     +  +#       |
     |                 |
  1 -+  *  *#    +# #  |
     |                 |
  0 -+--+--+--+--+--+--+
        1  2  3  4  5
'''

t='''
  4 +-----------+
    |           |
  3 +     *     |
    |           |
  2 +   +++#    |
    |      +    |
  1 + * *#  +## |
    |           |
  0 +-+-+-+-+-+-+
       1 2 3 4 5
'''

def slen(i):
    ''' Given a number, return the len of its string representation.
        slen(1) = 1
        slen(100) = 3
    '''
    return len(str(i))

def isLenOdd(i):
    '''Given a number, return true if its slen (size of string output) is odd

       True if  i= 1,   100,  12000
       False if i= 12, 1020, 401230
    '''
    return slen(i)/3 == slen(i)/3.0


class AgAxis(object):        
    def __init__(self, dir='x', start=0, end=10, inteval=1, parent=AgFrame()):
        self.interval   = interval
        self.start      = start
        self.end        = end
        self.dir        = dir
        self.markDist   = 3
        self.markSymbol = '+'
        #self.
        
    def __setattr__(self, n, v):
        if n=='dir':
            if v not in ('x','h','y','v'):
                raise ValueError, '''The attribute "dir" of AgAxis class should
                have been in ('x','h','y','v'). You gave: "%s"'''%v
        object.__setattr__(self, n, v)
        
    def getLabels(self):
        return range(self.start, self.end+self.interval, self.interval)
    labels = property(getLabels)
    
    def _output(self, dir='x', xAxisPrefix='0 '):
        '''
           Return a list:            
           ['0 +-+-+-+-+-+-+',
            '  0 1 2 3 4 5 6']
        '''
        line1= xAxisPrefix #+ #self.
        line2= ' '*len(xAxisPrefix)

        '''
            markDist= 2
            '0 +-+-+-+-+-+-+',
            '  0 1 2 3 4 5 6' 

            markDist= 2
            '0 +-+-+-+-+-+-+',
            '  10111213141516'   <== unreadable 

            markDist= 3
            '0 +--+--+--+--+--+--+',
            ' 10 11 12 13 14 15 16'

            markDist= 4
            '0 +---+---+---+---+---+---+',
            ' 100 110 120 130 140 150 160'

            
        '''
        
        #if dir in ('h','x'):
        #    for i in range(self.start, self.end+self.interval, self.interval):
                            
                
class AgSymbols(object):
    leftTop = rightTop = leftBottom = rightBottom = '+'
    leftFrame = rightFrame = '|'
    topFrame = bottomFrame = '-'
    axisMarkerY = axisMarkerX = '+'
    points = ['*','#','@','#']
    
class AgFrame(object):
    leftPadding = 12
    

                
        
    class XAxis(Axis):
        def __init__(self, start=0, end=10, inteval=1):
            Axis.__init__(self, start=0, end=10, inteval=1)
        def output(self)        
         

    X = Axis()
    Y = Axis()
    
        
    class Symbols(object):
        leftTop = rightTop = leftBottom = rightBottom = '+'
        leftFrame = rightFrame = '|'
        topFrame = bottomFrame = '-'
        axisMarkerY = axisMarkerX = '+'
        points = ['*','#','@','#']
        
    symbols = Symbols()

            