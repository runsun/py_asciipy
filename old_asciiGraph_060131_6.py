test_x = 0
test_y = 0
test_xy = 0
test_xu=0

#test_x = 1
#test_y = 1
test_xy = 1
#test_xu=1
'''
     10 -+-----------------------+
         |                       |
      8 -+                       |
 The     |                       |
  y   6 -+                       |
 Axis    |                       |
title 4 -+                       |
         |                       |
      2 -+                       |
         |                       |
         +--+--+--+--+--+--+--+--+
        20 30 40 50 60 70 80 90 100
                  x title        
'''

__history__={
    '060131_6':"possible to plot grid lines",
    '060131_5':"xaxis getPix done (checked yPixWidth)",
    '060131_4':"xaxis getPix done (haven't checked yPixWidth yet)",
    '060131_3':'clean-up',
'060131_2': 'Display x-axis correctly'
}
print
global ax

def ruler(size=40):
    r= '|'.join([      '123456789' for x in range(9)])
    r= r[:size]
    r2= ''.join([ '         %s'%x for x in range(1,9,1)])
    r2=r2[:size]
    return r + '\n'+r2

class AgErr(Exception):
    def __init__(self, **kargs):
        self.__dict__['kargs']=kargs
        #self.obj = None
        #self.errname = errname
        if kargs.has_key('obj'):
            obj = self.kargs['obj']
            self.kargs.update({
                '_xaxis': '(%s,%s,%s), markIntv= %s'%(obj.x.min,obj.x.intv,
                                                      obj.x.max,obj.x.markIntv ),
                '_yaxis': '(%s,%s,%s), markIntv= %s'%(obj.y.min,obj.y.intv,
                                                      obj.y.max,obj.y.markIntv ),
                }
            )
            '''
            ag = kargs['obj']
            self.__dict__['axesInfo']={
                'x':{'min':ag.x.min, 'max':ag.x.max,
                     'intv':ag.x.intv, 'mintv':ag.x.markIntv},
                'y':{'min':ag.y.min, 'max':ag.y.max,
                     'intv':ag.y.intv, 'mintv':ag.y.markIntv}
                }
        else: self.__dict__['axesInfo'] = {}
        '''
            
    def __str__(self):
        out = ['='*50 ]#'ERROR: '+self.errtype]
        '''
        if axesInfo:
            out+= [
            'x: (%(min)s,%(intv)s,%(max)s), markIntv=%(mintv)s'%(self.axesInfo['x']),
            'y: (%(min)s,%(intv)s,%(max)s), markIntv=%(mintv)s'%(self.axesInfo['y']),
            ]
            print 'out = ', out
        ''' 
        out +=['-'*50]    
        ks = self.kargs.keys()
        maxw = max( [ len(x) for x in ks] )
        ds = self.kargs.items()
        ds.sort()
        ds = [ '%s = %s'%(x.ljust(maxw), y) for x,y in ds ]
        out += ds
        #kss= [ x.ljust(maxw) for x in ks ]
        return '\n'+ '\n'.join(  out )
#        return '\n'+'\n'.join([ '%s = %s'%(kss[i], self.kargs[ kss[i].strip() ]) \
#                            for i in range(len(ks))])
    
def slen(i):
    ''' Given a number, return the len of its string representation.
        slen(1) = 1
        slen(100) = 3
    '''
    return len(str(i))
def srep(txt, i, n):
    '''substring replace. Replace the character in index i to n
    
    >>> a='abcdefghi'
    >>> srep(a, 3, 'X')
    'abcXefghi'
    '''
    return ''.join( [j==i and n or txt[j] for j in range(len(txt))])

def isOdd(i): return i/2 != i/2.0
def isLenOdd(i):
    '''Given a number, return true if its slen (width of string output) is odd

       True if  i= 1,   100,  12000
       False if i= 12, 1020, 401230
    '''
    return slen(i)/2!= slen(i)/2.0


class AgAxis(object):        
    def __init__( self,
                  dir   = 'x', 
                  vmin   = 0,
                  vmax   = 10,
                  intv  = 1,
                  markSym  = '+',
                  markIntv = 2,
                  axisSym  = '-',
                  title    ='Axis'):
       
        self.intv       = intv
        self.min        = vmin
        self.max        = vmax
        self.dir        = dir
        self.title      = title
        self.markIntv   = markIntv # +-+-+-+-+-+-+' ==> markIntv = 1
        self.markSym    = markSym
        self.axisSym    = axisSym
        
        self.frame       = ''  # could be '.', '-', '|' ...

    #def __setattr__(self, n, v):

    #    if n=='markIntv': print 'in __setattr__, new markIntv=',v 
    #    if n=='dir':
    #        if v not in ('x','h','y','v'):
    #            raise ValueError, '''The attribute "dir" of AgAxis class should
    #            have been in ('x','h','y','v'). You gave: "%s"'''%v
    #    object.__setattr__(self, n, v)
        '''
        if n in ('min','max','intv'):
           r = self.getScaleRange()
           if r:
              d = self.__dict__
              d['min'], d['max'] = min(r), max(r)'''
class AgXScaleUnit(object):

    '''
       class XScaleUnit(object):
           An x-axis

           +---+---+---+---+
           80  90 100 110 120

           consists of 'scale unit':
       
            ' +-' '-' '-+-' '-' '-+-' '-' '-+-' '-' '-+ '
            ' 80' ' ' ' 90' ' ' '100' ' ' '110' ' ' '120'

        Note that the width of each 'unit' is the same as
        the markIntv of the entire axis:

            width = markIntv = 3            

        more: markIntv = 5:
            
           +-----+-----+-----+-----+-----+-----+-----+
          80    90    100   110   120   130   140   150
    '''
    def __init__(self, value=0, width=None, markSym='+', axisSym='-',
                markRightShift=0,
                showLeft=1,
                showRight=1):
        if width==None:
            width = self.width = slen(value)
        else:
            self.width = width
        self.value     = value
        self.markSym   = markSym
        self.axisSym   = axisSym
        self.showLeft  = showLeft
        self.showRight = showRight

        self.reset(width=width, markRightShift=markRightShift)
        #return self.getlines()

    #def __setattr__(self, n, v):
    #    object.__setattr__(self,n,v)
    #    if n in ('value', 'width'):
    #        self.reset()
            
    def reset(self, width=None, markRightShift=0):
        '''
           Set self.line1 = '--+--'
               self.line2 = ' 90  '

           return ('--+--',' 90  ')

           For situations like these:
           |+-|+-|-+ 
            80 90 100

           |-+--|-+--|--+  
            8000 9000 10000

           where the last unit has different width, it would
           be better for the + to shift to right once to make
           the markIntv consistent:
           
           |-+|-+|-+ 
            80 90 100

           |--+-|--+-|--+  
            8000 9000 10000

           This is done by setting markRightShift=1            
        '''
        #----------------------------------------- line-1 
        if width==None: width = self.width
        sl = slen(self.value)
        w  = self.width = max(sl, self.width, width)
        #print 'in AgXScaleUnit, width = ', w
        R  = w / 2 - markRightShift
        L  = w-1-R
        ''' (r,l) = (1,1) @ w=3
                    (1,2) @ w=4
                    (2,2) @ w=5
                    (2,3) @ w=6
        '''
        #print '  markRightShift=',markRightShift,', R=',R, ', L=',L
        left = self.showLeft  and self.axisSym*L or ' '*L 
        right= self.showRight and self.axisSym*R or ' '*R
        l1= left + self.markSym + right
        
        #----------------------------------------- line-2 
        l2 = str(self.value).center(self.width)
        
        #-------------------------------------- strip spaces
        if not self.showLeft:
            ''' Convert:  '   +---'
                          ' 10000 '
                To:
                          '  +---'
                          '10000 '
            '''
            l1, l2 = l1.lstrip(), l2.lstrip()
            s = len(l2)-len(l1)
            l1 = ' '*s + l1
            
        if not self.showRight:
            ''' Convert:  '-----+     '
                          '   10000   '
                To:
                          '-----+  '
                          '   10000'
            '''
            l1, l2 = l1.rstrip(), l2.rstrip()
            s = len(l2)-len(l1)
            l1 += ' '*s
            
        self.line1, self.line2 = l1,l2
        return (l1,l2)
    
    def getlines(self):
        return self.line1, self.line2
    
def test_AgXScaleUnit():
    xu = AgXScaleUnit()
    for x in (0, 12, 123):
        for itv in (0,3,5,8):
            xu.value=x
            xu.width = itv
            xu.reset()
            print 'x=%s,markIntv=%s,'%(x,itv)
            print '\n'.join( ['|'+x+'|' for x in xu.getlines()])

                
    #print xu.getlines()
class AgAxisX(AgAxis):
    '''
       When min is one-digit wide:
           '0 +-+-+-+-+-+-+',
           '  0 1 2 3 4 5 6'
       When min is 2-digit wide:(markDist=3)
           '0 +--+--+--+--+--+--+',
           ' 10 11 12 13 14 15 16'               
       When min is 3-digit wide: (markDist=4)
           '0 +---+---+---+---+---+---+',
           ' 100 110 120 130 140 150 160'
       When min is 3-digit wide: (markDist=4)
           '0 +---+---+---+---+---+---+',
           ' 80  90  100 110 120 130 140'
       '''    
    def __init__( self,
                  xmin   = 0,
                  xmax   = 10,
                  intv  = 1,
                  markSym  = '+',
                  markIntv = 2,
                  axisSym  = '-',
                  title    ='x axis',
                  yAxis    = None):

        AgAxis.__init__( self, dir='x',
                         vmin  = xmin,
                         vmax  = xmax,
                         intv = intv,
                         markSym  = markSym,
                         markIntv = markIntv,
                         axisSym  = axisSym,
                         title    = title)
        self.yAxis=yAxis

    def getLabels(self) :    return [str(i) for i in self.scaleRange]
    def getYPerPix(self):    return (self.intv*1.0)/self.markIntv
    def getPixPerX(self):    return self.markIntv/(self.intv*1.0)
    def getScaleRange(self): return range(self.min, self.max+self.intv, self.intv)   
    def getScaleWidth(self): return self.max - self.min
    def getPixWidth(self):   return len(self.filledRange) #getFilledRange())
    def getYPixWidth(self):
        if self.yAxis:
           try:   return self.yAxis.pixWidth #getPixWidth()
           except: return 0
        else: return 0
    def getBone(self):
        return (self.axisSym*self.markIntv).join([ self.markSym for x in self.scaleRange])   
        
    def getFilledRange(self):
       ''' scaleRange  : [ 3,5,7,9,11 ] 
           markIndv= 1 :   3,-,5,-,7,-,9,-,11
           filledRange : [ 3,4,5,6....11 ] 
       '''
       r = self.scaleRange[:] #getScaleRange()[:]
       intv = (r[1]-r[0])/ ((self.markIntv+1)*1.0)
       #print 'markIntv=', self.markIntv
       out = []
       for i in r:
          out.append(i)
          d=0
          if self.markIntv>0 and i!=r[-1]:
             for j in range(self.markIntv):
                d+=intv
                out.append( i+d)
       return out
    
    labels    = property(getLabels)
    scaleRange= property(getScaleRange)
    filledRange=property(getFilledRange)
    scaleWidth= property(getScaleWidth)
    pixWidth  = property(getPixWidth)
    yPixWidth = property(getYPixWidth)
    xPerPix   = property(getYPerPix)
    pixPerX   = property(getPixPerX)
    bone      = property(getBone)

    
    def getPix(self,v):
      '''
         w/o y:

          +-----+---
          20    25   
      
        with y:
        
          |         
       0 -+-----+---
          20    25   

        which is:
        
          |         
       0 -+

           -----+---
          20    25   


      '''
      #if self.yAxis==None: adj = 1
      #else: adj=0
      fr= self.getFilledRange()
      xpp = self.xPerPix
      fr +=[ fr[-1]+xpp, fr[-1]+xpp*2]
      if self.min<=v<=self.max:         
         i=0
         while fr[i]<v: 
            #print "fr[%i]=%f, v=%f, fr[i+1]=%f"%(i,fr[i], v,fr[i+1])
            i+=1
         out=  [i,i+1][ (v-fr[i])<(fr[i+1]-v)]
         out += self.getYPixWidth() 
         return out-2 #-adj


    def reset(self, indent=0, title=None, markIntv=None):
        ''' take all available parameters, do recalculation

           | title 4 -+    min = 2000                   
           |          |    max = 8000                   
           |       2 -+    intv= 1000                   
           |          |<--------------- bone ------->                       
           |       0 -+----+----+----+----+----+----+
                    2000 3000 4000 5000 6000 7000 8000   
           |                   x title         |____|
           |        |_|                           ^--- markIntv=4
           |         ^----- indent1 = difference between line1 and line2
           |          |
           |<-------->| 
                ^--------- indent = .yAxis.pixWidth if .yAxis exists, 0 if not

        '''
        if title==None: title = self.title
        else: self.title = title
        if markIntv==None: markIntv = self.markIntv        
        markSym, axisSym = self.markSym, self.axisSym
            # markSym = '+'
            # axisSym = '-'

        #------------------------------------- find the mininum markIntv
            
        ws = [ slen(x) for x in self.scaleRange ]
            # [2,2,2,...2]      or [ 2,2,3,3,3,...3 ] or [2,2,2,...2,3]
            # => minw=2=ws[-1]         =3=ws[-1]          =2=ws[-2]

        minw = ws[-2] 
        markIntv = max( minw, markIntv )
        self.markIntv = markIntv

        '''
           Minimum markIntv:

           The width of x labels limits the possible min markIntv.
           For example:

           +--+--+--+--+--+ 
           10 11 12 13 14 15

           the min markIntv is 2. If 1, it is unreadable:
           
           +-+-+-+-+-+ 
           101112131415
                   
           But sometimes:

           +--+--+--+--+--+ 
           0  2  4  6  8  10

           the min markIntv could be 1:
           
           +-+-+-+-+-+ 
           0 2 4 6 8 10

           and still readable, even though the min-markIntv-requirement
           of 10 is 2.
           
           In situations like this, the all the xScaleUnits (except the 
           last one) could have the '+' mark shifted to the right by 1.
           This is done by setting markRightShift=1.
        '''
        self.scaleUnits = []
        for v in self.scaleRange:
            markRightShift = 0
            if (ws[-1]!=ws[-2]) and isOdd(ws[-1]) and v!=self.scaleRange[-1]:
                markRightShift = 1
            #print 'v=',v,', markRightShift=', markRightShift    
            showLeft = (v!=self.scaleRange[ 0]) and 1 or 0
            showRight= (v!=self.scaleRange[-1]) and 1 or 0
            
            xu = AgXScaleUnit(value         = v,
                              width         = markIntv,
                              markSym       = markSym,
                              axisSym       = axisSym,
                              markRightShift= markRightShift,
                              showLeft      = showLeft,
                              showRight     = showRight)
            self.scaleUnits.append( xu )

        #print 'indent = ',indent, ', yPixWidth=',self.yPixWidth, ', yAxis=',self.yAxis
        lines = [ x.getlines() for x in self.scaleUnits ]
        lines = zip(*lines)
        line1 = self.axisSym.join( lines[0])
        line2 = ' '.join( lines[1] )
        #---------------------------------------------- set indents
        indent = (self.yPixWidth or indent)-1
        
        '''
        indent1: the space preceding line1 (compared to line2)
        
           +--+   indent1 = 1
          100
          
           +--+   indent1 = 2
         1000  

        '''
        #print 'yAxis=', self.yAxis
        #print 'yAxis.pixWidth=', self.yAxis.pixWidth, ', indent = ', indent
        self.indent1 = len(line1) - len(line1.lstrip())
        #line3= title.center( len(self.bone)
                      
        lines = [ line1,
                  line2,
                  title.center( len(self.bone) )
                 ]
        lines= [' '*indent+x for x in lines]
        self.line1, self.line2, self.line3 = lines
        return '\n'.join( lines )
            

    def __call__(self,
                 title=None,
                 markIntv=None,
                 indent=0):
        '''                
           Return a list:            
           ['-+-+-+-+-+-+',
            '  0 1 2 3 4 5 6',
            '    x title    ']
            
           '''

        return self.reset(indent=indent, title=title, markIntv=markIntv)

                  
class AgAxisX_old(AgAxis):
    def __init__( self,
                  xmin   = 0,
                  xmax   = 10,
                  intv  = 1,
                  markSym  = '+',
                  markIntv = 2,
                  axisSym  = '-',
                  title    ='x axis',
                  yAxis    = None):

        AgAxis.__init__( self, dir='x',
                         vmin  = xmin,
                         vmax  = xmax,
                         intv = intv,
                         markSym  = markSym,
                         markIntv = markIntv,
                         axisSym  = axisSym,
                         title    = title)
        self.yAxis = yAxis
        
    def getLabels(self):
        return range(self.min, self.max+self.intv, self.intv)
    labels = property(getLabels)
    def getYPerPix(self): return (self.intv*1.0)/self.markIntv
    def getPixPerY(self): return self.markIntv/(self.intv*1.0)
    def getScaleWidth(self):     return self.max - self.min
    def getPixWidth(self):
       return len(self.getFilledRange())

    scaleWidth= property(getScaleWidth)
    pixWidth  = property(getPixWidth)
    xPerPix   = property(getYPerPix)
    pixPerY   = property(getPixPerY)
    
    def getScaleRange(self):
       r= range(self.min, self.max+self.intv, self.intv)   
       self.min, self.max = min(r), max(r)
       #print 'in getScaleRange, min, max = ', self.min, self.max
       return r

    def getPixRange(self):pass
        
    def getPix(self,v):
      '''   
      '''
      fr= self.getFilledRange()
      xpp = self.xPerPix
      fr +=[ fr[-1]+xpp, fr[-1]+xpp*2]
      if self.min<=v<=self.max:
         
         i=0
         while fr[i]<v: 
            #print "fr[%i]=%f, v=%f, fr[i+1]=%f"%(i,fr[i], v,fr[i+1])
            i+=1
         out=  [i,i+1][ (v-fr[i])<(fr[i+1]-v)]
         out += self.getYPixWidth() 
         return out
        
    def getYPixWidth(self):
        if self.yAxis:
           try:
              return self.yAxis.getPixWidth()
           except:
              return 0
            
    def getFilledRange(self):
       ''' span : [ 3,5,7,9,11] (== getScaleRange())
           markIndv=1 : 3,-,5,-,7,-,9,-,11
           pixRange = [ 3,4,5,6....11] 
       '''
       r = self.getScaleRange()[:]
       intv = (r[1]-r[0])/ ((self.markIntv+1)*1.0)
       #print 'markIntv=', self.markIntv
       out = []
       for i in r:
          out.append(i)
          d=0
          if self.markIntv>0 and i!=r[-1]:
             for j in range(self.markIntv):
                d+=intv
                out.append( i+d)
       return out
      
#getXPerPix
#getPixPerX
#getWidth
#getPixWidth      
    def __call__0(self,
                 title=None,
                 markIntv=None,
                 indent=3,
                 hideFirstIndent=1):
        '''
           In a graph like below:

           | title 4 -+                       
           |          |                       
           |       2 -+                       
           |          |                       
           |       0 -+--+--+--+--+--+--+--+--+
           |         20 30 40 50 60 70 80 90 100
           |                   x title
           
           |<-------->| 
                ^--------- indent that usually = y.yWidth
           
           The y axis will be 

           | title 4 -+                       
           |          |                       
           |       2 -+                       
           |          |                       
           |       0 -+
 
           So we need x to be:  (that is, hideFirstIndent)
           
           |           --+--+--+--+--+--+--+--+
           |         20 30 40 50 60 70 80 90 100
           |                   x title
           
                  
           Return a list:            
           ['-+-+-+-+-+-+',
            '  0 1 2 3 4 5 6',
            '    x title    ']
            
           '''
        '''
           When min is one-digit wide:
               '0 +-+-+-+-+-+-+',
               '  0 1 2 3 4 5 6'
           When min is 2-digit wide:(markDist=3)
               '0 +--+--+--+--+--+--+',
               ' 10 11 12 13 14 15 16'
           When min is 2-digit wide:(markDist=3)
               '0 +--+--+--+--+--+--+',
               ' 10 11 12 13 14 15 16'
               
           When min is 3-digit wide: (markDist=4)
               '0 +---+---+---+---+---+---+',
               ' 100 110 120 130 140 150 160'
           When min is 3-digit wide: (markDist=4)
               '0 +---+---+---+---+---+---+',
               ' 80  90  100 110 120 130 140'
           '''
        if title==None: title = self.title
        else: self.title = title

        if markIntv==None: markIntv = self.markIntv
        else: self.markIntv = markIntv
        
        labels, markSym, axisSym = self.labels, self.markSym, self.axisSym
            # labels  = [ 0, 1, 2 ...]
            # markSym = '+'
            # axisSym = '-' 

        #------------------------------------------------ Determine minumum markIntv
        _markIntv = slen( labels[-1] )
        if (slen(labels[-1])-slen(labels[-2])==1): _markIntv-=1 # micro adjustment

        #--------------------------------------------------------- Evaluate markIntv
            # If the user-defined self.markIntv is larger than the
            # minimum _markIntv, use the larger self.markIntv;
            # Otherwise, reset the smaller self.markIntv to _markIntv
        markIntv      = max( _markIntv, self.markIntv)
        self.markIntv = markIntv

        #--------------------------------------------- Evaluate y pixWidth for 'indent'
            # If self.yAxis exists and y.getPixWidth() is successful,
            # set the indent to it. Otherwise, leave indent as input  

        indent = (self.getYPixWidth() or indent)-1

        #--------------------------------------------------------------------  line2
        labels = [ isOdd(slen(i)) \
                   and str(i).rjust(markIntv) \
                   or str(i).center(markIntv) for i in labels]        
            # [ ' 0',  ' 1', ... '10'] 
            # ['82',   '81', ... '90'] 
            # [' 80', ' 90', ... '130'] 
            # ['100', '110', ...'1000']
        microadj = (markIntv/2==markIntv/2.0) and 1 or 0
        print 'indent=%s, labels[0]=|%s|, len(prefix)=%s'%(indent,labels[0],len(' '*(indent-len(labels[0])/2+ microadj)))
        line2 = ' '*(indent-len(labels[0])/2+ microadj)+ ' '.join( labels )

        #-------------------------------------------------------------------- line1        
        line1 = [ markSym.center(len( labels[0] ),axisSym) ] * len(labels)           
        line1= axisSym.join( line1 )
        while line1.startswith( axisSym ): line1 = line1[1:]  # strip terminal '-'
        while line1.endswith( axisSym )  : line1 = line1[:-1] # strip terminal '-'
        self.axisBone = line1 # +---+---+---+---+---+---+---+---+---+
        
        line1= hideFirstIndent and line1[1:] or ' '*(indent+1)+ line1
        
        #-------------------------------------------------------------------- line3        
        line3 = ' '*indent + title.center( len(self.axisBone) )

        return '\n'.join( (line1, line2, line3))

    def __call__(self,
                 title=None,
                 markIntv=None,
                 indent=3,
                 hideFirstIndent=1):
        '''
           In a graph like below:

           | title 4 -+                       
           |          |                       
           |       2 -+                       
           |          |                       
           |       0 -+---+---+---+---+---+---+---+
           |          80  90 100 110 120 130 140 150
           |                   x title
           
           |<-------->| 
                ^--------- indent that usually = y.scaleWidth

           An x-axis consists of 'scale unit':
           
             +-- -+-- -+-- -+-- -+-- -+-- -+-- -+
             80   90  100  110  120  130  140  150

           class XScaleUnit(object):
              value=0
              width=3
              markSym = '+'
              axisSym = '-'
              showLeft= 1
              showRight=1
              def __add__(self,v):
                  if type(v)
           The y axis will be 

           | title 4 -+                       
           |          |                       
           |       2 -+                       
           |          |                       
           |       0 -+
 
           So we need x to be:  (that is, hideFirstIndent)
           
           |           --+--+--+--+--+--+--+--+
           |         20 30 40 50 60 70 80 90 100
           |                   x title
           
                  
           Return a list:            
           ['-+-+-+-+-+-+',
            '  0 1 2 3 4 5 6',
            '    x title    ']
            
           '''
        '''
           When min is one-digit wide:
               '0 +-+-+-+-+-+-+',
               '  0 1 2 3 4 5 6'
           When min is 2-digit wide:(markDist=3)
               '0 +--+--+--+--+--+--+',
               ' 10 11 12 13 14 15 16'
           When min is 2-digit wide:(markDist=3)
               '0 +--+--+--+--+--+--+',
               ' 10 11 12 13 14 15 16'
               
           When min is 3-digit wide: (markDist=4)
               '0 +---+---+---+---+---+---+',
               ' 100 110 120 130 140 150 160'
           When min is 3-digit wide: (markDist=4)
               '0 +---+---+---+---+---+---+',
               ' 80  90  100 110 120 130 140'
           '''
        if title==None: title = self.title
        else: self.title = title

        if markIntv==None: markIntv = self.markIntv
        else: self.markIntv = markIntv
        
        labels, markSym, axisSym = self.labels, self.markSym, self.axisSym
            # labels  = [ 0, 1, 2 ...]
            # markSym = '+'
            # axisSym = '-' 

        #------------------------------------------------ Determine minumum markIntv
        _markIntv = slen( labels[-1] )
        if (slen(labels[-1])-slen(labels[-2])==1): _markIntv-=1 # micro adjustment

        #--------------------------------------------------------- Evaluate markIntv
            # If the user-defined self.markIntv is larger than the
            # minimum _markIntv, use the larger self.markIntv;
            # Otherwise, reset the smaller self.markIntv to _markIntv
        markIntv      = max( _markIntv, self.markIntv)
        self.markIntv = markIntv

        #--------------------------------------------- Evaluate y pixWidth for 'indent'
            # If self.yAxis exists and y.getPixWidth() is successful,
            # set the indent to it. Otherwise, leave indent as input  

        indent = (self.getYPixWidth() or indent)-1

        #--------------------------------------------------------------------  line2
        labels = [ isOdd(slen(i)) \
                   and str(i).rjust(markIntv) \
                   or str(i).center(markIntv) for i in labels]        
            # [ ' 0',  ' 1', ... '10'] 
            # ['82',   '81', ... '90'] 
            # [' 80', ' 90', ... '130'] 
            # ['100', '110', ...'1000']
        microadj = (markIntv/2==markIntv/2.0) and 1 or 0
        print 'indent=%s, labels[0]=|%s|, len(prefix)=%s'%(indent,labels[0],len(' '*(indent-len(labels[0])/2+ microadj)))
        line2 = ' '*(indent-len(labels[0])/2+ microadj)+ ' '.join( labels )

        #-------------------------------------------------------------------- line1        
        line1 = [ markSym.center(len( labels[0] ),axisSym) ] * len(labels)           
        line1= axisSym.join( line1 )
        while line1.startswith( axisSym ): line1 = line1[1:]  # strip terminal '-'
        while line1.endswith( axisSym )  : line1 = line1[:-1] # strip terminal '-'
        self.axisBone = line1 # +---+---+---+---+---+---+---+---+---+
        
        line1= hideFirstIndent and line1[1:] or ' '*(indent+1)+ line1
        
        #-------------------------------------------------------------------- line3        
        line3 = ' '*indent + title.center( len(self.axisBone) )

        return '\n'.join( (line1, line2, line3))


class AgAxisY(AgAxis):
    def __init__( self,
                  ymin     = 0,
                  ymax     = 10,
                  intv     = 2,
                  markSym  = '+',
                  markIntv = 1,
                  axisSym  = '|',
                  title    ='The\ny\nAxis\ntitle'):

        AgAxis.__init__( self,
                         dir      = 'y',
                         vmin     = ymin,
                         vmax     = ymax,
                         intv     = intv,
                         markSym  = markSym,
                         markIntv = markIntv,
                         axisSym  = axisSym,
                         title    = title)

        self.data=()
        

    def getPixWidth(self):
        axis = self()
        return len(axis.split('\n')[-1])
    pixWidth= property(getPixWidth)

    def getYPerPix(self):  return self.scaleHeight/(self.pixHeight*1.0)
    #def getPixPerY(self): return (self.pixHeight*1.0)/self.scaleHeight
    def getPixPerY(self): return (self.pixHeight*1.0)/self.scaleHeight
    def getScaleHeight(self):     return self.max - self.min
    def getPixHeight(self):
       return len(self.getFilledRange())

    scaleHeight= property(getScaleHeight)
    pixHeight  = property(getPixHeight)
    yPerPix    = property(getYPerPix)
    pixPerY    = property(getPixPerY)

    def getLabels(self):
        return range(self.min, self.max+self.intv, self.intv)
    labels = property(getLabels)
    
    def getScaleRange(self):
       r= range(self.min, self.max+self.intv, self.intv)   
       self.min, self.max = min(r), max(r)
       #print 'in getScaleRange, min, max = ', self.min, self.max
       return r 
       
    def getFilledRange(self):
       ''' span : [ 3,5,7,9,11]
           markIndv=2 : 3..5..7..9..11
           
       '''
       r = self.getScaleRange()[:]
       intv = (r[1]-r[0])/ ((self.markIntv+1)*1.0)
       out = []
       for i in r:
          out.append(i)
          d=0
          if self.markIntv>0 and i!=r[-1]:
             for j in range(self.markIntv):
                d+=intv
                out.append( i+d)         
       return out 
       
    def getPix(self,y):
      '''      11 -+
                   |
                   |
                9 -+
          The      |
           y       |
          Axis  7 -+
         title     |
                   |
                5 -+
                   |
                   |
                3 -+
      '''
      fr = self.getFilledRange()
      fr.reverse()
      #print 'In getPix,self.min<=y<=self.max:',self.min,y,self.max 
      #print 'In getPix,self.min<=y<=self.max:',type(self.min),y,self.max 
      if self.min<=y<=self.max:
         
         i=0
         while not ( fr[i]>=y >=fr[i+1]):
            #print "fr[i]<=y <=fr[i+1]",fr[i], y ,fr[i+1]
            i+=1
         return [i,i+1][ (y-fr[i])<(fr[i+1]-y)]   

      

       

      
    def __call__(self,
                 title      = None,
                 titleOnTop = 0,
                 markIntv   = None):
       
        if title==None: title = self.title
        else: self.title = title

        if markIntv==None: markIntv = self.markIntv
        else: self.markIntv = markIntv
        #print 'self.markSym=',type(self.markSym)
        
        '''
          4 -+-----------------+
             |                 |
          3 -+        *        |
             |                 |
          2 -+     +  +#       |
             |                 |
          1 -+  *  *#    +# #  |
             |                 |
          0 -+--+--+--+--+--+--+
                1  2  3  4  5
          4 -+-----------------+
          3 -+        *        |
          2 -+     +  +#       |
          1 -+  *  *#    +# #  |
          0 -+--+--+--+--+--+--+
                1  2  3  4  5                
        ''' 
        labels, markSym, axisSym = self.labels, self.markSym, self.axisSym
        labels = labels.reverse() or labels
            # labels  = [ 0, 1, 2 ...]
            # markSym = '+'
            # axisSym = '|' 
        w = slen( labels[0] )
        labels = [ str(i).rjust( w ) for i in labels ]        
            # [ ' 0',  ' 1', ... '10'] 
            # ['82',   '81', ... '90'] 
            # [' 80', ' 90', ... '130'] 
            # ['100', '110', ...'1000']
        #print [ type(x) for x in labels ]
        # print type(markSym), markSym
        labels = [ x+' -'+ markSym for x in labels ]
        
        for i in range(len(labels)-1,0,-1):
            for j in range(markIntv):
                labels.insert(i, axisSym.rjust( len(labels[0])))
             
        if titleOnTop:
            labels = [title] + labels
        else:
            lines = title.split('\n')
            
            w = max( [len(x) for x in lines] )

            lines = [ x.center( w ) for x in lines ]
            
            labels = [ ' '*(w+1) + x for x in labels]
            h = (len(labels)-len(lines))/2
            lh = 0
            for i in range( h, h+len(lines),1):
                x=labels[i]
                #print 'x=', x, len(x), ', w=', w, ', i=',i,', h=',h, ',h+lh=', h+lh
                #print 'lines=', lines
                labels[i] = lines[i-h] + x[w:]
                lh+=1
        #if skipLowestLine: labels.pop()
        
        return '\n'.join( labels )
    
def test_AgAxis():
    
    def t( x={'title':'x Axis', 'xAxisPrefix':'0 -'},
           y={'markIntv':1, 'titleOnTop':0, 'title':'\n'.join(['The','y','Axis','title'])}
         ):
        Y = AgAxisY(min   =y['min'],
                    max     =y['max'],
                    intv=y['intv'],
                    title   =y['title'])

        X = AgAxisX(min   =x['min'],
                    max     =x['max'],
                    intv=x['intv'],
                    title   =x['title'])

        print Y( title      = y['title'],
                 titleOnTop = y['titleOnTop'],
                 markIntv   = y['markIntv'] )
           
        print X( title      = x['title'],
                 xAxisPrefix= '      0 -'
                 )

    x={'title':'x Axis', 'xAxisPrefix':'0 -', 'min':0, 'max':10, 'intv':1}
    y={'markIntv':1, 'titleOnTop':0, 'title':'\n'.join(['The','y','Axis','title']),
       'min':0, 'max':10, 'intv':1}
    t( x,y)
    t( x, y.update({'intv':2}) or y)
    
def test_AgAxis_y():
    global ax
    def t(ymin=None, ymax=None, intv=None, markIntv=None):
        if ymin==None: ymin= ax.min
        if ymax==None: ymax = ax.max
        if intv==None: intv= ax.intv
        if markIntv==None: markIntv = ax.markIntv

        global ax
        
        print '-'*50
        print "AgAxis(min=%s, max=%s, intv=%s)"%(ymin,ymax,intv), ' markIntv=',markIntv        
        axx= ax(markIntv=markIntv).splitlines()
        fg = ax.getFilledRange()
        fg.reverse()
        axx= [ x+'  i=%i, y=%s, getPix(y)=%s'%(i, fg[i], ax.getPix(fg[i])) for i,x in enumerate(axx)]
        print '\n'.join(axx) 

    def gothru():
       print 'ymin, ymax, intv=', ax.min, ax.max, ax.intv
       print 'getScaleRange:', ax.getScaleRange()
       print 'getFilledRange:', ax.getFilledRange()
       #for i in ax.getScaleRange():
           #print 'y=%i, pix=%i'%(i,ax.getPix(i))
       
    mi, mx, intv = 3,10,2
    ax = AgAxisY(ymin=mi, ymax=mx, intv=intv)   
    ax.markIntv=0 ; t();gothru(); #t(mi,mx,intv) ; gothru()
    ax.markIntv=1 ; t();gothru(); #t(mi,mx,intv) ; gothru()
    ax.markIntv=2 ; t();gothru(); #t(mi,mx,intv) ; gothru()
    ax.markIntv=3 ; t();gothru(); #t(mi,mx,intv) ; gothru()
    ax.markIntv=4 ; t();gothru(); #t(mi,mx,intv) ; gothru()
    '''    
    for i in range(ax.min, ax.max+ax.intv, ax.intv):
        print 'y=%i, pix=%i'%(i,ax.getPix(i))

    ax.markIntv=1    
    t(0,10,1)
    for i in range(ax.min, ax.max+ax.intv, ax.intv):
        print 'y=%i, pix=%i'%(i,ax.getPix(i)), ', ax.yPerPix=',ax.yPerPix

        
    ax.markIntv=2    
    t(0,10,1)
    for i in range(ax.min, ax.max+ax.intv, ax.intv):
        print 'y=%i, pix=%i'%(i,ax.getPix(i))

    ax.markIntv=3   
    t(0,10,1)
    for i in range(ax.min, ax.max+ax.intv, ax.intv):
        print 'y=%i, pix=%i'%(i,ax.getPix(i))
    '''    
    print 'ax.markIntv=', ax.markIntv
    print 'ax.scaleHeight=',ax.scaleHeight
    print 'ax.pixHeight=',ax.pixHeight
    print 'ax.yPerPix=',ax.yPerPix
    '''
    t(10,100,10)
    t(100,1000,100)
    t(80, 150, 10)
    t(34,56,2, markIntv=0)
    t(1000,10000,1000)
    t(9000,10000,100, markIntv=2)
    '''
    
def test_AgAxis_x():
    def t(min=0, max=10, intv=1, markIntv=2, xs=()):
        ax = AgAxisX(xmin=min, xmax=max, intv=intv, markIntv=markIntv)
        ax.yAxis = AgAxisY(ymin=0, ymax=10, intv=2)
        print '\n\n'+'#'*60
        print "AgAxis(min=%s, max=%s, intv=%s, markIntv=%s, xs=%s)"%(min,max,intv, markIntv, xs)
        
        #print '|'.join([ ''.join([str(i) for i in range(1,10,1)]) for x in range(6)])
        #print '|'.join([ ' '*9 for x in range(6)])
        ax(hideFirstIndent=0)
        ps = list( ' '*(ax.yAxis.pixWidth-1)+ ax.bone) 
        for x in xs:
            print 'ax.getPix(x)=', ax.getPix(x), ', len(ps)=', len(ps)
            ps[ ax.getPix(x) ] = 'o'

        print ax.indent1*' '+''.join(ps)            
        print ax(hideFirstIndent=0)
        #print ruler( len(ax.bone) + 2 )
        #print 'getFilledRange():',ax.getFilledRange()
        for i in range(len(ax.scaleRange)):
            v= ax.scaleRange[i]
            #print 'scale:',v,', pix:', ax.getPix( v )
        #print 'markIntvs = ',[x.width for x in ax.scaleUnits] 
        #print 'ax.markIntv = ', ax.markIntv     
    t(0,10,1, xs=(2,9))
    t(0,10,1,markIntv=1,xs=(3,10))
    t(0,10,1,markIntv=0,xs=(0,5))
    t(0,10,1,markIntv=6)
    
    '''     '''   
    t(10,100,10,xs=(11,20,93,100))
    t(20,110,10,xs=(25,109,80))
    t(100,1000,100,xs=(150,900))
    t(80, 150, 10,xs=(90,100,143))
    t(80, 150, 10, markIntv=5,xs=(90,100,143))
    t(80, 150, 10, markIntv=8,xs=(90,100,143))
    t(0,56,2, xs=(10,21,36,45))
    t(1000,10000,1000, xs= (1905,2345,9500))
    t(9000,10100,100, xs=(9200, 9300,10000))

    
    '''
    AgAxis(min=0, max=10, intv=1)
    0 -+-+-+-+-+-+-+-+-+-+-+
       0 1 2 3 4 5 6 7 8 9 10
             x title
             
    AgAxis(min=10, max=100, intv=10)
    0 -+--+--+--+--+--+--+--+--+--+
      10 20 30 40 50 60 70 80 90 100
                x title
                
    AgAxis(min=100, max=1000, intv=100)
    0 -+---+---+---+---+---+---+---+---+---+
      100 200 300 400 500 600 700 800 900 1000
                     x title
                     
    AgAxis(min=80, max=150, intv=10)
    0 -+---+---+---+---+---+---+---+
       80  90 100 110 120 130 140 150
                 x title
                 
    AgAxis(min=0, max=56, intv=2)
    0 -+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
       0  2  4  6  8 10 12 14 16 18 20 22 24 26 28 30 32 34 36 38 40 42 44 46 48 50 52 54 56
                                            x title
                                            
    AgAxis(min=1000, max=10000, intv=1000)
    0 -+----+----+----+----+----+----+----+----+----+
     1000 2000 3000 4000 5000 6000 7000 8000 9000 10000
                          x title
                          
    AgAxis(min=9000, max=10000, intv=100)
    0 -+----+----+----+----+----+----+----+----+----+----+
     9000 9100 9200 9300 9400 9500 9600 9700 9800 9900 10000
                            x title
    '''
                

class Data(object):
    points = []  # [[3,5], [4,10], ...]
    label= 'plot_data'
    symbol= '+'
    
class AsciiGraph(object):

    def __init__(self,
                 xGridSym='|',
                 yGridSym='-',
                 xyIntcSym = '.',

                 xMin   = 0,
                 xMax   = 10,
                 xIntv  = 1,
                 xMarkSym  = '+',
                 xMarkIntv = 2,
                 xAxisSym  = '-',
                 xTitle    ='x axis',
                 
                 yMin   = 0,
                 yMax   = 10,
                 yIntv  = 2,
                 yMarkSym  = '+',
                 yMarkIntv = 1,
                 yAxisSym  = '|',
                 yTitle    ='The\ny\nAxis\ntitle'
                 ):
                     
        self.xGridSym=xGridSym
        self.yGridSym=yGridSym
        self.xyIntcSym=xyIntcSym
        
        self.grid = {} # {(3,5):'*', (4,6):'*'...}
        self.lines = [ ] # list of Data instances
        self.frame  = []
        self.autoRangeX = 0
        self.autoRangeY = 0

        self.y = AgAxisY(
                    ymin     = yMin,
                    ymax     = yMax,
                    intv     = yIntv,
                    markSym  = yMarkSym,
                    markIntv = yMarkIntv,
                    axisSym  = yAxisSym,
                    title    = yTitle
                    )        
        self.x = AgAxisX(
                    xmin     = xMin,
                    xmax     = xMax,
                    intv     = xIntv,
                    markSym  = xMarkSym,
                    markIntv = xMarkIntv,
                    axisSym  = xAxisSym,
                    title    = xTitle,
                    yAxis    = self.y
                    )
        self.x.yAxis = self.y
        
        
    #def load(self, data= (( 3,5,6,8)
    def _initCellArray(self):
       ''' Using the range of x and y axis, setup an array
           .cells which is a dict looking like: { (3,5): '+' }
            such that plotting a point (3,5) will be
           .cells[3,5] = '+'           
       '''
    def addDataLine(self, x, y, label='', symbol='*'): 
      '''
      '''
      d = Data()      
      d.points =[ list(m) for m in zip( x,y ) ]
      d.label= label and label or 'data-%s'%len(self.lines)
      d.symbol=symbol
      self.lines.append( d )
      
    def inPlotRange(self,x,y=None):
      if type(x) in [tuple, list]: x,y = x[:2]
      return (self.x.min <= x <= self.x.max) and \
             (self.y.min <= y <= self.y.max)
       
    def resetFrame(self):
      X,Y = self.x,self.y      # Axes  
      x   = X().splitlines()   # out string of X
      ys  = Y().splitlines()   # out string of Y, and split
      yw  = len(ys[-1])        # width of Y axis (including title)
      gw  = len( X.bone) #X.line1.strip())   # frame width
      ys[0] += '-'* (gw-2)+'+' 
      ys  = [ys[0]]+ [ x+ ' '*(gw-2)+'|' for x in ys[1:-1]]+ [ys[-1]] 
      xs  = X(indent = yw-1 ).splitlines()
      ys[-1] += xs[ 0][yw:]
      
      self.frame = ys+xs[1:]
      self._yWidth = yw

      #for xx in X.getScaleRange()[1:-1]:
      #    for yy in Y.getFilledRange()[1:-1]: #range(lei(Y.getFilledRange()[1:-1])):
      #        self.plotPoint(xx,yy, '.') 
      #if X.frame:
          
      return self.frame

      #return   self.frame , yw) 
    def getpix(self,c,r):
       return self.frame[r][c]
      
    def plotLine(self, (x1,y1),(x2,y2), symbol='o'):

      print 'x1,y1,x2,y2 =',x1,y1,x2,y2 
      
      x1,x2= self.x.getPix(x1),self.x.getPix(x2)
      y1,y2= self.y.getPix(y1),self.y.getPix(y2)
      
      print 'in plotLine, x1,y1,x2,y2 =',x1,y1,x2,y2 
                     
      slope = (y2-y1)/( 1.0*(x2-x1))
      intc  = y1 - slope* x1
      for x in range(x1,x2+1,1):
         y = int(slope*x+ intc)
         #print '  looping in plotLine, x,y=',x,y
         self.plotPix((x,y),symbol)           
      
    def plotGridline(self, symbol='.'):pass
        
    def plotPoint(self, (x,y),symbol='o'):
      #print 'in plotPoint, x,y=(%s,%s)'%(x,y)  
      if self.inPlotRange(x,y):
         #print 'plotting point (', x,', %4.1f'%y,')' ,        
         x,y = self.x.getPix(x), self.y.getPix(y)
         #print 'pix =>(',x,',',y, ')',
         self.plotPix( (x,y), symbol)
      else:
         print '... (%s,%s) is not in range( %s..%s, %s..%s)'%(
               x,y, self.x.min, self.x.max, self.y.min, self.y.max)
         pass
         
      return self.frame
      
    def plotPix(self,(x,y),symbol='.'):
       #print 'plotPix(%s,%s)'%(x,y) #, ', len frame =', len(self.frame)
       #print 'type(frame)=', type(self.frame)
       self.grid[ x,y ] = symbol

    def plotXGrid(self, symbol='.'):
        #print '\nin plotXGrid:'
        #print 'self.x.getScaleRange():',self.x.getScaleRange()
        #print 'self.y.getFilledRange():',self.y.getFilledRange()
        #for x in self.x.getScaleRange()[1:-1]:
        for x in self.x.scaleRange[1:-1]:    
           for y in self.y.getFilledRange()[1:-1]:
                self.plotPoint((x,y), symbol)

    def plotYGrid(self, symbol='.'):
        #print '\nin plotXGrid:'
        #print 'self.x.getScaleRange():',self.x.getScaleRange()
        #print 'self.y.getFilledRange():',self.y.getFilledRange()
        #for x in self.x.getScaleRange()[1:-1]:
        for y in self.y.getScaleRange()[1:-1]:    
           for x in self.x.filledRange[1:-1]:
                self.plotPoint((x,y), symbol)

    def plotXYGridIntersections(self, symbol='+'):
        for x in self.x.scaleRange[1:-1]:    
           for y in self.y.getScaleRange()[1:-1]:
                self.plotPoint((x,y), symbol)
            
    def __call__(self):
        
      self.resetFrame()

      if self.xGridSym: self.plotXGrid( self.xGridSym )
      if self.yGridSym: self.plotYGrid( self.yGridSym)
      if self.xyIntcSym: self.plotXYGridIntersections( self.xyIntcSym )
      
      fr = self.frame[:]
      fr = [ list(x) for x in fr ]
      for x,y in self.grid:
          fr[y][x]= self.grid[x,y]          

      fr = [ ''.join(x) for x in fr ]          
      #for x,y in self.grid:
      #    s = self.grid[x,y]
      #    self.frame[y]= self.frame[y][:x] + s +self.frame[y][(x+1):]
      #print 'len frame = ',len(self.frame)
      #xpp, ypp = self.xPerPix, self.yPerPix
      #def plotpoint( x,y,symbol='o'):
      '''
          
      '''
      ''' Given (x,y)'''
      #if self.inPlotRange(x,y):
      '''
         xx = int( (x-self.x.min)/xpp + yw )-1 
         yy = int( len(frame) -3 - (y-self.y.min)/ypp  ) 
         frame[yy]= frame[yy][:xx] + symbol +frame[yy][(xx+1):]
         '''
     
      '''
      self.plotLine( (100,50),(1100,50),'.')      
      self.plotLine( (100,30),(1100,30),'.')      
      self.plotPoint( (100,10) )
      self.plotPoint( (300,30) )
      self.plotPoint( (500,50) )
      self.plotPoint( (700,70) )
      '''
      #for p in data:
      #    frame, yw= self.plotPoint( p) #plotpoint( p[0],p[1] )
          
      return '\n'.join( fr )
                     
def test_AsciiGraph():
    ag= AsciiGraph()
    ag.x.min, ag.x.max, ag.x.intv, ag.x.markIntv = 20, 60, 5, 5
    ag.y.min, ag.y.max, ag.y.intv, ag.y.markIntv = 10,80,20,2
    #ag.xyIntcSym='0'
    #ag.x.markSym='0'
    #ag.y.markSym='0'
    
    def o(ag):
       print 'ag.y.min= %s, ag.y.max= %s, ag.y.intv=%s, ag.y.markIntv=%s'%(ag.y.min, ag.y.max, ag.y.intv, ag.y.markIntv)
       print 'ag.x.min= %s, ag.x.max= %s, ag.x.intv=%s, ag.x.markIntv=%s'%(ag.x.min, ag.x.max, ag.x.intv, ag.x.markIntv)
       print ag()
       print ruler(70)
       
    o(ag)

    #ag.y.min, ag.y.max, ag.y.intv, ag.y.markIntv = 10,80,20,2
    ag.x.min, ag.x.max, ag.x.intv, ag.x.markIntv = 10, 100,20,5
    #o(ag)
    data = ( (20,20), (30,40),(60,80),(50,30))
    #for x,y in data:
    #    print 'x,y=', x,',',y
    #    ag.plotPoint( (x,y))
   #self.plotLine( (120,70),(1090,70),'.')
    #o(ag)
   
    '''
    for i in range(10):
       print '-'*60, i
       ag.x.min, ag.x.max, ag.x.intv, ag.x.markIntv = 100, 1000,200,i
       o(ag)
       print ' 123456789'*6 #.join( [str(x) for x in range(1,10,1)])
       print '|         '*6 
     '''  
    #o(ag)
    
if __name__=='__main__':
    #test_AgAxis()
    if test_y: test_AgAxis_y()
    if test_x: test_AgAxis_x()
    if test_xy: test_AsciiGraph()
    if test_xu: test_AgXScaleUnit()