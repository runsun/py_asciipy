# -*- coding: utf-8 -*-

import pprint,math
import inspect 
from copy import deepcopy  # deepcopy used in AscGrid.data 



def lineSetter(obj, attrprefix, args):
    args=list(args[0]) if (type(args[0])==list
               or type(args[0])==tuple) else list(args)
    filled2=False
    while len(args)>0:
        a=args.pop(0)
        if type(a)==bool: obj.__dict__[ attrprefix+'show']=a
        elif filled2: obj.__dict__[ attrprefix+'node']=a
        else:
            obj.__dict__[ attrprefix+'line']=a
            filled2=True


def blockSetter(obj, blockname='b', args=()):
    for pos in ('t', 'r', 'b', 'l'):
        lineSetter(obj, attrprefix=blockname+pos, args=args)

class GridCell(object):
    def __init__(self, text='',w=0,h=0):
        self.__dict__['w']=w
        self.__dict__['h']=h
        self.__dict__['text']=text
        def getw(self): return self.__dict__['w']
        def setw(self,v): __dict__['w']=v
        w=property(getw,setw)  
        def geth(self): return self.__dict__['h']
        def seth(self,v): __dict__['h']=v
        h=property(geth,seth)  
        def gettext(self): return self.__dict__['text']
        def settext(self,v): __dict__['text']=v
        text=property(gettext,settext)  

class AscGrid(object):
    '''

        >> ac= AscGrid()

        .corners = ['+','+','+','+']
        .lines = []

        ac.w
        ac.h



        ac.margin  .top  .right  .bottom  .left

        ac.border.top
                      .linesymb
                      .intersymb
                      .size

        ac.grid.vert|hori
                    .show
                    .symb

        ac.grid.inter
                    .show
                    .symb

        widths=None  # auto calc
        widths=[ 10,20,12 ...]

        printout :
            lines:[ ]


        '''
    def __init__(self, **kargs):
        #self.symbols={'default':{}}

        self.styles={ 'default': {
                        'style':'default'
                        , 't_align':'center'
                        , 'gshow': True
                        , 'corner': ''
                        , 'wline' : '-', 'hline':':', 'node' : '+'
                        #============ border                        
                        , 'btshow': False, 'btline': '=', 'btnode': 'o'
                        , 'brshow': False, 'brline': '|', 'brnode': 'o'
                        , 'bbshow': False, 'bbline': '=', 'bbnode': 'o'
                        , 'blshow': False, 'blline': '|', 'blnode': 'o'

                        }

                       , 'square':{ 'corner':('#', '#', '#', '#')
                              , 'wline':'-', 'hline':'|', 'node'  :'+'
                              , 'btshow':True, 'btline':'#' , 'btnode':'#'
                              , 'brshow':True, 'brline':'#' , 'brnode':'#'
                              , 'bbshow':True, 'bbline':'#' , 'bbnode':'#'
                              , 'blshow':True, 'blline':'#' , 'blnode':'#'
                            }

                        , 'func1':{'wline':'.', 'hline':' ', 'node':'.'
                                  , 'btshow':True, 'btline':'-' , 'btnode':'-'
                                  , 'brshow':True, 'brline':'|' , 'brnode':'+'
                                  , 'bbshow':True, 'bbline':'-' , 'bbnode':'-'
                                  , 'blshow':True, 'blline':'|' , 'blnode':'+'
                                  , 'corner':['+','+','+','+']
                               }
                        }  ## .styles        
        ''','line1':{ 'corner':('┌','┐','┘','└')
                              ,'btshow':True, 'btline':'─' ,'btnode':'┬'
                              ,'brshow':True, 'brline':'│' ,'brnode':'┤'
                              ,'bbshow':True, 'bbline':'─' ,'bbnode':'┴'
                              ,'blshow':True, 'blline':'│' ,'blnode':'├'
                              ,'node'  :'┼', 'wline' :'─','hline':'│'
                            }
                       '''
        self.ops={    'wunit' : 5
                    , 'wcount': 4
                    , 'wunits': []
                    , 'hunit' : 2
                    , 'hcount': 3
                    , 'hunits': []
                    , 'title' : '' #[title: AscGrid Demo]'
                    , 'header': '' #This line is ".header"'
                    , 'footer': '' #This line is ".footer"'

                    , 'pxdataset':[]
                    #, 'merges':{}
                    , 'style': 'default'
                    ## dataset:
                    ## [{symb:'o',name:'test',data:[(2,3),(3,5)...]}
                    ## ,{symb:'#',name:'test2',data:[(3,6),(5,8)...]}
                    ## ]

                    #,'margin':BlockProperties()   # TODO
                    }

        ##===================================================================
        ## Load default style             
        '''
        self.ops.update( self.styles['default'] )

        ## Try to load customized style if 'style="xxx"' is given in kargs
        ## and "xxx" is predefined as a style name in self.styles["xxx"].
        ##
        if 'style' in kargs :
            if not kargs['style'] in self.styles:
                kargs['style']='default'
            else:
                self.ops.update( self.styles[ kargs['style'] ] )
        '''
        kargs=self.__set_ops__(**kargs)
        self.ops.update(kargs)
        self.__dict__.update(self.ops)
        self.pxdataset=deepcopy(self.pxdataset)
        self.__dict__['__lines__']=[]
        self.__dict__['__merges__']={}
        self.draw()
        

    def setstyle_byname(self, stylename):
        if stylename in self.styles:
            style=self.styles[stylename]
            self.ops.update(style)
            self.__dict__.update(style)
            self.__dict__['style']=stylename
    def getstyle(self, name):
        return self.__dict__['style']
    style=property(getstyle, setstyle_byname)
    #==============================================
    def getlines(self): return self.__dict__['__lines__'] # deepcopy(self.__dict__['__lines__']) #self.draw()
    lines=property(getlines)
    #============================================== border 
    def getbt(self): return [ self.btshow, self.btline, self.btnode]
    def setbt(self, *a): lineSetter(self, attrprefix='bt', args=a)
    bt=property(getbt, setbt)
    #------------------------------------         
    def getbr(self): return [ self.brshow, self.brline, self.brnode]
    def setbr(self, *a): lineSetter(self, attrprefix='br', args=a)
    br=property(getbr, setbr)
    #------------------------------------         
    def getbb(self): return [ self.bbshow, self.bbline, self.bbnode]
    def setbb(self, *a): lineSetter(self, attrprefix='bb', args=a)
    bb=property(getbb, setbb)
    #------------------------------------         
    def getbl(self): return [ self.blshow, self.blline, self.blnode]
    def setbl(self, *a): lineSetter(self, attrprefix='bl', args=a)
    bl=property(getbl, setbl)
    #==============================================
    def getb(self): return (self.bt, self.br, self.bb, self.bl)
    def setb(self, *a): blockSetter(self, blockname='b', args=a)
    b=property(getb, setb)
    #==============================================
    def getDataNames(self):
        return [ x['name'] for x in self.pxdataset ]
    datanames=property(getDataNames)
    #==============================================
    def getwidth(self):
        return self.wunits and sum(self.wunits,1) or self.wunit*self.wcount+1
    width=property(getwidth)
    def getheight(self):
        return self.hunits and sum(self.hunits,1) or self.hunit*self.hcount+1
    height=property(getheight)
    #==============================================
    def getwunits(self): return self.__dict__['wunits']
    def setwunits(self,lis): 
        self.wcount = len(lis)
        self.__dict__['wunits']= lis
    wunits = property( getwunits, setwunits)    
    def gethunits(self): return self.__dict__['hunits']
        #x= self.__dict__['wunits']
        #if not x: x = self.__dict__['wunits'] = [self.wunit]*self.wcount
        #return x
    def sethunits(self,lis): 
        self.hcount = len(lis)
        self.__dict__['hunits']= lis
    hunits = property( gethunits, sethunits)    
    #def getcells(self):
                
    def __set_ops__ (self, **kargs):
        '''
            Set kargs properly before it is sent to update the ops
            This feature might not need to be a class function
        '''
        self.ops.update(self.styles['default'])

        ## Try to load customized style if 'style="xxx"' is given in kargs
        ## and "xxx" is predefined as a style name in self.styles["xxx"].
        ## 
        if 'style' in kargs :
            if not kargs['style'] in self.styles:
                kargs['style']='default'
            else:
                self.ops.update(self.styles[ kargs['style'] ])
        ## The name of style that is in use can be obtained:
        ## 
        ## ag.AscGrid()
        ## ag.style ==> 'default'
        ## ag(style='xxx')
        ## ag.style ==> 'xxx' (if 'xxx' is found in self.styles) or 'default'
        #====================================================================

        return kargs

    def merge(self, pt1, pt2, *args):
        '''
        
        >>> ag=AscGrid( title='', header='', footer='', wunit=6, wcount=5, hcount=8 )
        >>> lines = ag( wunits = [4,4,20,4,4])
        >>> lines = ag( hunits = [2,2,2,2,8,2,2,2])
        >>> ag.merge( (1,1),'c', (2,1), 'c', (-1,1),'c' )  # title, header, footer
        >>> ag.merge( (5,1),(-4,1), (5,2),(-4,2) )         # txl, scl 
        >>> ag.merge( (5,-2),(-4,-2), (5,-1),(-4,-1) )     # txr, scr
        >>> ag.merge( (3,3),(3,-3), (4,3),(4,-3) )         # txt, sct
        >>> ag.merge( (-2,3), (-2,-3), (-3,3), (-3,-3) )   # txb, scb
        >>> ag.merge( (3,1),'r1c1', (3,-2), 'r1c1', (-3,1), 'r1c1', (-3,-2), 'r1c1')
        >>> ag.addpxdata( symb= 'title', pt=(1, 16) ) 
        >>> ag.addpxdata( symb= 'header', pt=(3, 16) ) 
        >>> ag.addpxdata( symb= 'txt', pt=(5, 16) ) 
        >>> ag.addpxdata( symb= 'sct', pt=(7, 16) ) 
        
        >>> ag.addpxdata( symb= 'scb', pt=(-6, 16))
        >>> ag.addpxdata( symb= 'txb', pt=(-4, 16))
        >>> ag.addpxdata( symb= 'footer', pt=(-2, 16) ) 
        >>> ag.addpxdata( symb= 'grid', pt=(12,16))
        
        >>> for i,c in enumerate('txl'):
        ...   ag.addpxdata( symb= c, pt=( 11+i, 2 ) )
        >>> for i,c in enumerate('scl'):
        ...   ag.addpxdata( symb= c, pt=( 11+i, 6 ) )
        
        >>> for i,c in enumerate('txr'):
        ...   ag.addpxdata( symb= c, pt=( 11+i, -3 ) )
        >>> for i,c in enumerate('scr'):
        ...   ag.addpxdata( symb= c, pt=( 11+i, -7 ) )
        
        >>> print '\\n'.join( ag() )
        +---+---+-------------------+---+---+
        :               title               :
        +---+---+-------------------+---+---+
        :               header              :
        +---+---+-------------------+---+---+
        :       :       txt         :       :
        +       +-------------------+       +
        :       :       sct         :       :
        +---+---+-------------------+---+---+
        :   :   :                   :   :   :
        :   :   :                   :   :   :
        : t : s :                   : s : t :
        : x : c :       grid        : c : x :
        : l : l :                   : r : r :
        :   :   :                   :   :   :
        :   :   :                   :   :   :
        +---+---+-------------------+---+---+
        :       :       scb         :       :
        +       +-------------------+       +
        :       :       txb         :       :
        +---+---+-------------------+---+---+
        :               footer              :
        +---+---+-------------------+---+---+

        
        '''
        #print 'Entered merge, args: ',args
        if len(args)%2==1: args.pop()   # need pairs of points, so if one single out, drop it
        areas = [ (args[i], args[i+1]) for i,p in enumerate(args) if i%2==0]
        areas = [( pt1, pt2)]+ areas
        #print 'In merge, areas= ',areas
        for p1,p2 in areas:
            
            if type(p2)==str:      # 'r1', 'c2', 'r1c2'
                if p2=='c':  dh,dw = 0, self.wcount-p1[1]
                elif p2=='r': dh,dw = self.hcount-p1[0], 0
                else:
                    p2= p2.split('c')  # ['r1'], ['','2'], ['r1','2']                         
                    p= p2.pop()        # p='r1' ,'2', '2', leaving: p2= [],[''],['r1] 
                    if len(p2)>0:     # [''] or ['r1']
                          p2= p2.pop()
                          if p2: 
                              dh = int(p2.replace('r',''))
                          else:
                              dh = 0    
                          dw = int( p )
                    else:
                          dh = int(p.replace('r',''))
                          dw =  0 
                p2 = (p1[0]+dh, p1[1]+dw)    
            
            ## Allow negative coordination:       
            if p1[0]<0: p1 = ( p1[0] + self.hcount+1, p1[1] )
            if p1[1]<0: p1 = ( p1[0], p1[1]+ self.wcount+1 )
            if p2[0]<0: p2 = ( p2[0] + self.hcount+1, p2[1] )
            if p2[1]<0: p2 = ( p2[0], p2[1]+ self.wcount+1 )
            
            #if p2<0: p2 = self.width + p2 -5  
            #print 'p1,p2 = ', (p1,p2)
            self.__dict__['__merges__'][p1]= p2
        
        
    def getmerges(self): return self.__dict__['__merges__']
    merges = property(getmerges)   
    def drawmerges(self):
        '''
            >>> ag = AscGrid()
            >>> print '\\n'.join(ag(title='', header='',footer=''))
            +----+----+----+----+
            :    :    :    :    :
            +----+----+----+----+
            :    :    :    :    :
            +----+----+----+----+
            :    :    :    :    :
            +----+----+----+----+
            
            >>> ag.merge( (1,1),(1,2),   
            ...           (2,1),(3,1),
            ...           (1,4),(3,4),
            ...           (2,3),(3,3) )
            >>> print '\\n'.join(ag())
            +----+----+----+----+
            :         :    :    :
            +----+----+----+    +
            :    :    :    :    :
            +    +----+    +    +
            :    :    :    :    :
            +----+----+----+----+
            
            >>> ag.merges            # doctest: +NORMALIZE_WHITESPACE
            {(2, 3): (3, 3), 
             (1, 4): (3, 4), 
             (1, 1): (1, 2), 
             (2, 1): (3, 1)}
            
            
            >>> ag = AscGrid()
            >>> ag.merge( (2,2),(3,3),(1,4),(3,4) )
            >>> print '\\n'.join(ag(title='', header='',footer='', hcount=4))
            +----+----+----+----+
            :    :    :    :    :
            +----+----+----+    +
            :    :         :    :
            +----+         +    +
            :    :         :    :
            +----+----+----+----+
            :    :    :    :    :
            +----+----+----+----+
            
            
            >>> ag = AscGrid()
            >>> ag.merge( (1,1),'c1',   
            ...           (2,1),'r1',
            ...           (2,2),'r1c1',
            ...           (1,4),'r' )
            >>> print '\\n'.join(ag(title='', header='',footer='', hcount=4))
            +----+----+----+----+
            :         :    :    :
            +----+----+----+    +
            :    :         :    :
            +    +         +    +
            :    :         :    :
            +----+----+----+----+
            :    :    :    :    :
            +----+----+----+----+


            >>> ag = AscGrid()
            >>> lines = ag( title='', header='',footer='', wunits=[4,3,8,3,4], hunits=[2,3,4,2])
            >>> print '\\n'.join(ag())
            +---+--+-------+--+---+
            :   :  :       :  :   :
            +---+--+-------+--+---+
            :   :  :       :  :   :
            :   :  :       :  :   :
            +---+--+-------+--+---+
            :   :  :       :  :   :
            :   :  :       :  :   :
            :   :  :       :  :   :
            +---+--+-------+--+---+
            :   :  :       :  :   :
            +---+--+-------+--+---+


            >>> ag.merge( (1,1),'c1',   
            ...           (2,1),'r1',
            ...           (2,2),'r1c1',
            ...           (1,4),'r3' )
            >>> ag.merges                       # doctest: +NORMALIZE_WHITESPACE
            {(1, 4): (4, 4), 
             (1, 1): (1, 2), 
             (2, 1): (3, 1), 
             (2, 2): (3, 3)}
            >>> print '\\n'.join(ag())
            +---+--+-------+--+---+
            :      :       :  :   :
            +---+--+-------+  +---+
            :   :          :  :   :
            :   :          :  :   :
            +   +          +  +---+
            :   :          :  :   :
            :   :          :  :   :
            :   :          :  :   :
            +---+--+-------+  +---+
            :   :  :       :  :   :
            +---+--+-------+--+---+

            >>> ag.merge( (2,2), (3,4) )  
            >>> print '\\n'.join(ag())
            +---+--+-------+--+---+
            :      :       :  :   :
            +---+--+-------+  +---+
            :   :             :   :
            :   :             :   :
            +   +             +---+
            :   :             :   :
            :   :             :   :
            :   :             :   :
            +---+--+-------+  +---+
            :   :  :       :  :   :
            +---+--+-------+--+---+




        '''
        wus, hus = self.wunits, self.hunits
        if not wus: wus= [self.wunit]*self.wcount
        if not hus: hus= [self.hunit]*self.hcount
        ws= self.width
        #print 'wus = ', wus, ', ws=', ws
        lines = self.lines
        covered=[]
        #print 'merges = ', self.merges 
        '''
            .merges = { (1,2):(1,1) }  
        '''
        #def makeline(head, symb, unitsizes, node):
        #    return head+ ''.join([ (symb*(us-1)+node) for us in unitsizes])
        merge_p1_list = self.merges.keys()
        merge_p1_list.sort()
        for p1 in merge_p1_list:  #self.merges.items():
            p2 = self.merges[p1]
            if p1 not in covered:
                r1,c1= p1
                r2,c2= p2
                #print 'r1,c1,r2,c2= (', (r1,c1), '), (', (r2,c2), ')' 
                rowbeg = sum( [ h for h in hus[:r1-1] ])+1
                rowend = sum( [ h for h in hus[:r2] ])
                
                #print 'row range: %s, %s'%(rowbeg,rowend)
                for r in range(rowbeg,rowend):  
                    #cellidx = [ range( p1[1], p2[1])]
                    colbeg = sum( [ w for w in wus[:c1-1] ])+ 1
                    colend = sum( [ w for w in wus[:c2] ])
                
                    #print 'col range: %s, %s'%(colbeg,colend)
                    #wids = sum([ w for w in wus[p1[1]:p2[1]+1] ])
                    line = self.lines[r]
                    
                    lines[r]= line[0]+line[1:colbeg] +' '* (colend-colbeg)+ line[colend:]
                    
        self.__dict__['__lines__']= lines
    
    def drawmerges_test(self): 
        cmd=''
        print '#'*65
        ag= AscGrid()
        lines= ag(title='',header='',footer='')
        def prn(lines, cmd=cmd):
            print cmd
            #eval(cmd)
            for line in lines:
                print line
        prn( lines )
        print 'Q'*50
        
        cmd = "ag.merge((1,1),'c1')"
        #ag.merge( (1,1),'c1' )
        eval( cmd )
        prn( ag(), cmd )
        print 'ag.wcount= ', ag.wcount          
        cmd = "ag( wunits=[3,6,4,5,2,3], hunits=[2,3,2,4])"          
        lines= eval(cmd)          
        prn( lines, cmd )          
        print 'ag.wunits= ', ag.wunits
        print 'ag.wcount= ', ag.wcount          
                  
        cmd= 'ag.merge( (2,1),"c", (3,2),"r1c1")'
        eval(cmd)          
        prn( ag(), cmd )          
                  
        cmd= 'ag.merge( (2,1),"c", (3,2),"r1c6")'
        eval(cmd)          
        prn( ag(), cmd )          
            
        #ag.merge((1,1),(3,1)) 
        #for line in  ag(title='',header='',footer=''):
        #    print line
        
        '''
        print '@'*50
        #ag.merge((2,2),(2,4)) 
        ag.merge((2,2),'r2')
        print "ag.merge((2,2),'r2')"
        for line in  ag(title='',header='',footer=''):
           print line
        
        print '@'*50
        #ag.merge((3,2),(3,3)) 
        ag.merge( (3,2),'c1')
        print "ag.merge( (3,2),'c1')"
        for line in  ag(title='',header='',footer=''):
           print line
        
        print '@'*50
        #ag.merge((2,1),(3,1)) 
        ag.merge((2,1),'r2')
        print "ag.merge((2,1),'r2')"
        for line in  ag(title='',header='',footer=''):
           print line
   
        print '#'*50
        ag= AscGrid()
        ag.wcount=10
        ag.wunit=3
        ag.hcount=6
        #ag(wcount=10,wunit=3,hcount=6)
        for line in  ag(title='',header='',footer=''):
           print line
   
        #ag.merge((1,1),(1,10))
        ag.merge((1,1),'c')
        print "ag.merge((1,1),'c')"
        for line in  ag(title='',header='',footer=''):
           print line
   
        ag.merge((6,4),'c')
        print "ag.merge((2,1),'c')"
        for line in  ag(title='',header='',footer=''):
           print line
   
        #ag.merge((6,1),(6,10))
        #ag.merge((2,10),(5,10))
        #ag.merge((2,1),(5,1))
        ag.merge((5,2),'c',(2,10),'r2',(2,1),'r4')
        print "ag.merge((5,2),'c',(2,10),'r3',(2,1),'r3')"
        for line in  ag(title='',header='',footer=''):
           print line
        
        ag=AscGrid()
        for line in  ag(wcount=5,wunit=3,hcount=5,title='',header='',footer=''):
           print line
        #ag.merge((2,2),(4,4))   
        ag.merge( (2,2),'r2c2')
        print "ag.merge( (2,2),'r2c2')"
        for line in  ag(title='',header='',footer=''):
           print line
        '''
            
    def draw(self, **kargs):
        #print '\n\n\Entered draw, before grawgrid, kargs= ', kargs
        self.drawgrid(**kargs)
        #print 'in draw(), after drawgrid, before drawOuterText'
        #print 'in draw(), after drawOuterText, before drawdata'
        if len(self.merges)>0: self.drawmerges()
        if len(self.pxdataset)>0: self.drawpxdata() ##plotdataset()
        self.drawOuterText()
        #print 'in draw, after drawdata, before leaving'
        #return deepcopy(self.__dict__['__lines__'])

    def drawOuterText(self):
        lines=self.__dict__['__lines__']
        if self.header:   lines=[ self.header ]+lines
        if self.title:
            t=self.title
            if self.t_align=='center':
                t=t.center(self.wunit*self.wcount+1, ' ')
            lines=[ t]+lines
        if self.footer:   lines=lines+[self.footer]
        self.__dict__['__lines__']=lines
        #return lines      

    def drawgrid(self, **kargs):
        '''

            ___Basic___

                >>> ag = AscGrid()

                ag.draw(...) to redraw the grid. Then use the ag.lines to get a copy 
                of internal text lines. Or, 
                ag(...) to redraw and return in one step.

                >>> print( '\\n'.join(ag(title='[title: AscGrid Demo]',
                ...                   header='This line is ".header"',footer='This line is ".footer"')) )
                [title: AscGrid Demo]
                This line is ".header"
                +----+----+----+----+
                :    :    :    :    :
                +----+----+----+----+
                :    :    :    :    :
                +----+----+----+----+
                :    :    :    :    :
                +----+----+----+----+
                This line is ".footer"

                >>> ag.title
                '[title: AscGrid Demo]'
                >>> ag.header
                'This line is ".header"'
                >>> ag.footer
                'This line is ".footer"'

                >>> ag.width, ag.height
                (21, 7)

                >>> ag.title= '--AscGrid Demo--'
                >>> ag.draw()
                >>> print( '\\n'.join(ag.lines) )
                   --AscGrid Demo--  
                This line is ".header"
                +----+----+----+----+
                :    :    :    :    :
                +----+----+----+----+
                :    :    :    :    :
                +----+----+----+----+
                :    :    :    :    :
                +----+----+----+----+
                This line is ".footer"


                The 3 lines:

                    ag.title='xxx'
                    ag.draw()
                    ag.lines

                can be simplified to:

                    ag(title='xxx')

                Both will get a list of string.


            ___Size___

                >>> lines=ag( title='',
                ...           header='', hunit=3, hcount=2,
                ...           footer='hunit=3, hcount=2')
                >>> print('\\n'.join(lines))
                +----+----+----+----+
                :    :    :    :    :
                :    :    :    :    :
                +----+----+----+----+
                :    :    :    :    :
                :    :    :    :    :
                +----+----+----+----+
                hunit=3, hcount=2

                >>> ag.width, ag.height
                (21, 7)

                >>> lines=ag( hunit=2, hcount=3, wunit=6, wcount=3,
                ...           footer='hunit=2, hcount=3, wunit=6, wcount=3')
                >>> print('\\n'.join(lines))
                +-----+-----+-----+
                :     :     :     :
                +-----+-----+-----+
                :     :     :     :
                +-----+-----+-----+
                :     :     :     :
                +-----+-----+-----+
                hunit=2, hcount=3, wunit=6, wcount=3

                >>> ag.width, ag.height
                (19, 7)
            

            ___Style/Basic___


                >>> lines=ag(title="Styles", 
                ...          wline='*', hline='!', node='x',
                ...          footer="wline='*', hline='!', node='x'")
                >>> print('\\n'.join(lines))
                       Styles      
                x*****x*****x*****x
                !     !     !     !
                x*****x*****x*****x
                !     !     !     !
                x*****x*****x*****x
                !     !     !     !
                x*****x*****x*****x
                wline='*', hline='!', node='x'



            ___Style/Border/Basic___

                .bt is a property to set border-top style

                When drawing, 3 attributes are used to determine bt style:
                    .btshow:  True|False for show/hide
                    .btline:  '=' to set line symble to =
                    .btnode:  'x' to set node symble to x

                They can be set individually using one of the following:

                    ag.btshow = True       # no immediate redraw
                    ag(btshow = True)      # immediate redraw

                or set in a bundle through .bt property:

                    ag.bt= True
                    ag.bt= True, '='
                    ag.bt= '=','0'
                    ag.bt= True,'=','0'


                What .bt return is a run-time generated list:

                >>> ag.bt
                [False, '=', 'o']

                representing btshow, btline, btnode, respectively.
                 
                Since [False, '=', 'o'] doesn't exist as a list inside 
                the class, changing it directly won't have any effect:
                
                border= ag.bt
                border[0] = True    <==== this won't work 
                
                ------------------------
                >>> ag.bt= True
                >>> ag.bt
                [True, '=', 'o']

                >>> ag.bt= "@"
                >>> ag.bt
                [True, '@', 'o']

                ------------------------

                >>> ag.bt= False, "#"
                >>> ag.bt
                [False, '#', 'o']

                >>> ag.bt= '=','#'
                >>> ag.bt
                [False, '=', '#']

                ------------------------
                >>> ag.bt= True, '-','x'
                >>> ag.bt
                [True, '-', 'x']

                =============== Can set btshow, btline and btnode directly
                >>> ag.btnode = '#'
                >>> ag.bt
                [True, '-', '#']
                >>> print('\\n'.join(ag()))
                       Styles      
                #-----#-----#-----#
                !     !     !     !
                x*****x*****x*****x
                !     !     !     !
                x*****x*****x*****x
                !     !     !     !
                x*****x*****x*****x
                wline='*', hline='!', node='x'


                Set style name to the "default" style:

                    ag.style='default'   or
                    ag(style='default')

                >>> ag.style='default'   # This will set all borders to False
                >>> ag.bt
                [False, '=', 'o']
                >>> print('\\n'.join(ag()))
                       Styles      
                +-----+-----+-----+
                :     :     :     :
                +-----+-----+-----+
                :     :     :     :
                +-----+-----+-----+
                :     :     :     :
                +-----+-----+-----+
                wline='*', hline='!', node='x'



            ___Style/Border/Adv___


                Set all borders in a bundle

                >>> print('\\n'.join(ag(title='',footer='')))
                +-----+-----+-----+
                :     :     :     :
                +-----+-----+-----+
                :     :     :     :
                +-----+-----+-----+
                :     :     :     :
                +-----+-----+-----+
                
                .b gives the info of all borders t,r,b,l
                
                >>> ag.b                      # doctest: +NORMALIZE_WHITESPACE
                ([False, '=', 'o'], [False, '|', 'o'],
                 [False, '=', 'o'], [False, '|', 'o'])


                We turn on the r,b,l (right, bottom, left) borders: 
                
                >>> ag.brshow = True
                >>> ag.bbshow = True
                >>> ag.blshow = True
                >>> print('\\n'.join(ag(title='',footer='')))
                o-----+-----+-----o
                |     :     :     |
                o-----+-----+-----o
                |     :     :     |
                o-----+-----+-----o
                |     :     :     |
                o=====o=====o=====o

                
                >>> ag.style='default'

                >>> ag.b = True, '#', '@'     # set all borders

                >>> ag.b                      # doctest: +NORMALIZE_WHITESPACE
                ([True, '#', '@'], [True, '#', '@'],
                 [True, '#', '@'], [True, '#', '@'])
                >>> print('\\n'.join(ag()))
                @#####@#####@#####@
                #     :     :     #
                @-----+-----+-----@
                #     :     :     #
                @-----+-----+-----@
                #     :     :     #
                @#####@#####@#####@



                Note: when .gshow (grid show) is false, the grid lines will
                be hidden, then all borders will be turned on automatically, 
                even the individual .b?show is False. 


                >>> ag.style='default'
                >>> lines= ag(gshow=False, title='When grid hidden',
                ...                        footer='border shown automatically')
                >>> print('\\n'.join(lines))
                  When grid hidden 
                o=====o=====o=====o
                |                 |
                o                 o
                |                 |
                o                 o
                |                 |
                o=====o=====o=====o
                border shown automatically

                Note that the individual border setting is still 'False':

                >>> ag.bb
                [False, '=', 'o']



            ___Style/Corner___


                >>> lines= ag(style='default', corner='#',
                ...           title='Set corner', footer="corner='#'")
                >>> print('\\n'.join(lines))
                     Set corner    
                #-----+-----+-----#
                :     :     :     :
                +-----+-----+-----+
                :     :     :     :
                +-----+-----+-----+
                :     :     :     :
                #-----+-----+-----#
                corner='#'


                >>> lines= ag(style='default', corner=('0','1','2','3'),
                ...     title='Set corner', footer="corner='('0','1','2','3')'")
                >>> print('\\n'.join(lines))
                     Set corner    
                0-----+-----+-----1
                :     :     :     :
                +-----+-----+-----+
                :     :     :     :
                +-----+-----+-----+
                :     :     :     :
                3-----+-----+-----2
                corner='('0','1','2','3')'


                >>> lines= ag(  gshow=False, title='Set corner',
                ...     footer="Corner overwrites border settings")
                >>> print('\\n'.join(lines))
                     Set corner    
                0=====o=====o=====1
                |                 |
                o                 o
                |                 |
                o                 o
                |                 |
                3=====o=====o=====2
                Corner overwrites border settings


            ___Style/Loading___


                Use
                   ag.style='default'  or
                   ag(style='default')

                to reset the style to the default. The 2nd case, ag(...),
                forces a redraw immediately and returns the lines.

                >>> lines=ag( title="Load default",
                ...           style='default',
                ...           footer="style='default'")
                >>> print('\\n'.join(lines))
                    Load default   
                +-----+-----+-----+
                :     :     :     :
                +-----+-----+-----+
                :     :     :     :
                +-----+-----+-----+
                :     :     :     :
                +-----+-----+-----+
                style='default'


                >>> ag.styles['mystyle']={ 'wline':'=', 'node':'O', 'hline':'|' }
                >>> lines= ag( title="Load my style", style='mystyle',
                ...            footer='style="mystyle"')
                >>> print('\\n'.join(lines))
                   Load my style   
                O=====O=====O=====O
                |     |     |     |
                O=====O=====O=====O
                |     |     |     |
                O=====O=====O=====O
                |     |     |     |
                O=====O=====O=====O
                style="mystyle"


                # chk current styles in store.
                >>> sk = ag.styles.keys() 
                >>> sk.sort() or sk
                ['default', 'func1', 'mystyle', 'square']
                
                >> print('\\n'.join( [str(len(x)) for x in lines] ) )

                >>> lines= ag(title="Load my style", style='square',
                ...           footer='style="square"')

                >>> print('\\n'.join(lines))
                   Load my style   
                ###################
                #     |     |     #
                #-----+-----+-----#
                #     |     |     #
                #-----+-----+-----#
                #     |     |     #
                ###################
                style="square"

                
            ___Size/Adv__
            
            
                >>> lines=ag( style='default', title='', wunits= [3,7,2],
                ...           footer='Varying cell widths, wunits=[3,7,2]')
                >>> print('\\n'.join(lines))
                +--+------+-+
                :  :      : :
                +--+------+-+
                :  :      : :
                +--+------+-+
                :  :      : :
                +--+------+-+
                Varying cell widths, wunits=[3,7,2]
                
                >>> ag.wunits, ag.hunits
                ([3, 7, 2], [])
                >>> ag.width, ag.height
                (13, 7)
                
                >>> lines=ag( hunits= [3,1,4],
                ...           footer='Varying cell heights, hunits=[3,1,4]')
                >>> print('\\n'.join(lines))
                +--+------+-+
                :  :      : :
                :  :      : :
                +--+------+-+
                +--+------+-+
                :  :      : :
                :  :      : :
                :  :      : :
                +--+------+-+
                Varying cell heights, hunits=[3,1,4]

                >>> ag.wunits, ag.hunits
                ([3, 7, 2], [3, 1, 4])

                >>> ag.width, ag.height
                (13, 9)
                

        '''

        #print
        #print 'Entered draw, dataset: ', self.pxdataset
        #print 

        ##==========================================
        ## Set customized style.
        ##
        if 'style' in kargs:                    ## if style name given    
            if kargs['style'] in self.styles:   ## if it exists in styles
                self.__dict__.update(self.styles[ kargs['style']])
            else:
                del kargs['style']  ## del invalid style name given in kargs.
                                    ## All kargs will be updated to __dict__
                                    ## so later on we can check the current
                                    ## style name with: .style. If an invalid
                                    ## name not del, it will be assigned to
                                    ## __dict__. In that case, the .style= 
                                    ## check will give wrong info.  

        ## Chk border setting. If any of ('bt','br','bb','bl') given in kargs,
        ## say, 'bt', kargs['bt']=v, we need to filter v throu setbt(x) to
        ## set btshow, btline and/btnode, and return the modified v, which is
        ## a list like [True, '=','+'] and be assigned to the .ops. See 
        ## lineSetter() code for details. 
        ##
        for b2 in ('bt', 'br', 'bb', 'bl'):
            if b2 in kargs:
                x=kargs[b2]                          # EX: True 
                self.__getattribute__('set'+b2)(x)  # EX: self.setbt(x)
                kargs[b2]=self.__getattribute__(b2)  # EX: [True,'=','+']

        ## 
        if 'wunits' in kargs:
            kargs['wcount']= len(kargs['wunits'])
        #else:
        #    if self.wunits==[]: self.wunits= [self.wunit] * self.wcount      
        if 'hunits' in kargs:
            kargs['hcount']= len(kargs['hunits'])
        #else:
        #    if self.hunits==[]: self.hunits= [self.hunit] * self.hcount      
        
        
        self.__dict__.update(kargs)
        self.ops.update(kargs)      ## Also update them to self.ops so we can
                                    ## chk ops for current parameters.

        wu=self.wunit
        wc=self.wcount #= len(self.wunits)
        wl=self.wline
        hu=self.hunit
        hc=self.hcount
        hl=self.hline

        #if not self.wunits: self.wunits= [wu]* wc
        #if not self.hunits: self.hunits= [hu]* hc

        wus = self.wunits
        hus = self.hunits
        
        if not wus: wus = [wu]* wc
        if not hus: hus = [hu]* hc 
        
        #def makeline(head, symb, unitsize, node, repeat):
        #    return head+(symb*(unitsize-1)+node)*repeat

        def makeline(head, symb, unitsizes, node):
            return head+ ''.join([ (symb*(us-1)+node) for us in unitsizes])

        ##===================================================
        ## build x (line), only two types of x:
        ##
        ## la=makeline(self.node, wl, wu, self.node, wc)   ## +--+--+--+ ...--+
        ## lb=makeline(hl, ' ', wu, hl, wc)                ## |  |  |  |
        la=makeline(self.node, wl, wus, self.node)   ## +--+--+--+ ...--+
        lb=makeline(hl, ' ', wus, hl)                ## |  |  |  |
        lines=[la]
        
        ''''''
        _hus = [a for a in hus]
        if self.gshow:
            #[ lines.append(r%hu==0 and la or lb) for r in range(1, hu*hc+1) ]
            while len(_hus)>0:
                r = _hus.pop(0)
                [ lines.append(lb) for rr in range(r-1) ]
                lines.append( la )
        else: # no gridlines
            [ lines.append(' '*wu*wc+' ') for r in range(1, hu*hc+1) ]
            
        
        
        ##===================================================
        ## draw borders:
        ##
        bt, br, bb, bl=self.bt, self.br, self.bb, self.bl   # [True,'|','#']
        if not self.gshow:
            bt[0], br[0], bb[0], bl[0]=(True, True, True, True)

        #if bt[0]: lines[0]=makeline(bt[2], bt[1], wu, bt[2], wc)
        #if bb[0]: lines[-1]=makeline(bb[2], bb[1], wu, bb[2], wc)
        if bt[0]: lines[0]=makeline(bt[2], bt[1], wus, bt[2])
        if bb[0]: lines[-1]=makeline(bb[2], bb[1], wus, bb[2])

        for i, line in enumerate(lines):
            L=(i%hu==0) and bl[2] or bl[1]
            R=(i%hu==0) and br[2] or br[1]
            if bl[0]: lines[i]=L+lines[i][1:]
            if br[0]: lines[i]=lines[i][:-1]+R
        ##===================================================

        ##===============================
        ## draw corners:
        c=self.corner
        if c:
            if type(c)==str: c=[c]*4
            lines[0]=c[0]+lines[0][1:-1]+c[1]
            lines[-1]=c[3]+lines[-1][1:-1]+c[2]

        self.__dict__['__lines__']=lines

        #return lines

    #==============================================

    def addpxdata(self, pt, name=None, symb=None, append=False, wrange=None ): # data: 
        '''
            >>> ag= AscGrid()  
            >>> ag.addpxdata( pt=(3,7) ) 
            >>> ag.pxdataset
            [{'symb': '*', 'data': [(3, 7)], 'name': '#1'}]

            >>> print '\\n'.join( ag( ) )
            +----+----+----+----+
            :    :    :    :    :
            +----+----+----+----+
            :    : *  :    :    :
            +----+----+----+----+
            :    :    :    :    :
            +----+----+----+----+
    
    
            >>> ag.addpxdata(  (5,17)  )
            >>> ag.pxdataset                       # doctest: +NORMALIZE_WHITESPACE
            [{'symb': '*', 'data': [(3, 7)], 'name': '#1'}, 
             {'symb': 'o', 'data': [(5, 17)], 'name': '#2'}]
            >>> print '\\n'.join( ag( ) )
            +----+----+----+----+
            :    :    :    :    :
            +----+----+----+----+
            :    : *  :    :    :
            +----+----+----+----+
            :    :    :    : o  :
            +----+----+----+----+
            
            
            >>> ag.addpxdata( (5,2), 'New', 'N')
            >>> ag.pxdataset                       # doctest: +NORMALIZE_WHITESPACE
            [{'symb': '*', 'data': [(3, 7)], 'name': '#1'}, 
             {'symb': 'o', 'data': [(5, 17)], 'name': '#2'}, 
             {'symb': 'N', 'data': [(5, 2)], 'name': 'New'}]
            >>> print '\\n'.join( ag( ) )
            +----+----+----+----+
            :    :    :    :    :
            +----+----+----+----+
            :    : *  :    :    :
            +----+----+----+----+
            : N  :    :    : o  :
            +----+----+----+----+
            
            >>> ag.addpxdata( (1,8))
            >>> print '\\n'.join( ag( ) )
            +----+----+----+----+
            :    :  x :    :    :
            +----+----+----+----+
            :    : *  :    :    :
            +----+----+----+----+
            : N  :    :    : o  :
            +----+----+----+----+
            
                                                ## append data
            
            >>> ag.addpxdata(  pt=(2,13), name='#2', append=True  )
            >>> ag.pxdataset                       # doctest: +NORMALIZE_WHITESPACE
            [{'symb': '*', 'data': [(3, 7)], 'name': '#1'}, 
             {'symb': 'o', 'data': [(5, 17), (2, 13)], 'name': '#2'}, 
             {'symb': 'N', 'data': [(5, 2)], 'name': 'New'}, 
             {'symb': 'x', 'data': [(1, 8)], 'name': '#4'}]

            
            >>> print '\\n'.join( ag( ) )
            +----+----+----+----+
            :    :  x :    :    :
            +----+----+--o-+----+
            :    : *  :    :    :
            +----+----+----+----+
            : N  :    :    : o  :
            +----+----+----+----+
            
                                                ## getdata by name
            
            >>> ag.getpxdata('#1')           # doctest: +NORMALIZE_WHITESPACE
            {'symb': '*', 'data': [(3, 7)], 'name': '#1'}
            
            >>> ag.delpxdata('#1')              ## delete data
            >>> print '\\n'.join( ag( ) )
            +----+----+----+----+
            :    :  x :    :    :
            +----+----+--o-+----+
            :    :    :    :    :
            +----+----+----+----+
            : N  :    :    : o  :
            +----+----+----+----+
            
                                               ## symb can be a string
                                               
            >>> ag.addpxdata(pt=(1,7), symb="[Long text label]")
            >>> print '\\n'.join( ag( ) )
            +----+----+----+----+
            :    : [Long text label]
            +----+----+--o-+----+
            :    :    :    :    :
            +----+----+----+----+
            : N  :    :    : o  :
            +----+----+----+----+
            
            >>> ag.addpxdata( pt=[ (2,3),(3,5),(4,7),(5,10) ], name='L1')
            >>> print '\\n'.join( ag( ) )
            +----+----+----+----+
            :    : [Long text label]
            +--*-+----+--o-+----+
            :    *    :    :    :
            +----+-*--+----+----+
            : N  :    *    : o  :
            +----+----+----+----+

            >>> ag.pxdataset=[]
            >>> ag.addpxdata( pt= lambda w: w/3, wrange=(0,18) )
            >>> ag.pxdataset                                     # doctest:+ELLIPSIS
            [{'wrange': (0, 18), 'symb': '*', 'data': <function <lambda> at ...>, 'name': '#1'}]
            >>> print '\\n'.join( ag( ) )
            ***--+----+----+----+
            :  ***    :    :    :
            +----+***-+----+----+
            :    :   ***   :    :
            +----+----+-***+----+
            :    :    :    ***  :
            +----+----+----+----+
            
            >>> ag.addpxdata( pt= lambda w: 7-w/3 )
            >>> ag.pxdataset                             # doctest:+ELLIPSIS +NORMALIZE_WHITESPACE
            [{'wrange': (0, 18), 'symb': '*', 'data': <function <lambda> at ...>, 'name': '#1'}, 
             {'wrange': (1, 21), 'symb': 'o', 'data': <function <lambda> at ...>, 'name': '#2'}]
            >>> print '\\n'.join( ag( ) )
            ***--+----+----+----+
            :  ***    :    :  ooo
            +----+***-+----ooo--+
            :    :   ***ooo:    :
            +----+---ooo***+----+
            :    :ooo :    ***  :
            +--ooo----+----+----+
            
            
        ___draw function___      
            
            >>> hc, wc, wu = 10, 35, 2
            
            >>> ag=AscGrid( hcount=hc, wcount=wc, wunit=wu, style='func1')
            
            Create a function:
            
            >>> f = lambda w: hc+ hc*math.sin( w*(wu*1.0)/(hc*1.0))
            
            Add data :
            
            >>> ag.addpxdata( pt=f, symb='o', wrange=(0, wc*wu))               #1
            
            The above line is the same as:
            
                ag.addpxdata( pt=[(f(w), w) for w in range(0, wc*wu)], symb='o')   #2
            
            But the way data stored in ag.pxdataset is different: #1 stores the function,
            but #2 stores the function outcome (entire set of data).  
            
            >>> print '\\n'.join( ag( ) )
            +---------------------oooo----------------------------ooo-------------+
            |                    o    o                          o   o            |
            +...................o......o........................o.....o...........+
            |                                                  o       o          |
            +..................o........o.........................................+
            |                            o                    o         o         |
            +.................o..............................o...........o........+
            |                o            o                                       |
            +...............................................o.............o.......+
            |               o              o                                      |
            o..............................................o...............o......+
            |              o                o                                     |
            +o............................................o.................o.....+
            |             o                  o                                    |
            +.o..........................................o...................o....+
            |            o                    o                                   |
            +..o........................................o.....................o...+
            |   o       o                      o       o                       o  |
            +....o.....o........................o.................................+
            |     o   o                          o   oo                         oo|
            +------ooo----------------------------ooo-----------------------------+

            

            
        '''
        #print '\n\nEntered addpxdata:\n'
        def autosymb():  # get a symb automatically
            symbs = '*oxacemnosuvwz#'
            #print 'in autosumb(), pxdataset=', self.pxdataset
            #print 'in autosumb(), symbs= ', [d['symb'] for d in self.pxdataset ]
            for d in self.pxdataset: symbs = symbs.replace(d['symb'], '') 
            return symbs[0]
        
        def autoname():  # get a data name automatically
            dl=len(self.pxdataset)
            numbs= [dl]+[ int(x.replace('#', ''))
                           for x in self.datanames if x.startswith('#') ]
            return '#%s'%(max(numbs)+1)
        
           #for pti in pt:
            #    self.addpxdata( pti, name=name, symb=symb, append=True)
        if not inspect.isfunction(pt) and not(type(pt[0])==list or type(pt[0])==tuple): pt = [pt]
        
        if name and append:
            if name in [ d['name'] for d in self.pxdataset ] :
                self.getpxdata( name )['data']= self.getpxdata( name )['data']+ pt
        else:
            if not symb: symb = autosymb()
            if not name: name = autoname()
        
            d= {'name':name, 'symb':symb, 'data': pt}
            if inspect.isfunction(pt): 
                d['wrange']= wrange==None and (1, self.width) or wrange
                #d['wrange']= ( d['wrange'][0], d['wrange'][1]+1 )
            self.pxdataset.append(d )
            #print 'In addata, self.pxdataset= ', self.pxdataset
        

    def addpt2(self, pt, name=None, symb=None, append=False, wrange=(0,1) ): # data: 
        '''
            >>> ag= AscGrid()  
            >>> ag.addpxdata( pt=(3,7) , name='test', symb='x')
            >>> ag.pxdataset
            [{'symb': 'x', 'data': [(3, 7)], 'name': 'test'}]

            >>> print '\\n'.join( ag( title='', header='', footer='' ) )
            +----+----+----+----+
            :    :    :    :    :
            +----+----+----+----+
            :    : x  :    :    :
            +----+----+----+----+
            :    :    :    :    :
            +----+----+----+----+
    
    
            >>> ag.addpxdata(  pt=(5,17)  )
            >>> ag.pxdataset                       # doctest: +NORMALIZE_WHITESPACE
            [{'symb': 'x', 'data': [(3, 7)], 'name': 'test'}, 
             {'symb': '*', 'data': [(5, 17)], 'name': '#2'}]
            >>> print '\\n'.join( ag( ) )
            +----+----+----+----+
            :    :    :    :    :
            +----+----+----+----+
            :    : x  :    :    :
            +----+----+----+----+
            :    :    :    : *  :
            +----+----+----+----+
            
            
            >>> ag.addpxdata( (5,2), 'New', 'N')
            >>> ag.pxdataset                       # doctest: +NORMALIZE_WHITESPACE
            [{'symb': 'x', 'data': [(3, 7)], 'name': 'test'}, 
             {'symb': '*', 'data': [(5, 17)], 'name': '#2'}, 
             {'symb': 'N', 'data': [(5, 2)], 'name': 'New'}]
            >>> print '\\n'.join( ag( ) )
            +----+----+----+----+
            :    :    :    :    :
            +----+----+----+----+
            :    : x  :    :    :
            +----+----+----+----+
            : N  :    :    : *  :
            +----+----+----+----+
            
                                                ## append data
            
            >>> ag.addpxdata(  pt=(2,13), name='test', append=True  )
            >>> ag.pxdataset                       # doctest: +NORMALIZE_WHITESPACE
            [{'symb': 'x', 'data': [(3, 7), (2, 13)], 'name': 'test'}, 
             {'symb': '*', 'data': [(5, 17)], 'name': '#2'}, 
             {'symb': 'N', 'data': [(5, 2)], 'name': 'New'}]

            
            >>> print '\\n'.join( ag( ) )
            +----+----+----+----+
            :    :    :    :    :
            +----+----+--x-+----+
            :    : x  :    :    :
            +----+----+----+----+
            : N  :    :    : *  :
            +----+----+----+----+
            
                                                ## getdata by name
            
            >>> ag.getpxdata('test')           # doctest: +NORMALIZE_WHITESPACE
            {'symb': 'x', 'data': [(3, 7), (2, 13)], 'name': 'test'}
            
            
            >>> ag.delpxdata('test')              ## delete data
            >>> print '\\n'.join( ag( ) )
            +----+----+----+----+
            :    :    :    :    :
            +----+----+----+----+
            :    :    :    :    :
            +----+----+----+----+
            : N  :    :    : *  :
            +----+----+----+----+
            
        '''
        def autosymb():  # get a symb automatically
            symbs = '*oxacemnosuvwz#'
            for d in self.pxdataset: symbs.replace( d['symb'], '' ) 
            return symbs[0]
        
        def autoname():  # get a data name automatically
            dl=len(self.pxdataset)
            numbs= [dl]+[ int(x.replace('#', ''))
                           for x in self.datanames if x.startswith('#') ]
            return '#%s'%( max(numbs)+1 )
        
        
        if name and append:
            if name in [ d['name'] for d in self.pxdataset ] :
                self.getpxdata( name )['data']= self.getpxdata( name )['data']+ [pt]
        else:
            if not symb: symb = autosymb()
            if not name: name = autoname()
        
            #print 'In addata, self.pxdataset= ', self.pxdataset
            self.pxdataset.append({'name':name, 'symb':symb, 'data': [pt]})
            #print 'In addata, self.pxdataset= ', self.pxdataset
        
    def delpxdata(self, name):
        del self.pxdataset[ self.datanames.index(name) ]

    def getpxdata(self, name):
        '''
        >> ag= AscGrid()
        >> ag.markpix( 'x', (3,5), name='p1' )
        >> ag.plotpix( 'y', [(4,1),(2,11)], name='p2')
        >> ag.pxdataset                         # doctest: +NORMALIZE_WHITESPACE
        [{'symb': 'x', 'data': [(3, 5)], 'name': 'p1'},
         {'symb': 'y', 'data': [(4, 1), (2, 11)], 'name': 'p2'}]
        >> ag.datanames
        ['p1', 'p2']

        '''
        return self.pxdataset[ self.datanames.index(name) ]

    def drawpxdata(self):
        '''
            Place the data of .pxdataset to the .lines
        '''
        #print 'Entered drawdata '
        
        lines = self.__dict__['__lines__']
        #print 'In drawpxdata, lines len= ', len(lines)
        w = self.width 
        h = self.height 
        
        def drawpx(pr,pc, lines):
            pr, pc=int(round(pr*1.0)), int(round(pc*1.0))
            #print 'in drawpx,llen=', len(lines), ', pr,pc=', pr, ', ',pc, ', w,h= ', w, ', ', h
            if pc< w and pr< h  :
               #print '---       in drawpx, pr(%s)<=h(%s) and pc(%s)<=w(%s)'%(pr,h,pc,w)
               line=lines[pr]
               lines[pr]=line[:pc]+s+line[pc+len(s):]
        
        for D in self.pxdataset:
            s= D['symb']
            d= D['data']
            
            if inspect.isfunction(d):
                #for w in D['wrange']:
                #    print 'in drawpxdata, (w,h)= (', w, ', ', d(w) ,')' 
                rng = D['wrange']
                d = [(d(w), w) for w in range(rng[0],rng[1]+1)  ]
                #print 'in drawpxdata, d=', d
            #print 'in drawpxdata, data= ', d    
            
            for pr,pc in d: #D['data']:
                drawpx(pr,pc,lines)
       
        
    def plotdataset(self, dataset=None):
        '''

        >> d1={'name':'d1','symb':'1','data':[(3,2),(4,7),(2,17)]}
        >> d2={'name':'d2','symb':'2','data':[(1,5),(3,12)] }
        >> ag=AscGrid()
        >> ag.plotdataset( [d1,d2] )
        >> print '\\n'.join( ag.lines)
        +====+====+====+====+
        |    2    :    :    |
        +----+----+----+-1--+
        | 1  :    : 2  :    |
        +----+-1--+----+----+
        |    :    :    :    |
        +====+====+====+====+
        >> ag(xunit=4)
        >> print '\\n'.join( ag.lines)


        '''
        #print 'Entered plotdataset, dataset=', self.pxdataset 
        if not dataset: dataset=self.pxdataset #deepcopy(self.pxdataset)
        #self.pxdataset=[]
        for d in dataset:
            #print 'In plotdataset, adding data d ',d
            if len('data')==1:
                self.markpix(d['symb'], d['data'][0], name=d['name'])
            else:
                self.plotpix(s=d['symb'], data=d['data'], name=d['name'])


    

    def __call__(self, **kargs):
        #print '--- Entered __call__'
        self.draw(**kargs)
        return deepcopy(self.__dict__['__lines__'])

    def markpix(self, s, pr, pc=None, name=None, append=True):
        '''
            mark a pix (pixel) with string s at row,col = pr,pc
            or, pr could be a 2-item to represent both pr and pc

            Usage:

            ag.markpix('x', 3,14   )              # will be named: '#1'
            ag.markpix('x', (3,14) )
            ag.markpix('x', (3,14), name='age' )


            pc and pr both have to be >0 and within bound.
            If not, no mark will be added.
            The outer bounds can be obtained:

            x bound: ag.xunit * ag.xcount
            y bount: ag.yunit * ag.ycount

            >> ag=AscGrid()
            >> ag._next_data_name()         # automatically generated data name
            '#1'

            ========
            Add data
            ========

            >> ag.markpix('@',3,14)         # put '@' at row 3, col 14
            >> print '\\n'.join( ag() )
            +====+====+====+====+
            |    :    :    :    |
            +----+----+----+----+
            |    :    :   @:    |
            +----+----+----+----+
            |    :    :    :    |
            +====+====+====+====+

            Now we have a data in the dataset, named '#1':

            >> ag.pxdataset
            [{'symb': '@', 'data': [(3, 14)], 'name': '#1'}]

            Use (r,c) instead of r,c, and give it a name 'age':

            >> ag.markpix('#',(5,0), name='age')
            >> ag.markpix('x',(1,7))
            >> ag.pxdataset                   # doctest: +NORMALIZE_WHITESPACE
            [{'symb': '@', 'data': [(3, 14)], 'name': '#1'},
             {'symb': '#', 'data': [(5, 0)], 'name': 'age'},
             {'symb': 'x', 'data': [(1, 7)], 'name': '#3'}]

            >> print '\\n'.join( ag() )
            +====+====+====+====+
            |    : x  :    :    |
            +----+----+----+----+
            |    :    :   @:    |
            +----+----+----+----+
            #    :    :    :    |
            +====+====+====+====+


            >> ag.delpxdata('age')
            >> ag.pxdataset                   # doctest: +NORMALIZE_WHITESPACE
            [{'symb': '@', 'data': [(3, 14)], 'name': '#1'},
             {'symb': 'x', 'data': [(1, 7)], 'name': '#3'}]
            >> print '\\n'.join( ag() )
            +====+====+====+====+
            |    : x  :    :    |
            +----+----+----+----+
            |    :    :   @:    |
            +----+----+----+----+
            |    :    :    :    |
            +====+====+====+====+



            Use existing name override the old one:

            >> ag.markpix('p',(5,7), name='#3')
            >> print '\\n'.join( ag() )
            +====+====+====+====+
            |    :    :    :    |
            +----+----+----+----+
            |    :    :   @:    |
            +----+----+----+----+
            |    : p  :    :    |
            +====+====+====+====+

            >> ag.pxdataset                   # doctest: +NORMALIZE_WHITESPACE
            [{'symb': '@', 'data': [(3, 14)], 'name': #1},
             {'symb': '#', 'data': [(2, 5)], 'name': 'age'}]

            >> print '\\n'.join( ag(xcount=5) )
            +====+====+====+====+====+
            |    :    :    :    :    |
            +----+----+----+----+----+
            |    :    :   @:    :    |
            +----+----+----+----+----+
            #    :    :    :    :    |
            +====+====+====+====+====+

            >> ag.markpix('O',(0,0) )       # Chk origin
            >> print '\\n'.join( ag.lines )
            O====+====+====+====+
            |    :    :    :    |
            +----+----+----+----+
            |    :    :   @:    |
            +----+----+----+----+
            #    :    :    :    |
            +====+====+====+====+

            >> ag.markpix('Z',(6,20) )      # Chk the outer bounds
            >> print '\\n'.join( ag.lines )
            O====+====+====+====+
            |    :    :    :    |
            +----+----+----+----+
            |    :    :   @:    |
            +----+----+----+----+
            #    :    :    :    |
            +====+====+====+====Z

            >> ag.markpix('?',(9,20) )
            >> print '\\n'.join( ag.lines )
            O====+====+====+====+
            |    :    :    :    |
            +----+----+----+----+
            |    :    :   @:    |
            +----+----+----+----+
            #    :    :    :    |
            +====+====+====+====Z

        >>>
        '''

        #print '---- in .markpix(s=%s, pr=%s, pc=%s, name=%s)'%(s, pr,pc, name)
        if pc==None and len(pr)>1: pr, pc=pr
        #print '---- pr, pc = ', pr, ', ', pc
        if not name:
            name=self._next_data_name()

        if name in self.datanames:
            #print
            #print 'In markpix, name in datanames= ', name
            #print 
            if append:
                self.getpxdata(name)['data'].append((pr, pc))
            else:
                self.delpxdata(name)
                self.addpxdata(name, s, [(pr, pc)])

                #self.getpxdata(name)['data']=[ (pr,pc) ]
                #self.getpxdata(name)['symb']=s
        else:
            #print
            #print 'In markpix, name not in datanames= ', name
            #print 
            self.addpxdata(name, s, [(pr, pc)])
            #print '  added, dataset=', self.pxdataset
        #else:
        '''
        pr,pc=int(round(pr*1.0)), int(round(pc*1.0))
        #print '---- pr, pc = ', pr, ', ', pc
        if (pr<=self.yunit*self.ycount and
            pc<=self.xunit*self.xcount and
            pr>=0 and pc>=0) :
            #print '---- pr, pc = ', pr, ', ', pc

            line=self.lines[pr]
            self.lines[pr] = line[:pc]+ s+line[pc+1:]
            #print '---- lines %s = '%pr, self.lines[pr]
        '''

    def plotpix(self, s='o', data=None, pc_beg=None, pc_end=None, name=None):
        # data: [(2,3),(4,5)...]
        '''
            >> ag= AscGrid()
            >> print '\\n'.join( ag.lines )
            +====+====+====+====+
            |    :    :    :    |
            +----+----+----+----+
            |    :    :    :    |
            +----+----+----+----+
            |    :    :    :    |
            +====+====+====+====+

            >> ag.plotpix( data=[(2,3),(4,12),(1,17) ] )
            >> print '\\n'.join( ag.lines )
            +====+====+====+====+
            |    :    :    : o  |
            +--o-+----+----+----+
            |    :    :    :    |
            +----+----+-o--+----+
            |    :    :    :    |
            +====+====+====+====+

            >> ag.plotpix( '#', lambda r:3*r, name="lam" )
            >> print '\\n'.join( ag.lines )
            #====+====+====+====+
            |  # :    :    : o  |
            +--o-+#---+----+----+
            |    :   #:    :    |
            +----+----+-#--+----+
            |    :    :    #    |
            +====+====+====+==#=+

            >> print '\\n'.join( ag(xunit=4) )
            #====+====+====+====+
            |  # :    :    : o  |
            +--o-+#---+----+----+
            |    :   #:    :    |
            +----+----+-#--+----+
            |    :    :    #    |
            +====+====+====+==#=+

        '''
        d=[]
        if isfunction(data):
            if pc_beg==None: pc_beg=0
            if pc_end==None: pc_end=self.xunit*self.xcount
            #print 'plot range: ', pc_beg, ',', pc_end
            for r in range(pc_beg, pc_end+1):
               c=data(r)
               self.markpix(s=s, pr=(r, c), name=name)
               if name: d.append((r, c))
        else:
            #print 'In plotpix, s=%s, data = '%s, data
            for p in data:
                #print 'In plotpix, p= ',p
                self.markpix(s=s, pr=p, name=name)
                if name: d.append(p)
        #if name:
        #    self.pxdataset.append( {'name':name, 'symb':s, 'data':d} )         


def test():
    ag=AscGrid()
    ag.drawmerges_test()
    
#ag = AscGrid()
#pprint.pprint( ag() )
#print '\n'.join( ag( ))

if __name__=='__main__':
    import doctest
    doctest.REPORT_NDIFF=True
    doctest.testmod()
    #test()
    
