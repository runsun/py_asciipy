'''
Utilities to calc the scale from a data set for the purpose of 
automatically deciding the axis scale to be used for a plot.

'''

import math

class Str(str):
	'''
	>>> a= Str('abcdef')
	>>> a
	'abcdef'
	
	>>> a.split('d')
	['abc', 'ef']
	>>> a._
	['abc', 'ef']
	
	>>> '%s and {%s}'%( a.split('b')[0], a._[1])
	'a and {cdef}'
	
	>>> s= Str( 'abc, 3, 3.14, 3.14' )
	>>> [ s.split(',')[0], int(s._[1]), float(s._[2]) ]
	['abc', 3, 3.14]
	
	'''
	def __init__(self, x=''):
		self._=[]
	def split(self, x=' '):
		o= super(Str, self).split(x)
		self._= o
		return o
		
def autoscale(data, d0=None):
	'''
		Given data (a list of numbers), return a list representing
		the scale of a plot that can accomodate the data properly.
		
		It serves the purpose of a quick way to view how the data
		distributes. 
	
	odmin = order of 10 for the data min
	odmax = order of 10 for the data max
	
    ========================================================	 
	Situation A: odmax> odmin:
    ========================================================	 
	(That is, the max is on higher order of 10 than min) ---> d0 = 0 
	
		-----------------------------------------
		(dmin, dmax)	drange	 ord of 10   d0
								 dm  dx dr
		-------------- 	------	----------	---- 
		(0.33, 2.88),	2.55	-1,  0,  0  0.0
		(0.03,  1.25),	1.22	-2,  0,  0  0.0
		(3, 1763),		1760	 0,  3,  3	0
		(33, 1763),		1730	 1,  3,  3	0
		(600, 176000),	175400   2,  5,  5  0
		-----------------------------------------
		
	========================================================	 
	Situation B: odmax = odmin:  (odmax couldn't be < odmin)	
    ========================================================	 
	
	B1: 	
		---------------------------------------------------------                           
		(dmin, dmax)	drange	 ord of 10    
								 dm  dx  dr	dmin	 d0
		-------------- 	------	----------	------	-------------
		(0.33, 0.334),  0.004	-1, -1, -3  0.33	0.33  = dmin
		(0.033, 0.0334),0.0004	-2, -2, -4	0.033	0.033 = dmin
		(0.0333,0.03334),0.00004 -2, -2,-5	0.0333	0.0333 = dmin
		( 330, 335),    5        2,  2,  0  330		330  = dmin
		( 330, 350),    20       2,  2,  1  330		330  = dmin
		( 3330, 3335),  5        3,  3,  0  3330	3330 = dmin
		
	>>> autoscale( (0.33, 0.334) )   
	[0.33, 0.331, 0.332, 0.333, 0.334, 0.335]
	
	>>> autoscale( (0.33, 0.334), d0=0 )   
	[0.0, 0.1, 0.2, 0.3, 0.4]
	
	>>> autoscale(	(0.033, 0.0334) )  
	[0.033, 0.0331, 0.0332, 0.0333, 0.0334]
	
	>>> autoscale(	(0.0333,0.03334)) 
	[0.0333, 0.03331, 0.03332, 0.03333, 0.03334]
	
	>>> autoscale(	( 330, 335) )      
	[330, 331, 332, 333, 334, 335]
	
	>>> autoscale(	( 330, 335), d0=0 )      
	[0, 100, 200, 300, 400]
	
	>>> autoscale(	( 330, 350) )      
	[330, 335, 340, 345, 350]
	
	>>> autoscale(	( 330, 350), d0=0 )      
	[0, 100, 200, 300, 400]
	
	>>> autoscale(	( 330, 350), d0=335 )      
	[335, 340, 345, 350]

	>>> autoscale(	( 3330, 3335) )     
	[3330, 3331, 3332, 3333, 3334, 3335]
	
		
	B2: 
		-----------------------------------------                           
		(dmin, dmax)	drange	 ord of 10    d0
								 dm  dx  dr
		-------------- 	------	----------	----- 
		(0.03, 0.034),	0.004	-2, -2, -3	0.03 
		(0.033, 0.038), 0.005	-2, -2, -3	0.03 
		(0.033, 0.088), 0.055	-2, -2, -2	0.03 
		(0.33, 0.88),	0.55	-1, -1, -1	0.3  
		(3.3, 8.4),		5.1		 0,  0,  0	3.0
		(330,  840),	510      2,  2,  2  300
		
		(33.4, 33.8),	0.4		 1,  1, -1	33.4
		(33.4, 39.5),	6.1		 1,  1,  0	30.0
	
	The diff between B1 and B2 is that:
		B1:  dm = dx != dr  :  d0 = dmin
		B2:	dm = dx  = dr  :  d0 needs to recalc
		
	>>> autoscale((0.03, 0.034))		 
	[0.03, 0.031, 0.032, 0.033, 0.034, 0.035]

	>>> autoscale((0.033, 0.038)) 		
	[0.033, 0.034, 0.035, 0.036, 0.037, 0.038]
	
	>>> autoscale((0.033, 0.088)) 		
	[0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09]
	
	>>> autoscale((0.33, 0.88))		 
	[0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
	
	>>> autoscale((3.3, 8.4))			
	[3, 4, 5, 6, 7, 8, 9]

	>>> autoscale((33.4, 33.8))		
	[33.4, 33.5, 33.6, 33.7, 33.8]

	>>> autoscale((33.4, 39.5))		
	[33, 34, 35, 36, 37, 38, 39, 40]

	>>> autoscale((330,  840))	
	[300, 400, 500, 600, 700, 800, 900]
	 		
	 		
	========================================================	 
	Situation C: dmin < 0	
    ========================================================	 
	
	---------------------------------------------------------                           
	(dmin, dmax)	drange	 ord of 10    
							 dm  dx  dr	dmin	 d0
	-------------- 	------	----------	------	-------------
    (-0.84, 0.76),	1.6     -1, -1,  0
	(-4.6, 8.4),	13.0     0,  0   1
    
    >>> autoscale((-0.84, 0.76))	
    [-1.0, -0.5, 0.0, 0.5, 1.0]
	 
    >>> autoscale(( -4.6, 8.4 ))	
    [-5, 0, 5, 10]

	>>> autoscale(( -7, 12 ))
	[-10, -5, 0, 5, 10, 15]

	>>> autoscale(( -12, -7 ))	
	[-12, -11, -10, -9, -8, -7]

	Examples:
	
	>>> autoscale( (1,10) )
	[0, 2, 4, 6, 8, 10]

	>>> autoscale( (1,10), d0=0)
	[0, 2, 4, 6, 8, 10]
	 
	'''
	
	dmin, dmax = min(data), max(data) 
	drange = dmax - dmin
	
	def cutsci(n):                  # '3.1e-2'==> (3.1, -2)
		n= ('%e'%n).split('e')
		return float(n[0]), int(n[1])
	
	def shrinkitv(itv): # Multiply the itv by a factor of 0.5 or 0.4
	                    #  1x.05=0.5,  2d0.5=1.0,  5d0.4=2
		s,e = cutsci(itv)
		d = { 1:0.5, 2: 0.5, 5:0.4 }[ int(s) ]
		return itv*d 
		    
	#print 'drange= ', drange
		
	def getd0():
		ordmin = cutsci(dmin)[1] 	
		ordmax = cutsci(dmax)[1] 		
		ordrng = cutsci(scalerange)[1] 	
		
		## If the ordmax > that of ordmin, like examles below, 
		## [0.33, 2.84] => [0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0]
		## [6, 1760]    => [0, 500, 1000, 1500, 2000] 
		## we don't want the d0 to be 0.3 or 6
		##
		if ordmax> ordmin: 
			return 0
		elif ordrng!=ordmin: 
			return dmin
		else:	
			d0 = '%e'%dmin
			d0 = d0.split('e')
			xi = int( float(d0[0]) )
			xi = abs(xi)
			d0 = float('%se%s'%( xi, d0[1] ) )
			return d0 
	
	if d0 == None:
		scalerange = drange 
		d0 = getd0() 
	
	scalerange = dmax- d0
	#else:
	#	scalerange = dmax - d0 
		
	#print 'scalerange= ', scalerange
	#print 'd0 = ', d0
	
	itv = eval('1e%s'% cutsci(scalerange)[1] )
	
	## if data=(0,10), the itv would be 10, which is too big 
	## even after the itv/=2.0 later. So we make a first cut here:
	if scalerange== itv:  itv = shrinkitv(itv)
		       
	#print 'itv= ', itv
	
	#| Shrink the itv if the bin count is smaller than 3. 
	#| which means the itv is too large. 
	#| We use "if" but not "while" 'cos "while" will make
	#| itv too small. 
	#|
	#| while scalerange/(itv*1.0)< 3: itv/= 2.0
	#|
	if scalerange/(itv*1.0)<3: 
		
		#if int(base) == 5:   # means itv = 5 something
			
		itv = shrinkitv( itv ) #itv/= 2.0
		
	#print 'scalerange/(itv*1.0) = ', scalerange/(itv*1.0)
	
	#print 'itv= ', itv
		
	bc = int(math.ceil( (scalerange) / itv ))+1   # bin count
	
	#print 'bc = ', bc 
	
	#| If dmin<0 and dmax>0, then we want to make sure that 0  
	#| is among one of the scales, like : ( -5, 0, 5 ...)
	#| but not ( -2, 3, 8 )
	#|
	if dmin<0 and dmax>0:
		scaleR=[ itv-itv ]  ## Don't just use [0]. Need to keep decimal  
							## positions consistent. EX: 
							## [-1.5, 0.0, 1.5], but not [-1.5, 0, 1.5] 
		while scaleR[-1]<dmax:
			scaleR.append( len(scaleR)*itv )
		scaleL=[0]
		while scaleL[0]>dmin:
			scaleL = [-itv*len(scaleL)]+scaleL
		scales= scaleL[:-1]+ scaleR
	else:
		scales=  [ d0+i*itv for i in range(bc) ]
	
			    
	if itv == int(itv):  scales= map(int, scales )
	else: 
	    ## If float format, we don't want something like:
		## 3.9999999999999998 or 3.000000000000001
		scales = map( lambda x: float('%e'%x), scales )
	 
	return scales 
    	
    			
if __name__ == "__main__":  	
	import doctest 
	doctest.testmod()

	#print autoscale( (1,10))
