import pprint 

def ascfreqbarplot( data ): 
	'''

	>>> d= [(3,1), (4,2), (5,1), (6,2), (8,3), (9,1)]
	>>> dd = ascfreqbarplot(d)   
	>>> print '\\n'.join(dd) # [(3,1), (4,2), (5,1), (6,2), (8,3), (9,1)] #doctest: +NORMALIZE_WHITESPACE
	  +---------------------+
	3-|                 8   |
	2-|         4   6   8   |
	1-|       3 4 5 6   8 9 |
	0-+-0-1-2---------7-----+


	>>> d= [(5,2), (6,3), (7,2), (8,1), (9,1), (13,1)]
	>>> dd = ascfreqbarplot(d)   
	>>> print '\\n'.join(dd) # [(5,2), (6,3), (7,2), (8,1), (9,1), (13,1)]  #doctest: +NORMALIZE_WHITESPACE
	  +---------------------------------+
	3-|             6                   |
	2-|           5 6 7                 |
	1-|           5 6 7 8 9          13 |
	0-+-0-1-2-3-4-----------10-11-12----+
	
	
	>>> d= [(2,1), (3,4), (4,5), (5,3)]
	>>> dd = ascfreqbarplot(d)   
	>>> print '\\n'.join(dd) # [(2,1) (3,4), (4,5), (5,3)]   #doctest: +NORMALIZE_WHITESPACE
	  +-------------+
	5-|         4   |
	4-|       3 4   |
	3-|       3 4 5 |
	2-|       3 4 5 |
	1-|     2 3 4 5 |
	0-+-0-1---------+

	
    >>> d= [3,2,4,1,0,2,5,11,4,3,0,0,2]
    >>> dd = ascfreqbarplot(d)   
	>>> print '\\n'.join(dd) # [3,2,4,1,0,2,5,11,4,3,0,0,2]   #doctest: +NORMALIZE_WHITESPACE
	   +------------------------------+
	11-|               7              |
	10-|               7              |
	 9-|               7              |
	 8-|               7              |
	 7-|               7              |
	 6-|               7              |
	 5-|             6 7              |
	 4-|     2       6 7 8            |
	 3-| 0   2       6 7 8 9          |
	 2-| 0 1 2     5 6 7 8 9       12 |
	 1-| 0 1 2 3   5 6 7 8 9       12 |
	 0-+---------4-----------10-11----+

	'''						  
	 
	if type(data[0]) in (float, int):           # Turn (4,3,7,9]
		data=[(i,x) for i,x in enumerate(data)] # to:[(0,4),(1,3),(2,7),(3,9)]
	else:
		xs = [x[0] for x in data] 
		mx = max( xs )   ## max x
		
		dic = dict( data )
		data= [ (i, dic[i]) if i in dic else (i, 0) for i in range(mx+1) ]
		
	ys = [x[1] for x in data]  #: [0, 0, 0, 1, 3, 1, 1, 3, 0, 0, 1]
	ymax = max( ys )           #: max y, using to determine y 
	
	#print  'ys = ',ys
	#print 'ymax = ', ymax 
	#print 'data= ', data
	
	lines = [ [' ']*len(data) ]*ymax 
	
	#return lines
	#return data
	
	#[(2,1), (3,4), (4,5), (5,3)]
	# ymax = 5  (that in (4,5))
	'''
	ys = 0 0 1 4 5 3  ==> ymax = 5
	----------------------------         
	r=0:         4    -+--- +---
	r=1:       3 4     | r  |
	r=2:       3 4 5  -+--- |     
	r=3:       3 4 5   | y  | ymax=5
	r=4:     2 3 4 5   |    |
	----------------  -+--- +-
	c=x= 0 1 2 3 4 5 
	'''  
	out= []
	cond = 'r>=ymax-y'  
	for r, line in enumerate( lines ):
		#print '\nr%s, line='%r, line, ', cond=', cond
		#out.append([]) #[r]=[]
		row=[]
		for c,y in enumerate( ys ):
			
			#print '--- c%s, ymax=%s, y=%s, ymax-y=%s, %s:%s'%(c, ymax, y, ymax-y, cond, eval(cond))
			if eval(cond): #r>=ymax-y-1:
				row.append(str(c)) #line[c] = str(c) 
				#print '\t\t\tappending str(c): |%s|'%c
			else:
				row.append(' '*len(str(c)) ) 
				#print '\t\t\tappending ' '*len(str(c)): |%s|'%(' '*len(str(c)))
				#line[c] = ' '*len(str(c))
			#out[r].append( str((r,ymax-y, line[c])) ) #'r=%s, c=%s, y=%s'%(r,c,y) ) 
		out.append( row ) #' '.join( line ) )
		#print '\t\tappend row: |%s|'%row
		#lines[r] = line	
		#			line[c] = (r+1>=ymax-y) and str(c) or ' '*len(str(c))
		#out[r]=	str(out[r])
		
	#return '\n'.join( str(x) for x in lines )	
	#print 'lines = ', lines
	#pprint.pprint( out )
	#print 'lens of out: ', [len(x) for x in out]
	
	lines = out # [ ''.join(line) for line in out ] 
	
	#return '\n'.join( lines )
	 		
	last = [ '-'*len(str(i)) if lines[-1][i]!=' ' else str(i) 
	         for i in range(len(lines[0]) ) ]
	last = [ ( str(i) if lines[-1][i].strip()=='' else '-'*len(str(i)) ) 
	         for i in range(len(lines[0]) ) ]
	#print 'lines[-1] : ', lines[-1] # [len(x) for x in lines]
	#print 'last      : ', last # [len(x) for x in lines]
	#print 'lens of lines: ', [len(x) for x in lines]
	#print 'last = ', last         
	lines = [ '| , |'.replace(',', ' '.join(line)) for line in lines ]
	lines.append( '+-,-+'.replace(',', '-'.join(last)) ) 
	
	scalewidth = len(str(ymax))
	
	yscales = [' '*scalewidth]+ [ ('%'+ str(scalewidth)+'d')%(ymax-i)+'-' for i in range(ymax+1)]
	
	border = ' '+ ( '-'*(len(lines[0])-2)).join(('+','+'))
	
	lines  = [ border ] + lines #+ [ border ]
	lines = [ yscales[i]+ line for i,line in enumerate(lines) ]
	#lines.append( last )
	#lines[-1]= '| '+' '.join( line )+' |' #.replace(' ','_')+'_|'
	#lines.append(  '|-,-|'.replace(',', '-'*(len(lines[-1])-4 )) ) 
	
	#lines.append(  '|-,-|'.replace(',', '-'.join( [str(i) for i in range(len(line)) ]) ))	
	return lines #'\n'.join( lines )	
	
if __name__ == "__main__":  	
	import doctest 
	doctest.testmod()
	#d= [(5,2), (6,3), (7,2), (8,1), (9,1), (13,1)] 
	#print ascfreqbarplot(d) # [(2,1) (3,4), (4,5), (5,3)]

