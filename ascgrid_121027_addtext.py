# -*- coding: utf-8 -*-

import pprint,math
import inspect, doctest 
from copy import deepcopy  # deepcopy used in AscGrid.data 



def lineSetter(obj, attrprefix, args):
    args=list(args[0]) if (type(args[0])==list
               or type(args[0])==tuple) else list(args)
    filled2=False
    while len(args)>0:
        a=args.pop(0)
        if type(a)==bool: obj.__dict__[ attrprefix+'show']=a
        elif filled2: obj.__dict__[ attrprefix+'node']=a
        else:
            obj.__dict__[ attrprefix+'line']=a
            filled2=True


def blockSetter(obj, blockname='b', args=()):
    for pos in ('t', 'r', 'b', 'l'):
        lineSetter(obj, attrprefix=blockname+pos, args=args)
def frange(beg, end, step):
    '''
    Create range with float steps:
    http://stackoverflow.com/questions/477486/python-decimal-range-step-value
         
    >>> rng= frange(0.0, 1.0, 0.1)
    >>> ["%g" % x for x in rng]
    ['0', '0.1', '0.2', '0.3', '0.4', '0.5', '0.6', '0.7', '0.8', '0.9', '1']
    >>>
    '''
    r=beg 
    while r < end:
        yield r 
        r += step 
        

class GridCell(object):
    def __init__(self, text='',w=0,h=0):
        self.__dict__['w']=w
        self.__dict__['h']=h
        self.__dict__['text']=text
        def getw(self): return self.__dict__['w']
        def setw(self,v): __dict__['w']=v
        w=property(getw,setw)  
        def geth(self): return self.__dict__['h']
        def seth(self,v): __dict__['h']=v
        h=property(geth,seth)  
        def gettext(self): return self.__dict__['text']
        def settext(self,v): __dict__['text']=v
        text=property(gettext,settext)  

class AscGrid(object):
    '''

        >> ac= AscGrid()

        .corners = ['+','+','+','+']
        .lines = []

        ac.w
        ac.h



        ac.margin  .top  .right  .bottom  .left

        ac.border.top
                      .linesymb
                      .intersymb
                      .size

        ac.grid.vert|hori
                    .show
                    .symb

        ac.grid.inter
                    .show
                    .symb

        widths=None  # auto calc
        widths=[ 10,20,12 ...]

        printout :
            lines:[ ]


        '''
    def __init__(self, **kargs):
        #self.symbols={'default':{}}

        self.styles={ 'default': {
                        'style':'default'
                        , 't_align':'center'
                        , 'gshow': True
                        , 'corner': ''
                        , 'wline' : '-', 'hline':':', 'node' : '+'
                        #============ border                        
                        , 'btshow': False, 'btline': '=', 'btnode': 'o'
                        , 'brshow': False, 'brline': '|', 'brnode': 'o'
                        , 'bbshow': False, 'bbline': '=', 'bbnode': 'o'
                        , 'blshow': False, 'blline': '|', 'blnode': 'o'

                        }

                       , 'square':{ 'corner':('#', '#', '#', '#')
                              , 'wline':'-', 'hline':'|', 'node'  :'+'
                              , 'btshow':True, 'btline':'#' , 'btnode':'#'
                              , 'brshow':True, 'brline':'#' , 'brnode':'#'
                              , 'bbshow':True, 'bbline':'#' , 'bbnode':'#'
                              , 'blshow':True, 'blline':'#' , 'blnode':'#'
                            }

                        , 'func1':{'wline':'.', 'hline':' ', 'node':'.'
                                  , 'btshow':True, 'btline':'-' , 'btnode':'-'
                                  , 'brshow':True, 'brline':'|' , 'brnode':'+'
                                  , 'bbshow':True, 'bbline':'-' , 'bbnode':'-'
                                  , 'blshow':True, 'blline':'|' , 'blnode':'+'
                                  , 'corner':['+','+','+','+']
                               }
                        }  ## .styles        
        ''','line1':{ 'corner':('┌','┐','┘','└')
                              ,'btshow':True, 'btline':'─' ,'btnode':'┬'
                              ,'brshow':True, 'brline':'│' ,'brnode':'┤'
                              ,'bbshow':True, 'bbline':'─' ,'bbnode':'┴'
                              ,'blshow':True, 'blline':'│' ,'blnode':'├'
                              ,'node'  :'┼', 'wline' :'─','hline':'│'
                            }
                       '''
        self.ops={    'wunit' : 5
                    , 'wcount': 4
                    , 'wunits': []
                    , 'hunit' : 2
                    , 'hcount': 3
                    , 'hunits': []
                    , 'title' : '' #[title: AscGrid Demo]'
                    , 'header': '' #This line is ".header"'
                    , 'footer': '' #This line is ".footer"'

                    , 'pxdataset':[]
                    , 'textdataset':[]
                    #, 'merges':{}
                    , 'style': 'default'
        
                    , 'ymin':0
                    , 'ymax':None
                    , 'xmin':0 
                    , 'xmax':None
                    , 'xscalerange': None
                    , 'yscalerange': None 
                    , 'yscaleranger':None
                    , 'xformat': '%1d'   ## x axis scale number
                    , 'yformat': '%1d'   ## '%1.5f'%3.123  => '3.12300'
                    , 'yformatr':'%1d'   ## '%1.5f'%3.123  => '3.12300'
        
        
                    , 'xlabel'  : '' 
                    , 'ylabel'  : ''     ## y axis label will be shown in single-width vertical line
                    , 'ylabelr' : ''     ## y axis label will be shown in single-width vertical line
                    , 'legend' : False   ## Set True to show legend in legendformat; Set string to show string
                                         ## set function(dataset) to show function return
                    , 'legendformat': ['%(symb)s:%(name)s', ', ', '[,]']   ## [item, item seperator, wrapper]
                    
                    #, '_yaxisscaler' : False 
                    , 'autoscale': False  
                    , 'outerborder': False 
                    ## dataset:
                    ## [{symb:'o',name:'test',data:[(2,3),(3,5)...]}
                    ## ,{symb:'#',name:'test2',data:[(3,6),(5,8)...]}
                    ## ]

                    #,'margin':BlockProperties()   # TODO
                    }

        ##===================================================================
        ## Load default style             
        '''
        self.ops.update( self.styles['default'] )

        ## Try to load customized style if 'style="xxx"' is given in kargs
        ## and "xxx" is predefined as a style name in self.styles["xxx"].
        ##
        if 'style' in kargs :
            if not kargs['style'] in self.styles:
                kargs['style']='default'
            else:
                self.ops.update( self.styles[ kargs['style'] ] )
        '''
        #if 'ops' in kargs: kargs = ops 
        kargs=self.__set_ops__(**kargs)
        self.ops.update(kargs)
        if self.ops['xmax']==None: self.ops['xmax'] = self.ops['wunit']* self.ops['wcount']
        if self.ops['ymax']==None: self.ops['ymax'] = self.ops['hunit']* self.ops['hcount']
        
        self.__dict__.update(self.ops)
        self.pxdataset=deepcopy(self.pxdataset)
        self.__dict__['__lines__']=[]
        self.__dict__['__merges__']={}
        self.draw()
        

    def setstyle_bylabel(self, stylename):
        if stylename in self.styles:
            style=self.styles[stylename]
            self.ops.update(style)
            self.__dict__.update(style)
            self.__dict__['style']=stylename
    def getstyle(self, name):
        return self.__dict__['style']
    style=property(getstyle, setstyle_bylabel)
    #==============================================
    def getlines(self): return self.__dict__['__lines__'] # deepcopy(self.__dict__['__lines__']) #self.draw()
    lines=property(getlines)
    #============================================== border 
    def getbt(self): return [ self.btshow, self.btline, self.btnode]
    def setbt(self, *a): lineSetter(self, attrprefix='bt', args=a)
    bt=property(getbt, setbt)
    #------------------------------------         
    def getbr(self): return [ self.brshow, self.brline, self.brnode]
    def setbr(self, *a): lineSetter(self, attrprefix='br', args=a)
    br=property(getbr, setbr)
    #------------------------------------         
    def getbb(self): return [ self.bbshow, self.bbline, self.bbnode]
    def setbb(self, *a): lineSetter(self, attrprefix='bb', args=a)
    bb=property(getbb, setbb)
    #------------------------------------         
    def getbl(self): return [ self.blshow, self.blline, self.blnode]
    def setbl(self, *a): lineSetter(self, attrprefix='bl', args=a)
    bl=property(getbl, setbl)
    #==============================================
    def getb(self): return (self.bt, self.br, self.bb, self.bl)
    def setb(self, *a): blockSetter(self, blockname='b', args=a)
    b=property(getb, setb)
    #==============================================
    def getDataNames(self):
        return [ x['name'] for x in self.pxdataset ]
    datanames=property(getDataNames)
    #==============================================
    def getwidth(self):
        return self.wunits and sum(self.wunits,1) or self.wunit*self.wcount+1
    width=property(getwidth)
    def getheight(self):
        return self.hunits and sum(self.hunits,1) or self.hunit*self.hcount+1
    height=property(getheight)
    #==============================================
    def getwunits(self): return self.__dict__['wunits']
    def setwunits(self,lis): 
        self.wcount = len(lis)
        self.__dict__['wunits']= lis
    wunits = property( getwunits, setwunits)    
    def gethunits(self): return self.__dict__['hunits']
        #x= self.__dict__['wunits']
        #if not x: x = self.__dict__['wunits'] = [self.wunit]*self.wcount
        #return x
    def sethunits(self,lis): 
        self.hcount = len(lis)
        self.__dict__['hunits']= lis
    hunits = property( gethunits, sethunits)    
    #def getcells(self):
                
    def __set_ops__ (self, **kargs):
        '''
            Set kargs properly before it is sent to update the ops
            This feature might not need to be a class function
        '''
        self.ops.update(self.styles['default'])

        ## Try to load customized style if 'style="xxx"' is given in kargs
        ## and "xxx" is predefined as a style name in self.styles["xxx"].
        ## 
        if 'style' in kargs :
            if not kargs['style'] in self.styles:
                kargs['style']='default'
            else:
                self.ops.update(self.styles[ kargs['style'] ])
        ## The name of style that is in use can be obtained:
        ## 
        ## ag.AscGrid()
        ## ag.style ==> 'default'
        ## ag(style='xxx')
        ## ag.style ==> 'xxx' (if 'xxx' is found in self.styles) or 'default'
        #====================================================================

        return kargs

    def merge(self, pt1, pt2, *args):
        '''
        
        >>> ag=AscGrid( title='', header='', footer='', wunit=6, wcount=5, hcount=8 )
        >>> lines = ag( wunits = [4,4,20,4,4])
        >>> lines = ag( hunits = [2,2,2,2,8,2,2,2])
        >>> ag.merge( (1,1),'c', (2,1), 'c', (-1,1),'c' )  # title, header, footer
        >>> ag.merge( (5,1),(-4,1), (5,2),(-4,2) )         # txl, scl 
        >>> ag.merge( (5,-2),(-4,-2), (5,-1),(-4,-1) )     # txr, scr
        >>> ag.merge( (3,3),(3,-3), (4,3),(4,-3) )         # txt, sct
        >>> ag.merge( (-2,3), (-2,-3), (-3,3), (-3,-3) )   # txb, scb
        >>> ag.merge( (3,1),'r1c1', (3,-2), 'r1c1', (-3,1), 'r1c1', (-3,-2), 'r1c1')
        >>> ag.addpxdata( symb= 'title', pt=(1, 16) ) 
        >>> ag.addpxdata( symb= 'header', pt=(3, 16) ) 
        >>> ag.addpxdata( symb= 'txt', pt=(5, 16) ) 
        >>> ag.addpxdata( symb= 'sct', pt=(7, 16) ) 
        
        >>> ag.addpxdata( symb= 'scb', pt=(-6, 16))
        >>> ag.addpxdata( symb= 'txb', pt=(-4, 16))
        >>> ag.addpxdata( symb= 'footer', pt=(-2, 16) ) 
        >>> ag.addpxdata( symb= 'grid', pt=(12,16))
        
        >>> for i,c in enumerate('txl'):
        ...   ag.addpxdata( symb= c, pt=( 11+i, 2 ) )
        >>> for i,c in enumerate('scl'):
        ...   ag.addpxdata( symb= c, pt=( 11+i, 6 ) )
        
        >>> for i,c in enumerate('txr'):
        ...   ag.addpxdata( symb= c, pt=( 11+i, -3 ) )
        >>> for i,c in enumerate('scr'):
        ...   ag.addpxdata( symb= c, pt=( 11+i, -7 ) )
        
        > ag.pxdataset
        
        > print '\\n'.join( ag() )
        
        
        +---+---+-------------------+---+---+
        :               title               :
        +---+---+-------------------+---+---+
        :               header              :
        +---+---+-------------------+---+---+
        :       :       txt         :       :
        +       +-------------------+       +
        :       :       sct         :       :
        +---+---+-------------------+---+---+
        :   :   :                   :   :   :
        :   :   :                   :   :   :
        : t : s :                   : s : t :
        : x : c :       grid        : c : x :
        : l : l :                   : r : r :
        :   :   :                   :   :   :
        :   :   :                   :   :   :
        +---+---+-------------------+---+---+
        :       :       scb         :       :
        +       +-------------------+       +
        :       :       txb         :       :
        +---+---+-------------------+---+---+
        :               footer              :
        +---+---+-------------------+---+---+

        
        '''
        #print 'Entered merge, args: ',args
        if len(args)%2==1: args.pop()   # need pairs of points, so if one single out, drop it
        areas = [ (args[i], args[i+1]) for i,p in enumerate(args) if i%2==0]
        areas = [( pt1, pt2)]+ areas
        #print 'In merge, areas= ',areas
        for p1,p2 in areas:
            
            if type(p2)==str:      # 'r1', 'c2', 'r1c2'
                if p2=='c':  dh,dw = 0, self.wcount-p1[1]
                elif p2=='r': dh,dw = self.hcount-p1[0], 0
                else:
                    p2= p2.split('c')  # ['r1'], ['','2'], ['r1','2']                         
                    p= p2.pop()        # p='r1' ,'2', '2', leaving: p2= [],[''],['r1] 
                    if len(p2)>0:     # [''] or ['r1']
                          p2= p2.pop()
                          if p2: 
                              dh = int(p2.replace('r',''))
                          else:
                              dh = 0    
                          dw = int( p )
                    else:
                          dh = int(p.replace('r',''))
                          dw =  0 
                p2 = (p1[0]+dh, p1[1]+dw)    
            
            ## Allow negative coordination:       
            if p1[0]<0: p1 = ( p1[0] + self.hcount+1, p1[1] )
            if p1[1]<0: p1 = ( p1[0], p1[1]+ self.wcount+1 )
            if p2[0]<0: p2 = ( p2[0] + self.hcount+1, p2[1] )
            if p2[1]<0: p2 = ( p2[0], p2[1]+ self.wcount+1 )
            
            #if p2<0: p2 = self.width + p2 -5  
            #print 'p1,p2 = ', (p1,p2)
            self.__dict__['__merges__'][p1]= p2
        
        
    def getmerges(self): return self.__dict__['__merges__']
    merges = property(getmerges)   
    def drawmerges(self):
        '''
            >>> ag = AscGrid()
            >>> print '\\n'.join(ag(title='', header='',footer=''))
            +----+----+----+----+
            :    :    :    :    :
            +----+----+----+----+
            :    :    :    :    :
            +----+----+----+----+
            :    :    :    :    :
            +----+----+----+----+
            
            >>> ag.merge( (1,1),(1,2),   
            ...           (2,1),(3,1),
            ...           (1,4),(3,4),
            ...           (2,3),(3,3) )
            >>> print '\\n'.join(ag())
            +----+----+----+----+
            :         :    :    :
            +----+----+----+    +
            :    :    :    :    :
            +    +----+    +    +
            :    :    :    :    :
            +----+----+----+----+
            
            >>> ag.merges            # doctest: +NORMALIZE_WHITESPACE
            {(2, 3): (3, 3), 
             (1, 4): (3, 4), 
             (1, 1): (1, 2), 
             (2, 1): (3, 1)}
            
            
            >>> ag = AscGrid()
            >>> ag.merge( (2,2),(3,3),(1,4),(3,4) )
            >>> print '\\n'.join(ag(title='', header='',footer='', hcount=4))
            +----+----+----+----+
            :    :    :    :    :
            +----+----+----+    +
            :    :         :    :
            +----+         +    +
            :    :         :    :
            +----+----+----+----+
            :    :    :    :    :
            +----+----+----+----+
            
            
            >>> ag = AscGrid()
            >>> ag.merge( (1,1),'c1',   
            ...           (2,1),'r1',
            ...           (2,2),'r1c1',
            ...           (1,4),'r' )
            >>> print '\\n'.join(ag(title='', header='',footer='', hcount=4))
            +----+----+----+----+
            :         :    :    :
            +----+----+----+    +
            :    :         :    :
            +    +         +    +
            :    :         :    :
            +----+----+----+----+
            :    :    :    :    :
            +----+----+----+----+


            >>> ag = AscGrid()
            >>> lines = ag( title='', header='',footer='', wunits=[4,3,8,3,4], hunits=[2,3,4,2])
            >>> print '\\n'.join(ag())
            +---+--+-------+--+---+
            :   :  :       :  :   :
            +---+--+-------+--+---+
            :   :  :       :  :   :
            :   :  :       :  :   :
            +---+--+-------+--+---+
            :   :  :       :  :   :
            :   :  :       :  :   :
            :   :  :       :  :   :
            +---+--+-------+--+---+
            :   :  :       :  :   :
            +---+--+-------+--+---+


            >>> ag.merge( (1,1),'c1',   
            ...           (2,1),'r1',
            ...           (2,2),'r1c1',
            ...           (1,4),'r3' )
            >>> ag.merges                       # doctest: +NORMALIZE_WHITESPACE
            {(1, 4): (4, 4), 
             (1, 1): (1, 2), 
             (2, 1): (3, 1), 
             (2, 2): (3, 3)}
            >>> print '\\n'.join(ag())
            +---+--+-------+--+---+
            :      :       :  :   :
            +---+--+-------+  +---+
            :   :          :  :   :
            :   :          :  :   :
            +   +          +  +---+
            :   :          :  :   :
            :   :          :  :   :
            :   :          :  :   :
            +---+--+-------+  +---+
            :   :  :       :  :   :
            +---+--+-------+--+---+

            >>> ag.merge( (2,2), (3,4) )  
            >>> print '\\n'.join(ag())
            +---+--+-------+--+---+
            :      :       :  :   :
            +---+--+-------+  +---+
            :   :             :   :
            :   :             :   :
            +   +             +---+
            :   :             :   :
            :   :             :   :
            :   :             :   :
            +---+--+-------+  +---+
            :   :  :       :  :   :
            +---+--+-------+--+---+




        '''
        wus, hus = self.wunits, self.hunits
        if not wus: wus= [self.wunit]*self.wcount
        if not hus: hus= [self.hunit]*self.hcount
        ws= self.width
        #print 'wus = ', wus, ', ws=', ws
        lines = self.lines
        covered=[]
        #print 'merges = ', self.merges 
        '''
            .merges = { (1,2):(1,1) }  
        '''
        #def makeline(head, symb, unitsizes, node):
        #    return head+ ''.join([ (symb*(us-1)+node) for us in unitsizes])
        merge_p1_list = self.merges.keys()
        merge_p1_list.sort()
        for p1 in merge_p1_list:  #self.merges.items():
            p2 = self.merges[p1]
            if p1 not in covered:
                r1,c1= p1
                r2,c2= p2
                #print 'r1,c1,r2,c2= (', (r1,c1), '), (', (r2,c2), ')' 
                rowbeg = sum( [ h for h in hus[:r1-1] ])+1
                rowend = sum( [ h for h in hus[:r2] ])
                
                #print 'row range: %s, %s'%(rowbeg,rowend)
                for r in range(rowbeg,rowend):  
                    #cellidx = [ range( p1[1], p2[1])]
                    colbeg = sum( [ w for w in wus[:c1-1] ])+ 1
                    colend = sum( [ w for w in wus[:c2] ])
                
                    #print 'col range: %s, %s'%(colbeg,colend)
                    #wids = sum([ w for w in wus[p1[1]:p2[1]+1] ])
                    line = self.lines[r]
                    
                    lines[r]= line[0]+line[1:colbeg] +' '* (colend-colbeg)+ line[colend:]
                    
        self.__dict__['__lines__']= lines
    
    def drawmerges_test(self): 
        cmd=''
        print '#'*65
        ag= AscGrid()
        lines= ag(title='',header='',footer='')
        def prn(lines, cmd=cmd):
            print cmd
            #eval(cmd)
            for line in lines:
                print line
        prn( lines )
        print 'Q'*50
        
        cmd = "ag.merge((1,1),'c1')"
        #ag.merge( (1,1),'c1' )
        eval( cmd )
        prn( ag(), cmd )
        print 'ag.wcount= ', ag.wcount          
        cmd = "ag( wunits=[3,6,4,5,2,3], hunits=[2,3,2,4])"          
        lines= eval(cmd)          
        prn( lines, cmd )          
        print 'ag.wunits= ', ag.wunits
        print 'ag.wcount= ', ag.wcount          
                  
        cmd= 'ag.merge( (2,1),"c", (3,2),"r1c1")'
        eval(cmd)          
        prn( ag(), cmd )          
                  
        cmd= 'ag.merge( (2,1),"c", (3,2),"r1c6")'
        eval(cmd)          
        prn( ag(), cmd )          
            
        #ag.merge((1,1),(3,1)) 
        #for line in  ag(title='',header='',footer=''):
        #    print line
        
        '''
        print '@'*50
        #ag.merge((2,2),(2,4)) 
        ag.merge((2,2),'r2')
        print "ag.merge((2,2),'r2')"
        for line in  ag(title='',header='',footer=''):
           print line
        
        print '@'*50
        #ag.merge((3,2),(3,3)) 
        ag.merge( (3,2),'c1')
        print "ag.merge( (3,2),'c1')"
        for line in  ag(title='',header='',footer=''):
           print line
        
        print '@'*50
        #ag.merge((2,1),(3,1)) 
        ag.merge((2,1),'r2')
        print "ag.merge((2,1),'r2')"
        for line in  ag(title='',header='',footer=''):
           print line
   
        print '#'*50
        ag= AscGrid()
        ag.wcount=10
        ag.wunit=3
        ag.hcount=6
        #ag(wcount=10,wunit=3,hcount=6)
        for line in  ag(title='',header='',footer=''):
           print line
   
        #ag.merge((1,1),(1,10))
        ag.merge((1,1),'c')
        print "ag.merge((1,1),'c')"
        for line in  ag(title='',header='',footer=''):
           print line
   
        ag.merge((6,4),'c')
        print "ag.merge((2,1),'c')"
        for line in  ag(title='',header='',footer=''):
           print line
   
        #ag.merge((6,1),(6,10))
        #ag.merge((2,10),(5,10))
        #ag.merge((2,1),(5,1))
        ag.merge((5,2),'c',(2,10),'r2',(2,1),'r4')
        print "ag.merge((5,2),'c',(2,10),'r3',(2,1),'r3')"
        for line in  ag(title='',header='',footer=''):
           print line
        
        ag=AscGrid()
        for line in  ag(wcount=5,wunit=3,hcount=5,title='',header='',footer=''):
           print line
        #ag.merge((2,2),(4,4))   
        ag.merge( (2,2),'r2c2')
        print "ag.merge( (2,2),'r2c2')"
        for line in  ag(title='',header='',footer=''):
           print line
        '''
            
    def draw(self, **kargs):
        #print '\n\n\Entered draw, before grawgrid, kargs= ', kargs
        self.drawgrid(**kargs)
        #print 'in draw(), after drawgrid, before drawOuterText'
        #print 'in draw(), after drawOuterText, before drawdata'
        if len(self.merges)>0: self.drawmerges()
        if len(self.pxdataset)>0: self.drawpxdata() 
        if len(self.textdataset)>0: self.drawtextdata() 
        
        self.drawaxes()
        
        self.drawOuterText()
        
        if self.outerborder:
            lines = self.__dict__['__lines__']
            h,w,node ='|','-','+'
            wid = max( [ len(line) for line in lines ])
            lines= [ line.ljust(wid,' ') for line in lines ]
            lines= [ node+ w*(wid+2) + node ] + [ h+' '+ line + ' '+h for line in lines ]+ [ node+ w*(wid+2) + node ]
            self.__dict__['__lines__'] = lines
        
        #print 'in draw, after drawdata, before leaving'
        #return deepcopy(self.__dict__['__lines__'])

    def drawaxes(self):
        dic= self.__dict__
        lines= dic['__lines__']
        
        H = self.height       
        
        ##
        ## yaxis  
        ##
        ## _yaxisscalel and r are calculated in drawpxdata() from yscalerange and
        ## should not be user-assigned.
        ##
        heads= dic.get('_yaxisscalel', ['']*len(lines) )  ## _yaxisscalel and _yaxisscaler are lists of same-length strings
        tails= dic.get('_yaxisscaler', ['']*len(lines) )
        #print 'in drawaxes, tails= ', tails
        ##
        ## yaxis - right
        ##
        if tails==True:
            tails = [ ' '+x.strip().ljust( len(x)) for x in heads ]
        elif tails==False: tails= ['']*len(lines)
        '''
        else:                                       
            t,b,h= tails[1], tails[0], tails[1]-tails[0]    
            u = h/(self.hcount*1.0)
            tails=[]
            for r in range(len(lines)):
                x = r%self.hunit==0 and self.yformatr%(h - u*(r/self.hunit)+b) or ''
                tails.append( x )
            wid = max( [len(x) for x in tails])
            tails = [ ' '+ x.ljust(wid,' ') for x in tails ]
        '''    
        
        ynr=['']* H
        if self.ylabelr:
            ynr= ((H-len(self.ylabelr))/2) * ' ' + self.ylabelr
            ynr= ynr + (H-len(ynr)) * ' '
            ynr = [' '+y for y in ynr ]
        
        
        yn=['']* H
        if self.ylabel:
            yn= ((H-len(self.ylabel))/2) * ' ' + self.ylabel
            yn= yn + (H-len(yn)) * ' '
            yn = [y+' ' for y in yn ]
            
        lines = [ yn[r] + heads[r] + lines[r] + tails[r] + ynr[r] for r in range(len(lines)) ]
        
        yaxiswidth = ' '*( len(yn[0]) + len(heads[0]) )    
          
        ##    
        ## xaxes 
        ##
        if '_xaxisscalet' in dic and dic['_xaxisscalet']: lines=[ yaxiswidth + self._xaxisscalet ]+lines        
        if '_xaxisscaleb' in dic and dic['_xaxisscaleb']: lines=lines+ [ yaxiswidth + self._xaxisscaleb ]
        
        if self.xlabel: lines=lines+ [ yaxiswidth + self.xlabel.center(self.width, ' ') ]
        
        ##
        ## legend
        ##
        if self.legend==True:
            fmt = self.legendformat         # ['(%(symb)s:%(name)s)', ', ', '[,]']
            leg = [ fmt[0]%D for D in self.pxdataset ]
            lines.append( fmt[2].replace(',', fmt[1].join( leg ) ) )
        elif type(self.legend)==str : lines.append( self.legend )
        elif inspect.isfunction(self.legend):
             lines+= [ yaxiswidth+li for li in ('%s'%self.legend( self.pxdataset)).split('\n')]
                
        dic['__lines__']=lines
        #return lines      
        
        
    def drawOuterText(self):
        lines=self.__dict__['__lines__']
        W = len(lines[0])
        if self.header:   lines=[ self.header ]+lines
        if self.title:
            t=self.title
            if self.t_align=='center':
                #t=t.center(self.wunit*self.wcount+1, ' ')
                t=t.center( W, ' ')
            lines=[ t]+lines
        
        '''self.footer = 'h(%s),w(%s), y(%s~%s), x(%s~%s)'%(self.height, self.width,
                                                         self.ymin, self.ymax,
                                                         self.xmin, self.xmax)
        '''    
        if self.footer:   lines=lines+[self.footer]
        self.__dict__['__lines__']=lines
        #return lines      

    def drawgrid(self, **kargs):
        '''

            ___Basic___

                >>> ag = AscGrid()

                ag.draw(...) to redraw the grid. Then use the ag.lines to get a copy 
                of internal text lines. Or, 
                ag(...) to redraw and return in one step.

                >>> print( '\\n'.join(ag(title='[title: AscGrid Demo]',
                ...                   header='This line is ".header"',footer='This line is ".footer"')) )
                [title: AscGrid Demo]
                This line is ".header"
                +----+----+----+----+
                :    :    :    :    :
                +----+----+----+----+
                :    :    :    :    :
                +----+----+----+----+
                :    :    :    :    :
                +----+----+----+----+
                This line is ".footer"

                >>> ag.title
                '[title: AscGrid Demo]'
                >>> ag.header
                'This line is ".header"'
                >>> ag.footer
                'This line is ".footer"'

                >>> ag.width, ag.height
                (21, 7)

                >>> ag.title= '--AscGrid Demo--'
                >>> ag.draw()
                >>> print( '\\n'.join(ag.lines) )
                   --AscGrid Demo--  
                This line is ".header"
                +----+----+----+----+
                :    :    :    :    :
                +----+----+----+----+
                :    :    :    :    :
                +----+----+----+----+
                :    :    :    :    :
                +----+----+----+----+
                This line is ".footer"


                The 3 lines:

                    ag.title='xxx'
                    ag.draw()
                    ag.lines

                can be simplified to:

                    ag(title='xxx')

                Both will get a list of string.


            ___Size___

                >>> lines=ag( title='',
                ...           header='', hunit=3, hcount=2,
                ...           footer='hunit=3, hcount=2')
                >>> print('\\n'.join(lines))
                +----+----+----+----+
                :    :    :    :    :
                :    :    :    :    :
                +----+----+----+----+
                :    :    :    :    :
                :    :    :    :    :
                +----+----+----+----+
                hunit=3, hcount=2

                >>> ag.width, ag.height
                (21, 7)

                >>> lines=ag( hunit=2, hcount=3, wunit=6, wcount=3,
                ...           footer='hunit=2, hcount=3, wunit=6, wcount=3')
                >>> print('\\n'.join(lines))
                +-----+-----+-----+
                :     :     :     :
                +-----+-----+-----+
                :     :     :     :
                +-----+-----+-----+
                :     :     :     :
                +-----+-----+-----+
                hunit=2, hcount=3, wunit=6, wcount=3

                >>> ag.width, ag.height
                (19, 7)
            

            ___Style/Basic___


                >>> lines=ag(title="Styles", 
                ...          wline='*', hline='!', node='x',
                ...          footer="wline='*', hline='!', node='x'")
                >>> print('\\n'.join(lines))
                       Styles      
                x*****x*****x*****x
                !     !     !     !
                x*****x*****x*****x
                !     !     !     !
                x*****x*****x*****x
                !     !     !     !
                x*****x*****x*****x
                wline='*', hline='!', node='x'



            ___Style/Border/Basic___

                .bt is a property to set border-top style

                When drawing, 3 attributes are used to determine bt style:
                    .btshow:  True|False for show/hide
                    .btline:  '=' to set line symble to =
                    .btnode:  'x' to set node symble to x

                They can be set individually using one of the following:

                    ag.btshow = True       # no immediate redraw
                    ag(btshow = True)      # immediate redraw

                or set in a bundle through .bt property:

                    ag.bt= True
                    ag.bt= True, '='
                    ag.bt= '=','0'
                    ag.bt= True,'=','0'


                What .bt return is a run-time generated list:

                >>> ag.bt
                [False, '=', 'o']

                representing btshow, btline, btnode, respectively.
                 
                Since [False, '=', 'o'] doesn't exist as a list inside 
                the class, changing it directly won't have any effect:
                
                border= ag.bt
                border[0] = True    <==== this won't work 
                
                ------------------------
                >>> ag.bt= True
                >>> ag.bt
                [True, '=', 'o']

                >>> ag.bt= "@"
                >>> ag.bt
                [True, '@', 'o']

                ------------------------

                >>> ag.bt= False, "#"
                >>> ag.bt
                [False, '#', 'o']

                >>> ag.bt= '=','#'
                >>> ag.bt
                [False, '=', '#']

                ------------------------
                >>> ag.bt= True, '-','x'
                >>> ag.bt
                [True, '-', 'x']

                =============== Can set btshow, btline and btnode directly
                >>> ag.btnode = '#'
                >>> ag.bt
                [True, '-', '#']
                >>> print('\\n'.join(ag()))
                       Styles      
                #-----#-----#-----#
                !     !     !     !
                x*****x*****x*****x
                !     !     !     !
                x*****x*****x*****x
                !     !     !     !
                x*****x*****x*****x
                wline='*', hline='!', node='x'


                Set style name to the "default" style:

                    ag.style='default'   or
                    ag(style='default')

                >>> ag.style='default'   # This will set all borders to False
                >>> ag.bt
                [False, '=', 'o']
                >>> print('\\n'.join(ag()))
                       Styles      
                +-----+-----+-----+
                :     :     :     :
                +-----+-----+-----+
                :     :     :     :
                +-----+-----+-----+
                :     :     :     :
                +-----+-----+-----+
                wline='*', hline='!', node='x'



            ___Style/Border/Adv___


                Set all borders in a bundle

                >>> print('\\n'.join(ag(title='',footer='')))
                +-----+-----+-----+
                :     :     :     :
                +-----+-----+-----+
                :     :     :     :
                +-----+-----+-----+
                :     :     :     :
                +-----+-----+-----+
                
                .b gives the info of all borders t,r,b,l
                
                >>> ag.b                      # doctest: +NORMALIZE_WHITESPACE
                ([False, '=', 'o'], [False, '|', 'o'],
                 [False, '=', 'o'], [False, '|', 'o'])


                We turn on the r,b,l (right, bottom, left) borders: 
                
                >>> ag.brshow = True
                >>> ag.bbshow = True
                >>> ag.blshow = True
                >>> print('\\n'.join(ag(title='',footer='')))
                o-----+-----+-----o
                |     :     :     |
                o-----+-----+-----o
                |     :     :     |
                o-----+-----+-----o
                |     :     :     |
                o=====o=====o=====o

                
                >>> ag.style='default'

                >>> ag.b = True, '#', '@'     # set all borders

                >>> ag.b                      # doctest: +NORMALIZE_WHITESPACE
                ([True, '#', '@'], [True, '#', '@'],
                 [True, '#', '@'], [True, '#', '@'])
                >>> print('\\n'.join(ag()))
                @#####@#####@#####@
                #     :     :     #
                @-----+-----+-----@
                #     :     :     #
                @-----+-----+-----@
                #     :     :     #
                @#####@#####@#####@



                Note: when .gshow (grid show) is false, the grid lines will
                be hidden, then all borders will be turned on automatically, 
                even the individual .b?show is False. 


                >>> ag.style='default'
                >>> lines= ag(gshow=False, title='When grid hidden',
                ...                        footer='border shown automatically')
                >>> print('\\n'.join(lines))
                  When grid hidden 
                o=====o=====o=====o
                |                 |
                o                 o
                |                 |
                o                 o
                |                 |
                o=====o=====o=====o
                border shown automatically

                Note that the individual border setting is still 'False':

                >>> ag.bb
                [False, '=', 'o']



            ___Style/Corner___


                >>> lines= ag(style='default', corner='#',
                ...           title='Set corner', footer="corner='#'")
                >>> print('\\n'.join(lines))
                     Set corner    
                #-----+-----+-----#
                :     :     :     :
                +-----+-----+-----+
                :     :     :     :
                +-----+-----+-----+
                :     :     :     :
                #-----+-----+-----#
                corner='#'


                >>> lines= ag(style='default', corner=('0','1','2','3'),
                ...     title='Set corner', footer="corner='('0','1','2','3')'")
                >>> print('\\n'.join(lines))
                     Set corner    
                0-----+-----+-----1
                :     :     :     :
                +-----+-----+-----+
                :     :     :     :
                +-----+-----+-----+
                :     :     :     :
                3-----+-----+-----2
                corner='('0','1','2','3')'


                >>> lines= ag(  gshow=False, title='Set corner',
                ...     footer="Corner overwrites border settings")
                >>> print('\\n'.join(lines))
                     Set corner    
                0=====o=====o=====1
                |                 |
                o                 o
                |                 |
                o                 o
                |                 |
                3=====o=====o=====2
                Corner overwrites border settings


            ___Style/Loading___


                Use
                   ag.style='default'  or
                   ag(style='default')

                to reset the style to the default. The 2nd case, ag(...),
                forces a redraw immediately and returns the lines.

                >>> lines=ag( title="Load default",
                ...           style='default',
                ...           footer="style='default'")
                >>> print('\\n'.join(lines))
                    Load default   
                +-----+-----+-----+
                :     :     :     :
                +-----+-----+-----+
                :     :     :     :
                +-----+-----+-----+
                :     :     :     :
                +-----+-----+-----+
                style='default'


                >>> ag.styles['mystyle']={ 'wline':'=', 'node':'O', 'hline':'|' }
                >>> lines= ag( title="Load my style", style='mystyle',
                ...            footer='style="mystyle"')
                >>> print('\\n'.join(lines))
                   Load my style   
                O=====O=====O=====O
                |     |     |     |
                O=====O=====O=====O
                |     |     |     |
                O=====O=====O=====O
                |     |     |     |
                O=====O=====O=====O
                style="mystyle"


                # chk current styles in store.
                >>> sk = ag.styles.keys() 
                >>> sk.sort() or sk
                ['default', 'func1', 'mystyle', 'square']
                
                >> print('\\n'.join( [str(len(x)) for x in lines] ) )

                >>> lines= ag(title="Load my style", style='square',
                ...           footer='style="square"')

                >>> print('\\n'.join(lines))
                   Load my style   
                ###################
                #     |     |     #
                #-----+-----+-----#
                #     |     |     #
                #-----+-----+-----#
                #     |     |     #
                ###################
                style="square"

                
            ___Size/Adv__
            
            
                >>> lines=ag( style='default', title='', wunits= [3,7,2],
                ...           footer='Varying cell widths, wunits=[3,7,2]')
                >>> print('\\n'.join(lines))
                +--+------+-+
                :  :      : :
                +--+------+-+
                :  :      : :
                +--+------+-+
                :  :      : :
                +--+------+-+
                Varying cell widths, wunits=[3,7,2]
                
                >>> ag.wunits, ag.hunits
                ([3, 7, 2], [])
                >>> ag.width, ag.height
                (13, 7)
                
                >>> lines=ag( hunits= [3,1,4],
                ...           footer='Varying cell heights, hunits=[3,1,4]')
                >>> print('\\n'.join(lines))
                +--+------+-+
                :  :      : :
                :  :      : :
                +--+------+-+
                +--+------+-+
                :  :      : :
                :  :      : :
                :  :      : :
                +--+------+-+
                Varying cell heights, hunits=[3,1,4]

                >>> ag.wunits, ag.hunits
                ([3, 7, 2], [3, 1, 4])

                >>> ag.width, ag.height
                (13, 9)
                

        '''

        #print
        #print 'Entered draw, dataset: ', self.pxdataset
        #print 

        ##==========================================
        ## Set customized style.
        ##
        if 'style' in kargs:                    ## if style name given    
            if kargs['style'] in self.styles:   ## if it exists in styles
                self.__dict__.update(self.styles[ kargs['style']])
            else:
                del kargs['style']  ## del invalid style name given in kargs.
                                    ## All kargs will be updated to __dict__
                                    ## so later on we can check the current
                                    ## style name with: .style. If an invalid
                                    ## name not del, it will be assigned to
                                    ## __dict__. In that case, the .style= 
                                    ## check will give wrong info.  

        ## Chk border setting. If any of ('bt','br','bb','bl') given in kargs,
        ## say, 'bt', kargs['bt']=v, we need to filter v throu setbt(x) to
        ## set btshow, btline and/btnode, and return the modified v, which is
        ## a list like [True, '=','+'] and be assigned to the .ops. See 
        ## lineSetter() code for details. 
        ##
        for b2 in ('bt', 'br', 'bb', 'bl'):
            if b2 in kargs:
                x=kargs[b2]                          # EX: True 
                self.__getattribute__('set'+b2)(x)  # EX: self.setbt(x)
                kargs[b2]=self.__getattribute__(b2)  # EX: [True,'=','+']

        ## 
        if 'wunits' in kargs:
            kargs['wcount']= len(kargs['wunits'])
        #else:
        #    if self.wunits==[]: self.wunits= [self.wunit] * self.wcount      
        if 'hunits' in kargs:
            kargs['hcount']= len(kargs['hunits'])
        #else:
        #    if self.hunits==[]: self.hunits= [self.hunit] * self.hcount      
        
        
        self.__dict__.update(kargs)
        self.ops.update(kargs)      ## Also update them to self.ops so we can
                                    ## chk ops for current parameters.

        wu=self.wunit
        wc=self.wcount #= len(self.wunits)
        wl=self.wline
        hu=self.hunit
        hc=self.hcount
        hl=self.hline

        #if not self.wunits: self.wunits= [wu]* wc
        #if not self.hunits: self.hunits= [hu]* hc

        wus = self.wunits
        hus = self.hunits
        
        if not wus: wus = [wu]* wc
        if not hus: hus = [hu]* hc 
        
        #def makeline(head, symb, unitsize, node, repeat):
        #    return head+(symb*(unitsize-1)+node)*repeat

        def makeline(head, symb, unitsizes, node):
            return head+ ''.join([ (symb*(us-1)+node) for us in unitsizes])

        ##===================================================
        ## build x (line), only two types of x:
        ##
        ## la=makeline(self.node, wl, wu, self.node, wc)   ## +--+--+--+ ...--+
        ## lb=makeline(hl, ' ', wu, hl, wc)                ## |  |  |  |
        la=makeline(self.node, wl, wus, self.node)   ## +--+--+--+ ...--+
        lb=makeline(hl, ' ', wus, hl)                ## |  |  |  |
        lines=[la]
        
        ''''''
        _hus = [a for a in hus]
        if self.gshow:
            #[ lines.append(r%hu==0 and la or lb) for r in range(1, hu*hc+1) ]
            while len(_hus)>0:
                r = _hus.pop(0)
                [ lines.append(lb) for rr in range(r-1) ]
                lines.append( la )
        else: # no gridlines
            [ lines.append(' '*wu*wc+' ') for r in range(1, hu*hc+1) ]
            
        
        
        ##===================================================
        ## draw borders:
        ##
        bt, br, bb, bl=self.bt, self.br, self.bb, self.bl   # [True,'|','#']
        if not self.gshow:
            bt[0], br[0], bb[0], bl[0]=(True, True, True, True)

        #if bt[0]: lines[0]=makeline(bt[2], bt[1], wu, bt[2], wc)
        #if bb[0]: lines[-1]=makeline(bb[2], bb[1], wu, bb[2], wc)
        if bt[0]: lines[0]=makeline(bt[2], bt[1], wus, bt[2])
        if bb[0]: lines[-1]=makeline(bb[2], bb[1], wus, bb[2])

        for i, line in enumerate(lines):
            L=(i%hu==0) and bl[2] or bl[1]
            R=(i%hu==0) and br[2] or br[1]
            if bl[0]: lines[i]=L+lines[i][1:]
            if br[0]: lines[i]=lines[i][:-1]+R
        ##===================================================

        ##===============================
        ## draw corners:
        c=self.corner
        if c:
            if type(c)==str: c=[c]*4
            lines[0]=c[0]+lines[0][1:-1]+c[1]
            lines[-1]=c[3]+lines[-1][1:-1]+c[2]

        self.__dict__['__lines__']=lines

        #return lines

    #==============================================
    def addtext(self, text
                , pt=None 
                , yaxisr= False
                ): 
                             
        '''
            >>> ag= AscGrid()  
            >>> ag.addpxdata( pt=(7,3) ) 
            >>> ag.pxdataset                
            [{'symb': 'o', 'yaxisr': False, 'data': [(7, 3)], 'name': '#1'}]


            >>> print '\\n'.join( ag( ) )          # doctest: +NORMALIZE_WHITESPACE
            6 +----+----+----+----+
              :    :    :    :    :
            4 +----+----+----+----+
              :    : o  :    :    :
            2 +----+----+----+----+
              :    :    :    :    :
            0 +----+----+----+----+
              0    5   10   15   20
        
            >>> ag.addtext( text=' <==', pt=(8,3) )
            >>> ag.pxdataset 
            [{'symb': 'o', 'yaxisr': False, 'data': [(7, 3)], 'name': '#1'}]

            >>> print '\\n'.join( ag( ) )          # doctest: +NORMALIZE_WHITESPACE
            6 +----+----+----+----+
              :    :    :    :    :
            4 +----+----+----+----+
              :    : o <==   :    :
            2 +----+----+----+----+
              :    :    :    :    :
            0 +----+----+----+----+
              0    5   10   15   20
       
            >>> ag.addpxdata(  (17,5)  )
            >>> ag.pxdataset                       # doctest: +NORMALIZE_WHITESPACE
            [{'symb': 'o', 'yaxisr': False, 'data': [(7, 3)], 'name': '#1'}, 
             {'symb': 'x', 'yaxisr': False, 'data': [(17, 5)], 'name': '#2'}]
             
            >>> print '\\n'.join( ag( ) )          # doctest: +NORMALIZE_WHITESPACE
            6 +----+----+----+----+
              :    :    :    : x  :
            4 +----+----+----+----+
              :    : o <==   :    :
            2 +----+----+----+----+
              :    :    :    :    :
            0 +----+----+----+----+
              0    5   10   15   20            
            
            >>> ag.addpxdata( (2, 1), 'New', 'N')
            >>> ag.pxdataset                       # doctest: +NORMALIZE_WHITESPACE
            [{'symb': 'o', 'yaxisr': False, 'data': [(7, 3)], 'name': '#1'}, 
             {'symb': 'x', 'yaxisr': False, 'data': [(17, 5)], 'name': '#2'}, 
             {'symb': 'N', 'yaxisr': False, 'data': [(2, 1)], 'name': 'New'}]

             
            >>> print '\\n'.join( ag( ) )          # doctest: +NORMALIZE_WHITESPACE
            6 +----+----+----+----+
              :    :    :    : x  :
            4 +----+----+----+----+
              :    : o <==   :    :
            2 +----+----+----+----+
              : N  :    :    :    :
            0 +----+----+----+----+
              0    5   10   15   20
            
            >>> ag.textdataset= []
            >>> ag.addpxdata( (13, 2))
            >>> print '\\n'.join( ag( ) )       # doctest: +NORMALIZE_WHITESPACE
            6 +----+----+----+----+
              :    :    :    : x  :
            4 +----+----+----+----+
              :    : o  :    :    :
            2 +----+----+--*-+----+
              : N  :    :    :    :
            0 +----+----+----+----+
              0    5   10   15   20
            
                                                ## append data
            
            >>> ag.addpxdata(  pt=(10, 4), name='#2', append=True  )
            >>> ag.pxdataset                       # doctest: +NORMALIZE_WHITESPACE
             [{'symb': 'o', 'yaxisr': False, 'data': [(7, 3)], 'name': '#1'}, 
              {'symb': 'x', 'yaxisr': False, 'data': [(17, 5), (10, 4)], 'name': '#2'}, 
              {'symb': 'N', 'yaxisr': False, 'data': [(2, 1)], 'name': 'New'}, 
              {'symb': '*', 'yaxisr': False, 'data': [(13, 2)], 'name': '#4'}]             

            
            >>> print '\\n'.join( ag( legend=True) )           # doctest: +NORMALIZE_WHITESPACE
            6 +----+----+----+----+
              :    :    :    : x  :
            4 +----+----x----+----+
              :    : o  :    :    :
            2 +----+----+--*-+----+
              : N  :    :    :    :
            0 +----+----+----+----+
              0    5   10   15   20  
            [o:#1, x:#2, N:New, *:#4]
            
                                                  ## getdata by name
            
            >>> ag.getpxdata('#1')           # doctest: +NORMALIZE_WHITESPACE
            {'symb': 'o', 'yaxisr': False, 'data': [(7, 3)], 'name': '#1'}

            
            >>> ag.delpxdata('#1')              ## delete data
            >>> print '\\n'.join( ag( ) )        # doctest: +NORMALIZE_WHITESPACE
            6 +----+----+----+----+
              :    :    :    : x  :
            4 +----+----x----+----+
              :    :    :    :    :
            2 +----+----+--*-+----+
              : N  :    :    :    :
            0 +----+----+----+----+
              0    5   10   15   20  
            [x:#2, N:New, *:#4]


                                               ## symb can be a string
                                               
            >>> ag.addpxdata(pt=(17,1), symb="Long_text_label")
            
            >>> print '\\n'.join( ag( ) )      # doctest: +NORMALIZE_WHITESPACE
             6 +----+----+----+----+
              :    :    :    : x  :
            4 +----+----x----+----+
              :    :    :    :    :
            2 +----+----+--*-+----+
              : N  :    :    : Long_text_label
            0 +----+----+----+----+
              0    5   10   15   20  
            [x:#2, N:New, *:#4, Long_text_label:#5]

             
            
            >>> ag.addpxdata( pt=[ (3,2),(5,3),(7,4),(10,5) ], name='L1')
            
            >>> print '\\n'.join( ag( ) )      # doctest: +NORMALIZE_WHITESPACE
            6 +----+----+----+----+
              :    :    o    : x  :
            4 +----+-o--x----+----+
              :    o    :    :    :
            2 +--o-+----+--*-+----+
              : N  :    :    : Long_text_label
            0 +----+----+----+----+
              0    5   10   15   20  
            [x:#2, N:New, *:#4, Long_text_label:#5, o:L1]


            >>> print '\\n'.join( ag( legendformat=['%(symb)s : %(name)s', '\\n',','] ) )      # doctest: +NORMALIZE_WHITESPACE
            6 +----+----+----+----+
              :    :    o    : x  :
            4 +----+-o--x----+----+
              :    o    :    :    :
            2 +--o-+----+--*-+----+
              : N  :    :    : Long_text_label
            0 +----+----+----+----+
              0    5   10   15   20  
            x : #2
            N : New
            * : #4
            Long_text_label : #5
            o : L1



        ___draw function___      
            
            
            >>> ag.pxdataset=[]
            >>> ag.addpxdata( pt= lambda w: w/3, xrange=(0,13) )
            >>> ag.pxdataset                   # doctest:+ELLIPSIS +NORMALIZE_WHITESPACE
            [{'symb': 'o', 'yaxisr': False, 'data': <function <lambda> at ...>, 'name': '#1', 'xrange': (0, 13)}]

            
            >>> print '\\n'.join( ag( ) )      # doctest: +NORMALIZE_WHITESPACE
            6 +----+----+----+----+
              :    :    :    :    :
            4 +----+----+-oo-+----+
              :    :   ooo   :    :
            2 +----+ooo-+----+----+
              :  ooo    :    :    :
            0 ooo--+----+----+----+
              0    5   10   15   20  
            o : #1

            
            >>> ag.addpxdata( pt= lambda w:6- w/3.0 )
            >>> ag.pxdataset                             # doctest:+ELLIPSIS +NORMALIZE_WHITESPACE
            [{'symb': 'o', 'yaxisr': False, 'data': <function <lambda> at ...>, 'name': '#1', 'xrange': (0, 13)}, 
             {'symb': 'x', 'yaxisr': False, 'data': <function <lambda> at ...>, 'name': '#2'}]


            >>> leg = '[*]: f(x)=x/3 (range:0~13)\\n[o]: f(x)=6-x/3'
            >>> print '\\n'.join( ag( legend= leg,
            ...                       xlabel='x', ylabel='y') )    # doctest: +NORMALIZE_WHITESPACE
              6 xx---+----+----+----+
                : xxx:    :    :    :
              4 +----xxx--+-oo-+----+
            y   :    :  xxxo   :    :
              2 +----+ooo-+xxx-+----+
                :  ooo    :   xxx   :
              0 ooo--+----+----+-xxx+
                0    5   10   15   20  
                          x          
            [*]: f(x)=x/3 (range:0~13)
            [o]: f(x)=6-x/3
            
            
            
            >>> wu, wc = 12, 5
            >>> hu, hc = 4, 4
            >>> ag= AscGrid( hunit=hu, hcount=hc, wcount=wc, xlabel='x value', ylabel='sin x', _yaxisscaler=True, 
            ... footer='This is a demo to show sine wave', wunit=wu, style='func1', hline=':', yformat='%1.1f')
            
            Create a function:
            
            >>> f = lambda x: math.sin(x)
            
            Add data :
            
            >>> ag.addpxdata(pt=f, symb='o', xscalerange=(0,10, 0.25), yscalerange=(-1,1))  #1
            
            The above line is the same as:
            
                ag.addpxdata( pt=[(f(w), w) for w in range(0, wc*wu)], symb='o')           #2
            
            But the way data stored in ag.pxdataset is different: #1 stores the function,
            but #2 stores the function outcome (entire set of data).  
            
            >>> print '\\n'.join( ag( ) )             # doctest: +NORMALIZE_WHITESPACE
               1.0 +-------oo-o---------------------------------o-oo-----------+ 1.0  
                   |     o     o           :           :       o   : o         |      
                   |           : o         :           :           :  o        |      
                   |    o      :  o        :           :     o     :    o      |      
               0.5 +..o.....................................o..................+ 0.5  
                   |           :    o      :           :           :     o     |      
            s      | o         :           :           :  o        :           |      
            i      |           :     o     :           :           :       o   |      
            n  0.0 o.....................................o.....................+ 0.0  
                   |           :       o   :           :           :        o  |      
            x      |           :           :           o           :           |      
                   |           :        o  :           :           :          o|      
              -0.5 +..................................o........................o -0.5 
                   |           :          o:           :           :           |      
                   |           :           o        o  :           :           |      
                   |           :           : o     o   :           :           |      
              -1.0 +--------------------------o-oo-----------------------------+ -1.0 
                   0           2           4           6           8          10     
                                              x value                           
            This is a demo to show sine wave
            
            
            
            
            
            
            
        '''
        
        self.textdataset.append( {'text':text, 'pt':pt, 'yaxisr':yaxisr} )
        
        
    def drawtextdata(self):
        ''' Text is added right after the plot data and before axes '''
        tds = self.textdataset 
        lines = self.__dict__['__lines__']
        g_xs = (self.xmin, self.xmax)
        g_ys = (self.ymin, self.ymax)
        H = self.height
        W = self.width
        def drawtx(D, lines):
            x= D['pt'][0]
            y= D['pt'][1]
            pc = self.x2cfactor * (x - g_xs[0])
            pc = int(round(pc*1.0))
            
            d= D['text']
            if inspect.isfunction( d ): d = d(D)
            
            if D.get('yaxisr',False):
                g_yrs = (self.yrmin, self.yrmax)
                pr = H- self.yr2rfactor * ( y - g_yrs[0] ) -1 
            else:
                pr = H- self.y2rfactor  * ( y - g_ys[0] ) -1 
                
            #yfactor = y2rfactor 
            #print '-- In drawpx, yfactor= ', yfactor, 'x,y= ', (x,y)
            #pr = H- yfactor * ( y - g_ys[0] ) -1 
            #pr = H- yfactor * ( y - g_ys[0] ) -1 
            pr = int(round(pr*1.0))
            #print '-- In drawpx, yfactor= ',  'x,y= ', (x,'%1.3f'%y), ', pc,pr= ', (pc, pr), ', H= ', H
            #pr = H- y2rfactor * (y - data['yrange'][0]) -1 
            #pc = x2cfactor * (x - data['xrange'][0])
            
            #print 'in drawpx,llen=', len(lines), ', pr,pc=', pr, ', ',pc, ', W,H= ', W, ', ', H
            if pc< W and pr< H  :
               #print '---       in drawpx, pr(%s)<=h(%s) and pc(%s)<=w(%s)'%(pr,h,pc,w)
               line=lines[pr]
               lines[pr]=line[:pc]+ d+line[pc+len(d):]
               
        for D in tds:
            drawtx( D , lines ) 
                  
        #if len(td):
                   
    def addpxdata(self, pt, name= None
                 , symb= None
                 , append= False 
                 , xrange= None            ## (0,1,0.2) The x range the data_function uses to generate y 
                 , yrange= None 
                 , xscalerange= None       ## (i,j): Global xscalerange is set at the time of initiation AscGrid(...).
                                          ## If it is set here again, it means we want to modify the scale  
                 , yscalerange= None 
                 , yscaleranger= None
                  
                 , yaxisr= False
                ):           
        '''
            >>> ag= AscGrid()  
            >>> ag.addpxdata( pt=(7,3) ) 
            >>> ag.pxdataset                
            [{'symb': 'o', 'yaxisr': False, 'data': [(7, 3)], 'name': '#1'}]


            >>> print '\\n'.join( ag( ) )          # doctest: +NORMALIZE_WHITESPACE
            6 +----+----+----+----+
              :    :    :    :    :
            4 +----+----+----+----+
              :    : o  :    :    :
            2 +----+----+----+----+
              :    :    :    :    :
            0 +----+----+----+----+
              0    5   10   15   20
        
            >>> ag.pxdataset 
            [{'symb': 'o', 'yaxisr': False, 'data': [(7, 3)], 'name': '#1'}]

                
            >>> ag.addpxdata(  (17,5)  )
            >>> ag.pxdataset                       # doctest: +NORMALIZE_WHITESPACE
            [{'symb': 'o', 'yaxisr': False, 'data': [(7, 3)], 'name': '#1'}, 
             {'symb': 'x', 'yaxisr': False, 'data': [(17, 5)], 'name': '#2'}]
             
            >>> print '\\n'.join( ag( ) )          # doctest: +NORMALIZE_WHITESPACE
            6 +----+----+----+----+
              :    :    :    : x  :
            4 +----+----+----+----+
              :    : o  :    :    :
            2 +----+----+----+----+
              :    :    :    :    :
            0 +----+----+----+----+
              0    5   10   15   20            
            
            >>> ag.addpxdata( (2, 1), 'New', 'N')
            >>> ag.pxdataset                       # doctest: +NORMALIZE_WHITESPACE
            [{'symb': 'o', 'yaxisr': False, 'data': [(7, 3)], 'name': '#1'}, 
             {'symb': 'x', 'yaxisr': False, 'data': [(17, 5)], 'name': '#2'}, 
             {'symb': 'N', 'yaxisr': False, 'data': [(2, 1)], 'name': 'New'}]

             
            >>> print '\\n'.join( ag( ) )          # doctest: +NORMALIZE_WHITESPACE
            6 +----+----+----+----+
              :    :    :    : x  :
            4 +----+----+----+----+
              :    : o  :    :    :
            2 +----+----+----+----+
              : N  :    :    :    :
            0 +----+----+----+----+
              0    5   10   15   20
            
            
            >>> ag.addpxdata( (13, 2))
            >>> print '\\n'.join( ag( ) )       # doctest: +NORMALIZE_WHITESPACE
            6 +----+----+----+----+
              :    :    :    : x  :
            4 +----+----+----+----+
              :    : o  :    :    :
            2 +----+----+--*-+----+
              : N  :    :    :    :
            0 +----+----+----+----+
              0    5   10   15   20
            
                                                ## append data
            
            >>> ag.addpxdata(  pt=(10, 4), name='#2', append=True  )
            >>> ag.pxdataset                       # doctest: +NORMALIZE_WHITESPACE
             [{'symb': 'o', 'yaxisr': False, 'data': [(7, 3)], 'name': '#1'}, 
              {'symb': 'x', 'yaxisr': False, 'data': [(17, 5), (10, 4)], 'name': '#2'}, 
              {'symb': 'N', 'yaxisr': False, 'data': [(2, 1)], 'name': 'New'}, 
              {'symb': '*', 'yaxisr': False, 'data': [(13, 2)], 'name': '#4'}]             

            
            >>> print '\\n'.join( ag( legend=True) )           # doctest: +NORMALIZE_WHITESPACE
            6 +----+----+----+----+
              :    :    :    : x  :
            4 +----+----x----+----+
              :    : o  :    :    :
            2 +----+----+--*-+----+
              : N  :    :    :    :
            0 +----+----+----+----+
              0    5   10   15   20  
            [o:#1, x:#2, N:New, *:#4]
            
                                                  ## getdata by name
            
            >>> ag.getpxdata('#1')           # doctest: +NORMALIZE_WHITESPACE
            {'symb': 'o', 'yaxisr': False, 'data': [(7, 3)], 'name': '#1'}

            
            >>> ag.delpxdata('#1')              ## delete data
            >>> print '\\n'.join( ag( ) )        # doctest: +NORMALIZE_WHITESPACE
            6 +----+----+----+----+
              :    :    :    : x  :
            4 +----+----x----+----+
              :    :    :    :    :
            2 +----+----+--*-+----+
              : N  :    :    :    :
            0 +----+----+----+----+
              0    5   10   15   20  
            [x:#2, N:New, *:#4]


                                               ## symb can be a string
                                               
            >>> ag.addpxdata(pt=(17,1), symb="Long_text_label")
            
            >>> print '\\n'.join( ag( ) )      # doctest: +NORMALIZE_WHITESPACE
             6 +----+----+----+----+
              :    :    :    : x  :
            4 +----+----x----+----+
              :    :    :    :    :
            2 +----+----+--*-+----+
              : N  :    :    : Long_text_label
            0 +----+----+----+----+
              0    5   10   15   20  
            [x:#2, N:New, *:#4, Long_text_label:#5]

             
            
            >>> ag.addpxdata( pt=[ (3,2),(5,3),(7,4),(10,5) ], name='L1')
            
            >>> print '\\n'.join( ag( ) )      # doctest: +NORMALIZE_WHITESPACE
            6 +----+----+----+----+
              :    :    o    : x  :
            4 +----+-o--x----+----+
              :    o    :    :    :
            2 +--o-+----+--*-+----+
              : N  :    :    : Long_text_label
            0 +----+----+----+----+
              0    5   10   15   20  
            [x:#2, N:New, *:#4, Long_text_label:#5, o:L1]


            >>> print '\\n'.join( ag( legendformat=['%(symb)s : %(name)s', '\\n',','] ) )      # doctest: +NORMALIZE_WHITESPACE
            6 +----+----+----+----+
              :    :    o    : x  :
            4 +----+-o--x----+----+
              :    o    :    :    :
            2 +--o-+----+--*-+----+
              : N  :    :    : Long_text_label
            0 +----+----+----+----+
              0    5   10   15   20  
            x : #2
            N : New
            * : #4
            Long_text_label : #5
            o : L1



        ___draw function___      
            
            
            >>> ag.pxdataset=[]
            >>> ag.addpxdata( pt= lambda w: w/3, xrange=(0,13) )
            >>> ag.pxdataset                   # doctest:+ELLIPSIS +NORMALIZE_WHITESPACE
            [{'symb': 'o', 'yaxisr': False, 'data': <function <lambda> at ...>, 'name': '#1', 'xrange': (0, 13)}]

            
            >>> print '\\n'.join( ag( ) )      # doctest: +NORMALIZE_WHITESPACE
            6 +----+----+----+----+
              :    :    :    :    :
            4 +----+----+-oo-+----+
              :    :   ooo   :    :
            2 +----+ooo-+----+----+
              :  ooo    :    :    :
            0 ooo--+----+----+----+
              0    5   10   15   20  
            o : #1

            
            >>> ag.addpxdata( pt= lambda w:6- w/3.0 )
            >>> ag.pxdataset                             # doctest:+ELLIPSIS +NORMALIZE_WHITESPACE
            [{'symb': 'o', 'yaxisr': False, 'data': <function <lambda> at ...>, 'name': '#1', 'xrange': (0, 13)}, 
             {'symb': 'x', 'yaxisr': False, 'data': <function <lambda> at ...>, 'name': '#2'}]


            >>> leg = '[*]: f(x)=x/3 (range:0~13)\\n[o]: f(x)=6-x/3'
            >>> print '\\n'.join( ag( legend= leg,
            ...                       xlabel='x', ylabel='y') )    # doctest: +NORMALIZE_WHITESPACE
              6 xx---+----+----+----+
                : xxx:    :    :    :
              4 +----xxx--+-oo-+----+
            y   :    :  xxxo   :    :
              2 +----+ooo-+xxx-+----+
                :  ooo    :   xxx   :
              0 ooo--+----+----+-xxx+
                0    5   10   15   20  
                          x          
            [*]: f(x)=x/3 (range:0~13)
            [o]: f(x)=6-x/3
            
            
            
            >>> wu, wc = 12, 5
            >>> hu, hc = 4, 4
            >>> ag= AscGrid( hunit=hu, hcount=hc, wcount=wc, xlabel='x value', ylabel='sin x', _yaxisscaler=True, 
            ... footer='This is a demo to show sine wave', wunit=wu, style='func1', hline=':', yformat='%1.1f')
            
            Create a function:
            
            >>> f = lambda x: math.sin(x)
            
            Add data :
            
            >>> ag.addpxdata(pt=f, symb='o', xscalerange=(0,10, 0.25), yscalerange=(-1,1))  #1
            
            The above line is the same as:
            
                ag.addpxdata( pt=[(f(w), w) for w in range(0, wc*wu)], symb='o')           #2
            
            But the way data stored in ag.pxdataset is different: #1 stores the function,
            but #2 stores the function outcome (entire set of data).  
            
            >>> print '\\n'.join( ag( ) )             # doctest: +NORMALIZE_WHITESPACE
               1.0 +-------oo-o---------------------------------o-oo-----------+ 1.0  
                   |     o     o           :           :       o   : o         |      
                   |           : o         :           :           :  o        |      
                   |    o      :  o        :           :     o     :    o      |      
               0.5 +..o.....................................o..................+ 0.5  
                   |           :    o      :           :           :     o     |      
            s      | o         :           :           :  o        :           |      
            i      |           :     o     :           :           :       o   |      
            n  0.0 o.....................................o.....................+ 0.0  
                   |           :       o   :           :           :        o  |      
            x      |           :           :           o           :           |      
                   |           :        o  :           :           :          o|      
              -0.5 +..................................o........................o -0.5 
                   |           :          o:           :           :           |      
                   |           :           o        o  :           :           |      
                   |           :           : o     o   :           :           |      
              -1.0 +--------------------------o-oo-----------------------------+ -1.0 
                   0           2           4           6           8          10     
                                              x value                           
            This is a demo to show sine wave
            
            
            
            
            
            
            
        '''
        #print '\n\nEntered addpxdata:, self.yscaleranger = ', self.yscaleranger
        #if xscalerange==None: xscalerange=self.xscalerange 
        #if yscalerange==None: xscalerange=self.xscalerange
        #if yscaleranger==None: xscaleranger=self.xscaleranger
        
        def autosymb():  # get a symb automatically
            symbs = 'ox*acemnosuvwz#'
            #print 'in autosumb(), pxdataset=', self.pxdataset
            #print 'in autosumb(), symbs= ', [d['symb'] for d in self.pxdataset ]
            for d in self.pxdataset: symbs = symbs.replace(d['symb'], '') 
            return symbs[0]
        
        def autoname():  # get a data name automatically
            dl=len(self.pxdataset)
            numbs= [dl]+[ int(x.replace('#', ''))
                           for x in self.datanames if x.startswith('#') ]
            return '#%s'%(max(numbs)+1)
        
        ## If data=(3,4), we make it to [(3,4)]    
        if not inspect.isfunction(pt) and not(type(pt[0])==list or type(pt[0])==tuple) and not type(pt)==str: pt = [pt]
        
        ## Append to existing data if name and append:
        if name and append:
            if name in [ d['name'] for d in self.pxdataset ] :
                self.getpxdata( name )['data']= self.getpxdata( name )['data']+ pt
        else:
            if not symb: symb = autosymb()
            if not name: name = autoname()
            
            d= {'name':name, 'symb':symb, 'data': pt, 'yaxisr':yaxisr}
            if xrange: d['xrange']= xrange
            if yrange: d['yrange']= yrange
            ##
            ## If scalerange is given for this data, it means we intend to 
            ## change the global scalerange. So we chk and store it to 
            ## be used in drawpxdata.
            ##
            if xscalerange: 
                if len(xscalerange)<3: xscalerange= list(xscalerange)+[1]
                self.xscalerange = d['xscalerange']= xscalerange
            if yscalerange: 
                if len(yscalerange)<3: yscalerange= list(yscalerange)+[1]
                self.yscalerange = d['yscalerange']= yscalerange
            if yscaleranger: 
                if len(yscaleranger)<3: yscaleranger= list(yscaleranger)+[1]
                self.yscaleranger = d['yscaleranger']= yscaleranger
                
             
            #d['xrange']= xrange==None and (0, self.width-1 ) or xrange
            #d['yrange']= yrange==None and (0, self.height-1) or yrange
                
            #if inspect.isfunction(pt): 
            #    d['xvalrange']= xvalrange==None and (self.xmin, self.xmax) or xvalrange
            #d['xvalrange']= xvalrange==None and d['xrange'] or xvalrange
                
            #d['wrange']= ( d['wrange'][0], d['wrange'][1]+1 )
            self.pxdataset.append( d )
            #print 'In addata, self.pxdataset= ', self.pxdataset
        
                
    def delpxdata(self, name):
        del self.pxdataset[ self.datanames.index(name) ]

    def getpxdata(self, name):
        '''
        >> ag= AscGrid()
        >> ag.markpix( 'x', (3,5), name='p1' )
        >> ag.plotpix( 'y', [(4,1),(2,11)], name='p2')
        >> ag.pxdataset                         # doctest: +NORMALIZE_WHITESPACE
        [{'symb': 'x', 'data': [(3, 5)], 'name': 'p1'},
         {'symb': 'y', 'data': [(4, 1), (2, 11)], 'name': 'p2'}]
        >> ag.datanames
        ['p1', 'p2']

        '''
        return self.pxdataset[ self.datanames.index(name) ]

    def drawpxdata(self):
        '''
            Place the data of .pxdataset to the .lines
        '''
        #print 'Entered drawdata '
        
        #print 'In drawpxdata, lines len= ', len(lines)
        W = self.width 
        H = self.height 
        
        def drawpx(x,y, lines, dataobj):
            #print 'Entered drawpx, xscalerange=%s, yscalerange=%s'%(xscalerange, yscalerange)
            #pr = H- y2rfactor * (y - yscalerange[0]) -1 
            pc = x2cfactor * (x - g_xs[0])
            pc = int(round(pc*1.0))
            
            ## if a function is given as the data, we need to re-adjust the x, 
            ## 'cos after the rounding process above, the x is not the original 
            ## x. We need to calc the new y according to this new x 
            d= dataobj['data']
            if inspect.isfunction( d ):
                y = d( pc / x2factor + g_xs[0])
            
            if dataobj.get('yaxisr',False):
                pr = H- yr2rfactor * ( y - g_yrs[0] ) -1 
            else:
                pr = H- y2rfactor  * ( y - g_ys[0] ) -1 
                
            #yfactor = y2rfactor 
            #print '-- In drawpx, yfactor= ', yfactor, 'x,y= ', (x,y)
            #pr = H- yfactor * ( y - g_ys[0] ) -1 
            #pr = H- yfactor * ( y - g_ys[0] ) -1 
            pr = int(round(pr*1.0))
            #print '-- In drawpx, yfactor= ',  'x,y= ', (x,'%1.3f'%y), ', pc,pr= ', (pc, pr), ', H= ', H
            #pr = H- y2rfactor * (y - data['yrange'][0]) -1 
            #pc = x2cfactor * (x - data['xrange'][0])
            
            #print 'in drawpx,llen=', len(lines), ', pr,pc=', pr, ', ',pc, ', W,H= ', W, ', ', H
            if pc< W and pr< H  :
               #print '---       in drawpx, pr(%s)<=h(%s) and pc(%s)<=w(%s)'%(pr,h,pc,w)
               line=lines[pr]
               lines[pr]=line[:pc]+s+line[pc+len(s):]
        
        dataset = deepcopy(self.pxdataset)  
        ##
        ## if not autoscale, the xy scale range, if none specified, 
        ## will be the same as the .width and .height
        ##
          
        ## Fist we chk if any xaxis, yaxis values are set in dataset 
        ## This will overwrite the default xmin, xmax, ymin, ymax values
        ## UNLESS autoscale = True, in which case the grid range will
        ## be decided by data
        ##

        ##
        ## check if any xscalerange, yscalerange is specified in dataset.
        ## If yes, compile them and use the min and max values for scales.
        ##
        ## Because we take the min and max of all values, this means that
        ## any new x(y)scalerange comes with a specific data
        ## will only "extend" or "enlarge" the grid, but not reduce it.  
        ##
        ##     TODO: make it reducible. Only with that can we implant 
        ##     "shrink" and "pan" features. 
        ##
        ## If not, set them to the sizes of the grid (.width, .height).
        ##
        
        ## global settings
        g_xs= self.xscalerange 
        g_ys= self.yscalerange
        g_yrs=self.yscaleranger 
        if not g_xs: g_xs  = ( 0, self.width-1, 1 )
        if not g_ys: g_ys  = ( 0, self.height-1, 1 )
        #if not g_yrs: g_yrs= ( 0, self.height-1, 1 )
        #print 'In drawpxdata, \ng_xs=%s, \ng_ys=%s, \ng_yrs=%s '%(g_xs, g_ys, g_yrs)
        
        ## data-specific settings
        d_xs = [ d['xscalerange']  for d in dataset if 'xscalerange' in d ]  # [(0,10), (5,20,0.5) ...]
        d_ys = [ d['yscalerange']  for d in dataset if 'yscalerange' in d ]
        d_yrs= [ d['yscaleranger'] for d in dataset if 'yscaleranger' in d ]
        
        #print 'In drawpxdata, \nd_xs=%s, \nd_ys=%s, \nd_yrs=%s '%(d_xs, d_ys, d_yrs)
        #print 'In drawpxdata, self.xranges, yranges, yarngesr '
        
        if d_xs: 
            g_xs= ( min( [ p[0] for p in d_xs ])
                  , max( [ p[1] for p in d_xs ]) 
                  , min( [ p[2] for p in d_xs ]))
        #else: g_xs = ( 0, self.width-1, 1 )
            
        if d_ys: 
            g_ys= ( min( [ p[0] for p in d_ys ])
                  , max( [ p[1] for p in d_ys ]) 
                  , min( [ p[2] for p in d_ys ]))
        #else: g_ys= ( 0, self.height-1, 1 )
        
        ##
        ## if g_yrs is set to True by user, we will copy left-y scale markers
        ## to right-y.
        ## 
        if g_yrs==True: g_yrs= g_ys
        elif d_yrs: 
             g_yrs= ( min( [ p[0] for p in d_yrs ])
                    , max( [ p[1] for p in d_yrs ]) 
                    , min( [ p[2] for p in d_yrs ]))
        #else: g_yrs= ( 0, self.height-1, 1 )          
        
        #print 'In drawpxdata, \ng_xs=%s, \ng_ys=%s, \ng_yscalearnger=%s '%(g_xs, g_ys, g_yrs)
        
                
        ## To simplify, we convert any data that is a function to values first 
        ## before actual plotting, because we might need some of the data values 
        ## to re-decide the grid size (scalerange) when .autoscale=True:
        ## 
        for D in dataset:
            d= D['data']
            if type(d)==str: 
                d = eval('lambda x:'+d)      
                D['data']=d
                #print 'string function, d=', d
            if inspect.isfunction(d):
                #for w in D['wrange']:
                #    print 'in drawpxdata, (w,h)= (', w, ', ', d(w) ,')' 
                rng = D.get('xrange', g_xs)
                #print '--- in drawpxdata, rng = ', rng 
                if len(rng)==2: rng = list(rng)+[1]
                d = [ (x, d(x)) for x in frange( rng[0],rng[1]+rng[2], rng[2] ) ]
                #print '--- in drawpxdata, data = ', d 
                
                D['data']=d
        ##    
        ## If autoscale, recalc the xscalerange, yscalerange based on the data     
        ##
        if self.autoscale:
            xvals=[]
            yvals=[]
            for D in dataset:
                xvals += [ p[0] for p in D['data'] ]
                yvals += [ p[1] for p in D['data'] ]
            xmin, xmax, ymin, ymax = min(xvals), max(xvals), min(yvals), max(yvals)     
            xlen, ylen = xmax-xmin, ymax-ymin 
            xstep, ystep = (1.0*xlen)/(self.width), (1.0*ylen)/(self.height)
            g_xs = ( xmin, xmax, xstep )    
            g_ys = ( ymin, ymax, ystep )
            #if yrangesr:
            #    yscaleranger = 
                
        xlen = g_xs[1] - g_xs[0]
        ylen = g_ys[1] - g_ys[0]
        if g_yrs:  
            yrlen= g_yrs[1]- g_yrs[0] #d_yrs and (g_yrs[1] - g_yrs[0]) or ylen
            self.yrmin, self.yrmax = g_yrs
        
        self.xmin, self.xmax, self.ymin, self.ymax = g_xs[0], g_xs[1], g_ys[0], g_ys[1]
        
       
        x2cfactor = ( self.width -1) / (xlen*1.0)   
        y2rfactor = ( self.height-1) / (ylen*1.0)  
        self.__dict__['x2cfactor'] = x2cfactor
        self.__dict__['y2rfactor'] = y2rfactor
        if g_yrs: 
            yr2rfactor= ( self.height-1) / (yrlen*1.0)
            self.__dict__['yr2rfactor'] = yr2rfactor
        
        
        lines = self.__dict__['__lines__']
        
        ##
        ## Actual drawing of data
        ## 
        for D in dataset:
            #print 
            #print 'In drawpxdata, drawing data ', D['name'], ', yaxisr= ', D.get('yaxisr', 'Not defined')
            #print 'data= ', D['data']
            #print
            s= D['symb']
            d= D['data']
            for x, y in d:                 
                drawpx(x,y,lines, D)
       
       
        ## We now calc axis scale numbers/markers ( _xaxisscalet, _xaxisscaleb
        ## _yaxisscalel, _yaxisscaler) to be used to drax axis in drawaxes() in
        ## next function
        
        ##
        ## Define the yaxis scales (using yscalerange)
        ##
        ys = [ self.yformat%(r*self.hunit/y2rfactor + g_ys[0]) for r in range(self.hcount+1) ]
        _yaxisscalel=[]
        ywid = max([ len(y) for y in ys])
        for r, line in enumerate(lines):
            yhead = (r%self.hunit==0) and ys.pop()  or ' '
            _yaxisscalel.append( yhead.rjust(ywid, ' ') + ' ' ) 
        self._yaxisscalel = _yaxisscalel
        
        ##
        ## Define the yaxisr scales (using yscaleranger) 
        ##
        #print 'In drawpxdata, defining yaxisr, yrangesr= ', yrangesr
        if g_yrs :
            ys = [ self.yformatr%(r*self.hunit/yr2rfactor + g_yrs[0]) for r in range(self.hcount+1) ]
            _yaxisscaler=[]
            ywid = max([ len(y) for y in ys])
            for r, line in enumerate(lines):
                yhead = (r%self.hunit==0) and ys.pop()  or ' '
                _yaxisscaler.append( ' '+yhead.rjust(ywid, ' ') ) 
            self._yaxisscaler = _yaxisscaler
                
        ##
        ## Define the xaxis scales
        ##
        xaxis= ' '* (self.width+ywid + 1)
        for c in range(self.wcount+1):  #  c: 0,1,2,3 ...
            cw= c* self.wunit #+ (c==0 and [0] or [1])[0]    # cw: 0, 5, 10, 12 ... 
            x= cw / x2cfactor + g_xs[0]
            x = self.xformat%x #(int(x)) 
            xl = len(x)/2
            xs= cw-xl #+ ywid + 1
            xaxis = xaxis[ :xs ] + x + xaxis[ (len(x)+xs): ]
        self._xaxisscaleb = xaxis 
        
        self.xscalerange = g_xs 
        self.yscalerange = g_ys
        self.yscaleranger= g_yrs
        

    def loaddatafile(self, fn, lineparser=None):
        '''
           5  56   5   3  27720     50  27670  0.9982 
        '''
        import re
        # re.split('\W+', 'Words, words, words.')
        def par(x):
            li= re.split('\s+', x)
            #print li 
            return (int(li[0]),float(li[-1]))
        if not lineparser:
            lineparser = par #lambda line: (int(line.split('\t')[0]), float(line.split('\t')[-1]) )    
            
        lines = open(fn, 'r').readlines()[1:]
        #print 'items per line: ', len(lines[0].split('\t'))
        pts = [ lineparser(line.strip()) for line in lines ]
        return pts
        #self.addpxdata(pt = pts)
        #print self.pxdataset 


    def __call__(self, **kargs):
        #print '--- Entered __call__'
        self.draw(**kargs)
        return deepcopy(self.__dict__['__lines__'])

    

def test1():
    '''
        >>> ag=AscGrid( wunit=8, wcount=10, hunit=8, hcount=4, yformat='%1.2f', ylabel='Y', ylabelr='Y2' )
        >>> pts = ag.loaddatafile('/home/vincent/code/repos/lotto/Simu_Rf_on_Nb56_Np5_Ns3.txt')
        >>> ag.addpxdata( pts,  xscalerange=(0, 1500), yscalerange=(0.5,1) )
        
        >> lines = ag( )
        >> print '\\n'.join( lines)
          1.00 o-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+  
               :  o    :       :       :       :       :       :       :       :       :       :  
               :     o :       :       :       :       :       :       :       :       :       :  
               :       o       :       :       :       :       :       :       :       :       :  
               :       :       :       :       :       :       :       :       :       :       :  
               :       :  o    :       :       :       :       :       :       :       :       :  
               :       :     o :       :       :       :       :       :       :       :       :  
               :       :       o       :       :       :       :       :       :       :       :  
          0.88 +-------+-------+--o----+-------+-------+-------+-------+-------+-------+-------+  
               :       :       :     o :       :       :       :       :       :       :       :  
               :       :       :       o       :       :       :       :       :       :       :  
               :       :       :       :  o    :       :       :       :       :       :       :  
               :       :       :       :     o :       :       :       :       :       :       :  
               :       :       :       :       o  o    :       :       :       :       :       :  
               :       :       :       :       :     o :       :       :       :       :       :  
               :       :       :       :       :       o       :       :       :       :       : Y
        Y 0.75 +-------+-------+-------+-------+-------+--o----+-------+-------+-------+-------+ 2
               :       :       :       :       :       :     o :       :       :       :       :  
               :       :       :       :       :       :       o       :       :       :       :  
               :       :       :       :       :       :       :  o  o :       :       :       :  
               :       :       :       :       :       :       :       o       :       :       :  
               :       :       :       :       :       :       :       :  o    :       :       :  
               :       :       :       :       :       :       :       :     o :       :       :  
               :       :       :       :       :       :       :       :       o  o    :       :  
          0.62 +-------+-------+-------+-------+-------+-------+-------+-------+-----o-+-------+  
               :       :       :       :       :       :       :       :       :       o  o    :  
               :       :       :       :       :       :       :       :       :       :     o :  
               :       :       :       :       :       :       :       :       :       :       o  
               :       :       :       :       :       :       :       :       :       :       :  
               :       :       :       :       :       :       :       :       :       :       :  
               :       :       :       :       :       :       :       :       :       :       :  
               :       :       :       :       :       :       :       :       :       :       :  
          0.50 +-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+  
               0      150     300     449     600     750     899    1050    1200    1350    1500
        '''
    #ag.drawmerges_test()
    
def test2():
    ag= AscGrid()  
    ag.addpxdata( pt=(7,3) ) 
    ag.pxdataset                
    print '\n'.join( ag( ) )             
    
    #[{'symb': 'o', 'yaxisr': False, 'data': [(7, 3)], 'name': '#1'}]

    '''
            >>> print '\\n'.join( ag( ) )          # doctest: +NORMALIZE_WHITESPACE
            6 +----+----+----+----+
              :    :    :    :    :
            4 +----+----+----+----+
              :    : o  :    :    :
            2 +----+----+----+----+
              :    :    :    :    :
            0 +----+----+----+----+
              0    5   10   15   20
    '''    
    ag.addtext( text='<==', pt=(8,3) )
    
    print ag.textdataset 
    
    # [{'symb': 'o', 'yaxisr': False, 'data': [(7, 3)], 'name': '#1'}]
    print '\n'.join( ag( ) )          # doctest: +NORMALIZE_WHITESPACE
            
             
def test3():
    def legendfunction(dataset):
        return '\n'.join( [ '%(name)s: %(symb)s, %(data)s'%d+ (d['yaxisr'] and ' (Y2)' or '') for d in dataset] )
        
    ops =  { 'hunit': 4
           , 'hcount': 4
           , 'wunit': 12
           , 'wcount': 5 
           , 'xlabel': 'x value'
           , 'ylabel':'sin x'
           , 'yscaleranger':True
           , 'footer':'This is a demo to show sine wave'
           , 'style':'func1'
           , 'hline': ':'
           , 'yformat':'%1.1f'
           , 'title':'Two sin waves and x/10'
           , 'header':'At the same graph'
           , 'xscalerange':(0, 10, 0.25)
           , 'yscalerange':(-1,1)  
           , 'legend': legendfunction  
           }
    f = 'math.sin(x)'  #lambda x: math.sin(x)
    f2 = 'x/10.0'      #lambda x: x/10 #math.exp(-0.5*x)
    
    
    ag=AscGrid(**ops)
    ag.addpxdata(pt=f)
    #print '\n'.join( ag( ) )             
    
    
    ops2=ops.copy()
    ops2.update( { 'yscaleranger': (-0.5,1.5)
                 , 'yformatr': '%1.1f'
                 , 'outerborder': True
                 } )
    ag2= AscGrid(**ops2)
    ag2.addpxdata(pt=f, symb='o')
    ag2.addpxdata(pt=f2, yaxisr=True)
    print '\n'.join( ag2( ) )             
    
    #print '\n'.join( ag2( yscalerange=(-2,2) ) )             
    
    
#ag = AscGrid()
#pprint.pprint( ag() )
#print '\n'.join( ag( ))

if __name__=='__main__':
    doctest.REPORT_NDIFF=True
    #ag= AscGrid()
    #doctest.run_docstring_examples(frange, globals())
    #doctest.run_docstring_examples(AscGrid.addpxdata, globals())
    #doctest.run_docstring_examples(test, globals())
    #doctest.run_docstring_examples(test2, globals())
    #test3()
    #test2()
    doctest.testmod()
    
